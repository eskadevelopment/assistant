# Prepare machine
```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
nvm install 10
sudo apt-get install postgresql-dev
```
relogin for this user

# Start kafka
In first console `vagga zookeeper`
In second console `vagga kafka`

Configure `run` part in `vagga.yaml` for self needs \

Run: \
`vagga run`

Run in prod mode: \
`vagga prod`


## For run ethan bot
Prepare:
```bash
cd src/logic/bots/ethan/slack_hooks
git submodule init
cd ../../../../../
git submodule update
```

## Child projects

 - [BPM](./src/logic/bpm/Readme.md) 
 - [PMT](./src/logic/bpm/src/controller/pmt/Readme.md)

# Enjoy!
