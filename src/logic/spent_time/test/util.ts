const MONDAY = 1;

export type BusinessDay = {
    start: Date,
    end: Date
}

export const getLastBusinessDay = (): BusinessDay => {
    const currentDateTime = new Date();
    const currentDate = currentDateTime.getDate();
    currentDateTime.setHours(0, 0, 0, 0);

    if (currentDateTime.getDay() == MONDAY) {
        currentDateTime.setDate(currentDate - 2);
    }

    return {
        start: getYesterdayDate(currentDateTime),
        end: currentDateTime
    };
};

export const getYesterdayDate = (date: Date = new Date()): Date => {
    const yesterdayDate = new Date(date);

    yesterdayDate.setDate(date.getDate() - 1);
    yesterdayDate.setHours(0, 0, 0, 0);

    return yesterdayDate;
};
