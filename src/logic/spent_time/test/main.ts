import { p } from './common';
import { Action, Route, State } from 'assistant';

const s = new State(new Route('seq', [
    new Action('SPENT_TIME', 'logic.spent_time'),
    new Action('GET_MANY', 'driver.postgresql'),
    new Action('PROCESS', 'logic.spent_time')
]), {
    spentTimeDate: {
        start: '2020-01-11T02:00:00.000Z',
        end: '2020-01-12T02:00:00.000Z'
    }
});

p.start(s);
