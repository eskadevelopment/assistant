import { SlackUser } from '../model/slack/slackUser';
import { Card } from '../model/slack/card';
import { Block } from '../../../../drivers/slack/src/model/block';
import { createSlackData } from '../util/slack';
import { SlackTransport } from '../transport/slack';
import { WORKSPACE } from '../../../../drivers/slack/src/configs';
import { Type } from '../../../../drivers/slack/src/types';

export interface ISpentTime {
    process(data: Map<SlackUser, Array<Card>>, date: Date);
}

export class SpentTimeService implements ISpentTime {

    slackTransport: SlackTransport;

    constructor() {
        this.slackTransport = new SlackTransport();
    }

    process(data: Map<SlackUser, Array<Card>>, date: Date) {
        [...data.entries()].forEach(([user, userCards]: [SlackUser, Array<Card>]) => {
            const userBlocks: Array<Block> = createSlackData(userCards, date);
            this.slackTransport.send(WORKSPACE.ESKA, Type.SECTION, userBlocks, user.id);
        });
    }
}

export default new SpentTimeService();
