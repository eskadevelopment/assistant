import { Action, Route, State } from 'assistant';
import { WORKSPACE } from '../../../../drivers/slack/src/configs';
import { Type } from '../../../../drivers/slack/src/types';
import { Block } from '../../../../drivers/slack/src/model/block';
import { protocol } from '../../src/protocol';

const slackState = new State(new Route('seq', [new Action('SEND', 'driver.slack')]), {});

export class SlackTransport {

    send(workspace: WORKSPACE, type: Type, blocks: Array<Block>, channel: string) {
        protocol.start(slackState, {
            workspace: workspace,
            type: type,
            blocks: blocks.map(block => block.serialize()),
            channel: channel
        });
    };
}
