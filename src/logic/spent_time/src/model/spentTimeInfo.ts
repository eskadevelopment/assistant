export class SpentTimeInfo {

    slackId: string;
    date: Date;
    hours: number;
    cardId: string;

    constructor(slackId: string, date: Date, hours: number, cardId: string) {
        this.slackId = slackId;
        this.date = date;
        this.hours = hours;
        this.cardId = cardId;
    }
}
