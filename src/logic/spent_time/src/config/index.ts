enum UserRole {
    QA = 1,
    DEV = 2,
    ENG = 3,
    BA = 4,
    UX = 5,
    SALE = 6,
    BOSS = 7,
    ACC = 8,
    OTH = 9,
    HR = 10,
}


export const USER_ROLES = [
    UserRole.QA,
    UserRole.DEV,
    UserRole.BA,
    UserRole.UX,
    UserRole.ENG,
];

export const TRELLO_PREFIX = 'https://trello.com/c/';

