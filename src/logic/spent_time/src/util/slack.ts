import uuidv4 from 'uuid/v4';
import { Block } from '../../../../drivers/slack/src/model/block';
import { Type } from '../../../../drivers/slack/src/types';
import { Accessory } from '../../../../drivers/slack/src/model/accessory';
import { Option } from '../../../../drivers/slack/src/model/option';
import { Text } from '../../../../drivers/slack/src/model/text';
import { Card } from '../model/slack/card';
import { TRELLO_PREFIX } from '../../src/config';
import { getDateString } from '../util/date';

// TODO: think about OOP
export const createSlackData = (cards: Array<Card>, date: Date): Array<Block> => {
    const convertedDate = getDateString(date);
    const header = createHeader(`Hello! Please, choose spent time for *${convertedDate}*! Here is your cards:`);
    const cardBlocks = cards.map((card: Card) => {
        const blockId = `${card.id}|${card.name}|${date}`;
        return createBlock(TRELLO_PREFIX + card.id, card.name, blockId);
    });
    return [header, ...cardBlocks];
};

export const createBlock = (cardLink: string, cardName: string, blockId: string = uuidv4()): Block => {
    return new Block({
        type: Type.SECTION,
        text: createLinkCard(cardLink, cardName),
        accessory: getAccessory('hours'),
        blockId: blockId,
    });
};

export const createLinkCard = (cardLink: string, cardName: string) => {
    return new Text({ type: Type.MRKDWN, text: `*<${cardLink}|${cardName}>*` });
};

export const getAccessory = (name: string) => {
    let placeholder: Text = new Text({ type: Type.PLAIN_TEXT, emoji: true, text: name });
    return new Accessory({
        type: Type.STATIC_SELECT,
        placeholder: placeholder,
        options: getHoursOptions(24),
        actionId: 'selectTimeSpent',
    });
};

export const getHoursOptions = (hours: number): Array<Option> => {
    const options: Array<Option> = [
        new Option({ text: new Text({ type: Type.PLAIN_TEXT, emoji: true, text: '0' }), value: '0' }),
        new Option({ text: new Text({ type: Type.PLAIN_TEXT, emoji: true, text: '0.5' }), value: '0.5' })
    ];
    for (let i = 1; i <= hours; i++) {
        let optionText: Text = new Text({ type: Type.PLAIN_TEXT, emoji: true, text: `${i}` });
        options.push(new Option({ text: optionText, value: `${i}` }));
    }
    return options;
};

export const createHeader = (text: string): Block => {
    return new Block({
        type: Type.SECTION,
        text: { type: Type.MRKDWN, text: text },
        blockId: 'header'
    });
};
