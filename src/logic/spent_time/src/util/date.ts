export const getDateString = (date: Date): string => {
    let convertedMonth;
    const month = date.getMonth() + 1;

    month < 10
        ? convertedMonth = `0${month}`
        : convertedMonth = month;

    return date.getDate() + '.' + convertedMonth + '.' + date.getFullYear();

};
