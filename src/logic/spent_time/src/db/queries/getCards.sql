with card_ids as (
    SELECT obj_id, user_id
    FROM history
    WHERE history.status in (4, 5)
      AND obj_type_id = 3
      AND history.timestamp between $2 and $3
    UNION
    select obj_id, user_id
    from history
    where obj_type_id = 3
      and history.timestamp between date_trunc('day', now()) - interval '2 week' and $3
    group by obj_id, user_id
    HAVING last(status order by timestamp) = 3
)
SELECT slack_id, email, array_agg(json_build_object('name', card.name, 'id', card.id)) as cards
FROM "user"
         JOIN card_ids ON "user".id = card_ids.user_id
         LEFT JOIN card on card_ids.obj_id = card.id
         JOIN user_to_role utr on "user".id = utr.user_id
where utr.role_id in ($1:csv)
GROUP BY slack_id, email;
