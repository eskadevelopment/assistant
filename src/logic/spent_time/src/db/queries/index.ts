import path from 'path';
import fs from 'fs';

const DIR = path.resolve(__dirname);

export const getQuery = (fileName: string) => {
    const queryPath = path.resolve(DIR, fileName);
    return fs.readFileSync(queryPath, 'utf8');
};

export const createSpentTime = '' +
    'INSERT INTO spent_time' +
    ' VALUES (${slackId}, ${date}, ${hours}, ${cardId})' +
    ' ON CONFLICT(slack_id, date, card_id) DO UPDATE' +
    ' SET hours = ${hours}';
