import { SlackUser } from '../../model/slack/slackUser';
import { Card } from '../../model/slack/card';

export interface DataExtractor<T> {
    extract(data: any): T;
}

export class Converter implements DataExtractor<Map<SlackUser, Array<Card>>> {

    extract(data: any): Map<SlackUser, Array<Card>> {
        const allUsersCards = new Map<SlackUser, Array<Card>>();
        data.forEach(item => {
            const user = new SlackUser(item.slack_id, item.email);
            const userCards = item.cards;
            allUsersCards.set(user, userCards);
        });
        return allUsersCards;
    }
}

export default new Converter();
