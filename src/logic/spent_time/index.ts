#! /usr/bin/env ts-node
import { State } from 'assistant';
import { createSpentTime, getQuery } from './src/db/queries';
import { USER_ROLES } from './src/config';
import { protocol } from './src/protocol';
import { SpentTimeInfo } from './src/model/spentTimeInfo';
import { SlackUser } from './src/model/slack/slackUser';
import { Card } from './src/model/slack/card';
import spentTimeService from './src/service/spentTimeService';
import dataExtractor from './src/db/converter';

class SpentTimeController {

    @protocol.action('SPENT_TIME')
    async getSpentTime(data: any, state: State) {
        const { start, end } = state.data.spentTimeDate;
        return {
            query: getQuery('getCards.sql'),
            args: [USER_ROLES, start, end],
            name: 'bpm',
        };
    }

    @protocol.action('PROCESS')
    async process(data: any, state: State) {
        // TODO: optimize this
        const convertedCards: Map<SlackUser, Array<Card>> = dataExtractor.extract(data);
        const businessDayDateTime = new Date(state.data.spentTimeDate.start);
        spentTimeService.process(convertedCards, businessDayDateTime);
    }

    @protocol.action('PERSIST')
    async persist(data: SpentTimeInfo, state: State) {
        return {
            query: createSpentTime,
            args: data,
            name: 'bpm'
        };
    }
}
