from typing import List, Dict

from protocol.protocol import Protocol, State

p = Protocol('logic.parser')


@p.action('PARSE_COMMITS_TO_MESSAGE')
def parse_commits_to_message(data: List[Dict], state: State):
    print('PARSE_COMMITS_TO_MESSAGE', data)
    return '\n'.join(filter(lambda x: not x.startswith('Merge'), [commit['message'] for commit in data]))


p.run_consumer_worker()
