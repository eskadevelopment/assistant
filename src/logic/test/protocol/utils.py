from typing import TypeVar, List

_T = TypeVar('_T')


def find(key_func, iterable: List[_T]) -> _T:
    for index, item in enumerate(iterable):
        if key_func(item):
            return item, index
    return None, -1
