from typing import Any

from protocol.protocol import Protocol, State, Action

p = Protocol('logic.test1')


@p.action('FUNC_1')
def func1(data: Any, state: State):
    print(1, data)
    state.a = 1
    return [2, 3, 4]


@p.action('FUNC_2')
def func2(data: Any, state: State):
    print(3, data)
    state.a = 3
    return [3]


# if __name__ == '__main__':
#     s = State([
#         Action('FUNC_1', 'logic.test1'),
#         Action('FUNC_2', 'logic.test2'),
#         Action('FUNC_1', 'logic.test1'),
#         Action('FUNC_2', 'logic.test1'),
#     ])
#     p.start(s, {})
p.run_consumer_worker()
