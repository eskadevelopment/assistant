from typing import Any

from protocol.protocol import Protocol, State

p = Protocol('logic.test2')


@p.action('FUNC_2')
def func2(data: Any, state: State):
    print(2, data)
    state.a = 2


p.run_consumer_worker()
