ALTER TABLE history ADD status SMALLINT NOT NULL DEFAULT 1;

UPDATE history SET status = 1 WHERE action = 1;
UPDATE history SET status = 3 WHERE action = 2;
UPDATE history SET status = 4 WHERE action = 3;
UPDATE history SET status = 3 WHERE action = 4;
UPDATE history SET status = 5 WHERE action = 5;

ALTER TABLE history DROP COLUMN action;
ALTER TABLE process ADD COLUMN label_subprocess_trello_id CHAR (24);
ALTER TABLE process ADD COLUMN label_testing_trello_id CHAR (24);
