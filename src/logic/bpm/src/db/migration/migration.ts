import { Route, State } from 'assistant';
import { Action } from 'assistant';
import { p } from '../../../test/common';

const idToBoardId = {
    '5b2ba4c60b20986cc788e42a': null,
    '598855e9bc91f5b29518de7b': '5b2bc021fb197f90e5985f02',
    '5a97c9575c8c1c0a2965c8a9': '5b2bc02ee1fbda19edc435b3',
    '54eec73d02890b315117d862': '5b2bc03bc6661f846dfa1eb5',
    '5b1a9836281421732031d38a': '5b2bc05552fbae0b4ad1af6b',
    '5988848bea210bc5029f3257': '5b27c61e1e4587ac3094c9ed',
    '5b1a97ff650a2df288ba30aa': '5b2bc06181bd13f8b8795e85',
    '5ad46daee7922e8beef4b2dd': '5b2a5262e68f113ba69799fd',
    '59c245977d878b3c671dbd75': '5b2bc06e7e77f858d0fc73b7',
    '59c37bd9ef69947bd836e819': '5b2bc086a61b21c4ecdfbd88',
    '59b7a01c01f086ef2e70a625': '5b2bc0a0201d627fdf51e7e8',
    '5b1e1fc73c6bd5369260e548': '5b2bc0a8fd4d27f43e881dac',
    '5b17979192b69a9785f25a42': '5b2bc0b63fe9eb6b788ec40c',
    '5a16a29feee3883a445fe6c8': '5b2bc0c31e43bcd7eeb1d15b',
    '59a95a1b324a42804363842c': '5b2bc0c99f77d3cc4990983d',
    '59883caca282c5b7b73d82c1': '5b2bc0e2eb5c8f5f7ce7bb35',
    '5ad46d5e9c0b24c89d289c74': '5b2bc10b961d13f3dd7ebc15',
    '5a9822658ab55f08df7e58bd': '5b2bc12068ee1aacf5071dd4',
    '598da07cea25ef7aefd6c114': '5b2bc12dc7fb2b63b1968b21',
    '5b1a9763fe67809b4f86d529': '5b2bc1457017174f9c1286fd',
    '5ad46e3e77944b832bb7c950': '5b2bc1546af65a54026e7c67',
    '583c28cafa55aeb3870e6318': '5b2bc161110042c4f1d5894f',
    '57e0e2b146e580c39d02db7e': '5b27bf07955e1d12ac2f7a37',
    '59dde323a0ec53e5fc0e4403': '5b2bc182903c42d26b370cd4',
    '5b4369d1cfcb6f3d454b84de': '5b447ca1e7d46e69f659ecdf',
    '5b169e33bf001ed714ed2c65': '5b2bc172ff75c2772c5747c0',
    '5b3f6bb506d5d21bb9fa3673': '5b472fb04f6b73efb3b916ea',
    '5a15a42becd6383f197cce7d': '5b2bb87cf8bb680fb67658e3',
    '5b1e195882a478e108b5b5e6': '5b2bc09388229a6d0de0f136',
    '58ef269abb30d57349127dd2': '5b2bc13bf31a3804292e36dd',
    '5b5ec09700ef196322d9aa5a': '5b4c4f73e9db0f08c975c29a',
    '5b718410aadc216dd145bc37': '5b718513cda2338be2816384',
    '5b7a938726aa708a071359d4': '5b790b095bb86110b4e92554',
    '5b6c117ff126494d08be3eb2': '5b6aa568f402396f5f06c1fc',
    '5b8d270ce106055658f97d8a': '5b8d381a7bc14614e6e9ade7',
    '5bac88e4a600ed58855cbfca': '5bb1f627bf159d602d9ee5a7',
    '5bb1d8a86667941df2e50865': '5bb202f792426b69968b7ad6',
    '5bb22a66cfc35e4adb5426e6': '5bb4a2eaba2a6c850de578eb',
    '5bdad7a8c6f9185d41e1f52d': '5bdbd26d7da7228a6d7651a9',
    '5bae158297ff191833d0f3ce': '5bf403f7a2706b40cabb468a',
    '5b90dff8e022c1267667d5f1': '5b8d381a7bc14614e6e9ade7',
    '5ba361a11570627368a17ae6': '5c0e973759dc886c2c4859b7',
    '5bb1bab0cc5a7d2d27234a86': '5c0fb9741ffb801e2f3af44a',
    '5b572b835fe211a044ff6f0a': '5b4c5011675dbd8635684d49',
    '5c59ad1db3952911a3a93065': '5c5aff377d66284542e76e12',
    '5ba2102ea028235edc44120c': '5c5aff7b0e0a0e6516a737d8',
    '5c6a518e25b8c05dcd4cb57f': '5c77a8e48b7a9d425a65de4b',
    '5c73f886a7e78b52172cc07b': '5c77a91fbba3a834d318fc5c',
    '5c7d5086a11c2e08a6162553': '5c7e68bf02588559ba88e2c1',
    '5c9b3479be91bf318a6475e6': '5cac7dd718d5944e45249026',
    '5ca355d5b95d5b0b19db1b21': '5cac7e1143a20a251a975724',
    '5cab312004fe707deba986d0': '5cb833d5cc7f935ad6f7bc3a',
    '5cda6737d3109586faeb5276': '5cdab74a72243768c1c03770',
    '5cefa8653acdb55835c3f461': '5cf1491d929a4939b486227f',
    '5d0872a35916672fcf503382': '5d0891662a884055fbdeda4c',
    '5d108f6db401656ab7827491': '5d10b043c4a40084d08abd39',
    '5d19ed6b108fa771be633d55': '5d1bab0043f1920209a69261',
    '5d3084ff2c1a8284ab13d894': '5d3700f657518314c5408f8c',
    '5d39bb898282c345d5698b46': '5d39bd23af4f790550aeb9b5',
    '5d53d6cb015b6351abc75beb': '5d5a4f2e1b878676a1fc6e28',
    '5d42dd261625e157df8fea09': '5d5a8ab9caaaee6b468d3e9f',
    '5d5bb8d56c54b52d8bff3f2e': '5d5d5f785e73a636f74171a2',
};


const s = new State(new Route('seq', [
    new Action('HOOK:GET_ALL', 'driver.trello'),
    new Route('par',
        Array.from(new Array(121)).map(_ => new Action('HOOK:DELETE', 'driver.trello'))
    ),
    new Action('ORG:MEMBERS:GET_ALL', 'driver.trello', { id: 'skconsulting1' }),
    new Action('TEST:MIGRATION', 'test'),
    new Route('par',
        Array.from(new Array(41)).map(_ => new Action('HOOK:CREATE', 'driver.trello'))
    ),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test')
]));

class Migration {

    @p.action('TEST:MIGRATION')
    newHookData(data: Array<{ id: string, fullName: string }>, state: State) {
        return data.map(elem => ({
            callbackURL: `https://bpm.skc.today/trello/events/user_board_hook`,
            idModel: idToBoardId[elem.id],
            description: `${elem.fullName} user board hook`
        })).filter(elem => elem.idModel);
    }

}

p.start(s);
