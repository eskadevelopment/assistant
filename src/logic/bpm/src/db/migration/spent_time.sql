create table spent_time
(
	slack_id varchar(64),
	date timestamp,
	hours integer default 0,
	card_id char(24)
		constraint spent_time_card_id_fk
			references card
				on update cascade on delete cascade
);

alter table spent_time owner to postgres;

create unique index spent_time_slack_id_date_card_id_uindex
	on spent_time (slack_id, date, card_id);

INSERT INTO public.role (id, name, timestamp) VALUES (1, 'QA', '2019-07-10 12:53:05.687107');
INSERT INTO public.role (id, name, timestamp) VALUES (2, 'DEV', '2019-07-10 12:53:21.595091');
INSERT INTO public.role (id, name, timestamp) VALUES (3, 'ENG', '2019-07-10 12:53:33.434020');
INSERT INTO public.role (id, name, timestamp) VALUES (4, 'BA', '2019-07-10 12:53:44.417547');
INSERT INTO public.role (id, name, timestamp) VALUES (5, 'UX', '2019-07-10 12:53:53.574837');
INSERT INTO public.role (id, name, timestamp) VALUES (6, 'SALE', '2019-07-10 12:54:01.489721');
INSERT INTO public.role (id, name, timestamp) VALUES (7, 'BOSS', '2019-07-10 12:54:07.579919');
INSERT INTO public.role (id, name, timestamp) VALUES (8, 'ACC', '2019-07-10 13:03:20.310930');
INSERT INTO public.role (id, name, timestamp) VALUES (9, 'OTH', '2019-07-10 13:09:40.996144');
INSERT INTO public.role (id, name, timestamp) VALUES (10, 'HR', '2019-07-10 13:10:57.100427');
