SELECT "user".id,
       fname           as name,
       username,
       email,
       phone,
       slack_id        as "slackId",
       trello_board_id as "trelloBoardId",
       r.name as role,
       0.25 as power,
       0.1 as "regularPart",
       0.20 as "calculatedPower"
from "user"
left join user_to_role utr on "user".id = utr.user_id
left join role r on utr.role_id = r.id
where "user".enabled = true
order by r.name NULLS LAST;
