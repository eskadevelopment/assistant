with RECURSIVE pp as (
    select id,
           name,
           parent_id       as project,
           '{}'::varchar[] as parents,
           0               as level
    from process
    where parent_id = any (
            '{}'::varchar[] ||
            (select array_agg(process.id::varchar)
             from process
                      left join source_board s on s.id = process.parent_id
             where code = '000000')
        )
    union ALL
    select process.id, process.name, project, parents || process.parent_id::varchar, level + 1
    from pp
             join process on process.parent_id = pp.id
    where not process.id = any (parents)
)
select card.id,
       card.name,
       case when project.name is not null then p.name else card.name end                                    as feature,
       -- 1: backlog, 2: to_do, 3: WIP, 4: pending, 5: done
       (select last(status order by history.timestamp) from history where obj_id = card.id group by obj_id) as status,
       coalesce(
               project.name,
               (select case when code != '000000' then name else p.name end
                from source_board
                where (id = p.parent_id or id = p.id)),
               'OTHER'
           )                                                                                                as project,
       coalesce(
               project.id,
               (select case when code != '000000' then id else p.id end
                from source_board
                where (id = p.parent_id or id = p.id))
           )                                                                                                as "projectId",
       "user".fname                                                                                         as assignee,
       "user".id                                                                                            as "userId",
       "user".trello_board_id                                                                               as "trelloBoardId",
       role.name                                                                                            as department,
       '-'                                                                                                  as estimate,
       (select sum(hours) from spent_time where card_id = card.id)                                          as "spentTime",
       count(1) over ()                                                                                     as count
from card
         left join "user" on "user".id = card.user_id
         left join process p on p.id = card.process_id
         left join pp on pp.id = card.process_id
         left join process project on project.id = pp.project
         left join user_to_role on "user".id = user_to_role.user_id
         left join role on user_to_role.role_id = role.id
    {{filter}}
    {{sort}}
    {{paginator}};
