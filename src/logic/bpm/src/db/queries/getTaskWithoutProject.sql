with RECURSIVE pp as (
  select id,
         name,
         id       as project,
         '{}'::varchar[] as parents,
         0               as level
  from process
  where id = any (
      '{}'::varchar[] ||
      (select array_agg(process.id::varchar)
       from process
              left join source_board s on s.id = process.parent_id
       where code = '000000')
      || (select array_agg(source_board.id::varchar)
       from source_board where code != '000000')
    )
  union ALL
  select process.id, process.name, project, parents || process.parent_id::varchar, level + 1
  from pp
         join process on process.parent_id = pp.id
  where not process.id = any (parents)
)
select card.name,
       sum(spent_time.hours)           as "spentTime"
from card
       left join pp on pp.id = card.process_id
       left join process project on project.id = pp.project
       left join spent_time on spent_time.card_id = card.id
where pp.name is null and spent_time.date > date_trunc('month', now())
GROUP BY card.id
HAVING sum(spent_time.hours) is not null;
