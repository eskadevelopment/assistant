import path from 'path';
import fs from 'fs';
import { Query, SQLFilter, SQLPaginator, SQLSort } from './ORM';

const DIR = path.resolve(__dirname);

export const getQuery = (fileName: string) => {
    const queryPath = path.resolve(DIR, fileName);
    return fs.readFileSync(queryPath, 'utf8');
};

export const getLabels = getQuery('./getLabels.sql');

export const getCardDataForHistory = getQuery('./getCardDataForHistory.sql');

export const addHistory = getQuery('./addHistory.sql');

export const getParentProcessInfo = getQuery('./getParentProcessInfo.sql');

export const getWIPTasks = getQuery('./getWIPTasks.sql');

export const getAllTasks = (filter: SQLFilter, sort: SQLSort, paginator: SQLPaginator) =>
    new Query(getQuery('./getAllTasks.sql'), { filter, sort, paginator }).getQuery();

export const getTimeSpentReport = getQuery('./getTimeSpentReport.sql');

export const getUsers = getQuery('./getUsers.sql');
export const getProjects = getQuery('./getProjects.sql');
export const getProjectsForFilter = getQuery('getProjectsForFilter.sql');

export const getResources = getQuery('./getResources.sql');
