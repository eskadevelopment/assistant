WITH task_WIP as (
  select obj_id, user_id
  from history
  where obj_type_id = 3
    and history.timestamp between date_trunc('day', now()) - interval '2 week' and now()
  group by obj_id, user_id
  HAVING last(status order by timestamp) = 3
)
select json_build_object('name', u.fname, 'trelloBoardId', u.trello_board_id, 'role', role.name) as "user",
       array_agg(json_build_object('name', c.name, 'id', c.id)) as tasks
from task_WIP
       LEFT JOIN "user" u on task_WIP.user_id = u.id
       LEFT JOIN user_to_role on user_to_role.user_id = u.id
       LEFT JOIN role on user_to_role.role_id = role.id
       LEFT JOIN card c on c.id = task_WIP.obj_id
GROUP BY u.fname, u.trello_board_id, role.name
ORDER BY role.name NULLS LAST;
