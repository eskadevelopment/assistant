SELECT "user".id,
       fname           as name,
       username,
       email,
       phone,
       slack_id        as "slackId",
       trello_board_id as "trelloBoardId",
       r.name as role
from "user"
left join user_to_role utr on "user".id = utr.user_id
left join role r on utr.role_id = r.id
where "user".enabled = true
order by fname NULLS LAST;
