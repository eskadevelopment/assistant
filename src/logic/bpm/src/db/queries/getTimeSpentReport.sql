select json_build_object(
               'name', u.fname,
               'trelloBoardId', u.trello_board_id,
               'role', role.name
           )  as "user",
       array_agg(json_build_object(
               'name', c.name,
               'id', c.id,
               'spentTime', spent_time.hours
           )) as tasks
from spent_time
         LEFT JOIN card c on spent_time.card_id = c.id
         LEFT JOIN "user" u on spent_time.slack_id = u.slack_id
         LEFT JOIN user_to_role on user_to_role.user_id = u.id
         LEFT JOIN role on user_to_role.role_id = role.id
where date = date_trunc('day', ${date}::date)
GROUP BY u.fname, u.trello_board_id, role.name
order by role.name NULLS LAST;
