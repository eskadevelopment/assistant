export class Query {

    private readonly query: string;

    constructor(rawQuery: string, params?: {
        filter?: SQLFilter,
        sort?: SQLSort,
        paginator?: SQLPaginator
    }) {
        let preparedQuery = rawQuery;
        if (params.filter) {
            preparedQuery = params.filter.build(preparedQuery);
        }

        if (params.sort) {
            preparedQuery = params.sort.build(preparedQuery);
        }

        if (params.paginator) {
            preparedQuery = params.paginator.build(preparedQuery);
        }

        // TODO dor debug
        // console.log(preparedQuery);
        this.query = preparedQuery;
    }

    getQuery(): string {
        return this.query;
    }
}

export interface QueryPart {
    getQueryPart(): string;

    build(query: string): string;
}


export class FilterPredicate {

    private readonly filterFields: Array<FilterPredicate | string>;
    protected separator: string;

    constructor(...filterFields: Array<FilterPredicate | string>) {
        this.filterFields = filterFields;
        this.separator = ' ';
    }

    getQuery() {
        return this.filterFields.map(
            field => typeof field === 'string' ? field : `(${field.getQuery()})`
        ).join(this.separator);
    }
}

export class Or extends FilterPredicate {

    constructor(
        firstField: FilterPredicate | string,
        secondField: FilterPredicate | string,
        ...additionalFields: Array<FilterPredicate | string>) {
        super(firstField, secondField, ...additionalFields);
        this.separator = ' or ';
    }

}

export class And extends FilterPredicate {
    constructor(
        firstField: FilterPredicate | string,
        secondField: FilterPredicate | string,
        ...additionalFields: Array<FilterPredicate | string>) {
        super(firstField, secondField, ...additionalFields);
        this.separator = ' and ';
    }
}

export class SQLFilter implements QueryPart {

    constructor(private filterPredicate?: FilterPredicate | string) {
    }

    getQueryPart() {
        return this.filterPredicate
            ? ` WHERE ${typeof this.filterPredicate === 'string'
                ? this.filterPredicate
                : this.filterPredicate.getQuery()}`
            : '';
    }

    build(query: string): string {
        return query.replace('{{filter}}', this.getQueryPart());
    }
}

export abstract class SortPredicate {

    protected constructor(protected sortField: string) {
    }

    abstract getQuery(): string;
}

export class Asc extends SortPredicate {

    constructor(sortField: string) {
        super(sortField);
    }

    getQuery(): string {
        return `${this.sortField} ASC`;
    }
}

export class Desc extends SortPredicate {
    constructor(sortField: string) {
        super(sortField);
    }

    getQuery(): string {
        return `${this.sortField} DESC`;
    }
}


export class SQLSort implements QueryPart {

    private readonly sortPredicates: SortPredicate[];

    constructor(...sortPredicates: SortPredicate[]) {
        this.sortPredicates = sortPredicates;
    }

    getQueryPart() {
        return this.sortPredicates.length
            ? ` ORDER BY ${this.sortPredicates.map(predicate => predicate.getQuery()).join(', ')}`
            : '';
    }

    build(query: string): string {
        return query.replace('{{sort}}', this.getQueryPart());
    }
}

export class SQLPaginator implements QueryPart {
    constructor(private limit?: number, private offset?: number) {
    }

    getQueryPart() {
        return this.limit ? ` LIMIT ${this.limit} OFFSET ${this.offset || 0}` : '';
    }

    build(query: string): string {
        return query.replace('{{paginator}}', this.getQueryPart());
    }
}

// // filter test
// const t1 = new SQLFilter(new Or('test>10', new And('test<5', 'test = 3')));
// console.log(t1.getQueryPart());
//
// const t2 = new SQLFilter();
// console.log(t2.getQueryPart());
//
// const t3 = new SQLFilter('test > 20');
// console.log(t3.getQueryPart());
//
// // sort test
// const s1 = new SQLSort(new Desc('test'), new Asc('filed'));
// console.log(s1.getQueryPart());
//
// const s2 = new SQLSort();
// console.log(s2.getQueryPart());
//
// // paginator test
// const p1 = new SQLPaginator(10);
// console.log(p1.getQueryPart());
//
// const p2 = new SQLPaginator(10, 20);
// console.log(p2.getQueryPart());
//
// const p3 = new SQLPaginator(0);
// console.log(p3.getQueryPart());
