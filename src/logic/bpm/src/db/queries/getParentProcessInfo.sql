with sub as (
    select parent_card_id,
           parent_id
    from process
    where id = ${processId}
)
select label_done_trello_id, label_in_progress_trello_id, sub.parent_card_id
from process,
     sub
where process.id = sub.parent_id;
