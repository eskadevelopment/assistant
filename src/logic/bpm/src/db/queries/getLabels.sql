SELECT label_done_trello_id as "doneLabelId",
    label_in_progress_trello_id as "inProgressLabelId",
    label_pending_trello_id as "pendingLabelId",
    card.id as "id"
FROM process
    LEFT JOIN card ON process.id = card.process_id
WHERE card.user_board_id = ${cardId};
