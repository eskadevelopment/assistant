// TODO: Migrate to trello lib
export const SLACK_WORKSPACE = 'sk-consultingteam';

export type TrelloCard = { id: string, name: string, idMembers: Array<string>, pos: number, idList: string };

export const SOURCE_BOARD_ID = '5b17f86b1dbbc85f594e1c4c';
export const CUSTOM_FILED_PLUGIN_ID = '56d5e249a98895a9797bebb9';

// export const ORGANIZATION_ID = '5ba5126204c0df5b4b63b923'; // test
// export const BPM_PLUGIN_ID = '5ba513b889f21e5db6864ab1'; // test
// export const HOST = 'https://dmehed.skc.today'; // test
export const HOST = 'https://bpm.skc.today'; // sk-consulting1
export const ORGANIZATION_ID = '59883aae64b522c3f76fc8cd'; // sk-consulting1
export const BPM_PLUGIN_ID = '5b17c83f3ab88ddf8e3c2c27'; // sk-consulting1

export enum User {
    DMEHED = '57e0e2b146e580c39d02db7e',
    ISKRIPKA = '59b7a01c01f086ef2e70a625',
    HALYNA = '5bb22a66cfc35e4adb5426e6'
}

export enum ProcessStatus {
    BACKLOG = 1,
    TODO,
    IN_PROGRESS,
    PENDING,
    DONE,
}

export enum ObjectType {
    PROCESS = 1,
    STAGE,
    STEP,
}

export const DB_NAME = 'bpm';
export enum ProgressLabel {
    WIP,
    Pending,
    Done,
}

export enum CardStatus {
    CLARIFY = 'CLARIFY',
    DEV = 'DEV',
    TEST = 'TEST',
    DONE = 'DONE',
}
