import { p } from '../protocol';
import { State } from 'assistant';

export enum UtilAction {
    STORE_IN_STATE = 'STORE_IN_STATE',
}

export class UtilController {
    @p.action(UtilAction.STORE_IN_STATE)
    storeInState(data: { variable: string, fields?: Array<string>, data: any }, state: State) {
        if (typeof data.data === 'object' && data.data.constructor === Object && data.data !== null) {
            let fields = {};
            if (data.fields) {
                const filteredFields = Object.keys(data.data).filter(key => data.fields.indexOf(key) !== -1);
                const objWithFilteredFields = {};
                filteredFields.forEach(key => {
                    objWithFilteredFields[key] = data.data[key];
                });
                fields = objWithFilteredFields;
            } else {
                fields = data.data;
            }
            // TODO make recursive read from variable like board.list.nextStep
            state.data[data.variable] = { ...state.data[data.variable], ...fields };
        } else {
            state.data[data.variable] = data.data;
        }
        return data.data;
    }
}
