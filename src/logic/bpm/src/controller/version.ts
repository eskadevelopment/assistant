import { p } from '../protocol';
import { Action, Route, State } from 'assistant';
import { Executors } from '../../executors';
import { DB_NAME } from '../const';

export enum VersionAction {
    UPDATE_VERSION = 'UPDATE_VERSION',
}

const INCAPABILITY_VERSIONS = [
    '1.0.2',
    '1.0.1',
    '1.0.0'
];

export class VersionController {

    @p.action(VersionAction.UPDATE_VERSION)
    updateVersion(data: { oldVersion: string, boardId: string }, state: State) {
        const incapabilityVersionList = INCAPABILITY_VERSIONS
            .slice(0, INCAPABILITY_VERSIONS.findIndex(version => version === data.oldVersion));
        p.start(new State(new Route('seq',
            incapabilityVersionList
                .reverse()
                .map(version => new Action('UPDATE_VERSION_' + version, Executors.BPM, {
                        boardId: data.boardId
                    })
                )
        )));
    }

    @p.action('UPDATE_VERSION_1.0.1')
    updateVersion1_0_1(data: { boardId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: 'select label_subprocess_trello_id, label_testing_trello_id from process where id = ${id}',
                args: {
                    id: data.boardId
                },
                name: DB_NAME
            }),
            new Action('UPDATE_VERSION_1.0.1_1', Executors.BPM)
        ]), {
            boardId: data.boardId
        });
        p.start(s);
    }

    @p.action('UPDATE_VERSION_1.0.1_1')
    updateVersion1_0_1_1(data: { label_subprocess_trello_id: string, label_testing_trello_id: string }, state: State) {
        if (!data) {
            return;
        }

        const route = new Route('par', []);

        if (!data.label_subprocess_trello_id) {
            route.route.push(new Route('seq', [
                new Action('LABEL:CREATE', Executors.TRELLO, {
                    boardId: state.data.boardId,
                    name: 'subprocess',
                    color: 'sky'
                }),
                new Action('PREPARE_QUERY_UPDATE_PROCESS_FILED_SET_ID_BY_ID', Executors.BPM, {
                    id: state.data.boardId,
                    field: 'label_subprocess_trello_id',
                    boardId: state.data.boardId
                }),
                new Action('EXECUTE', Executors.PG)
            ]));
        }

        if (!data.label_testing_trello_id) {
            route.route.push(new Route('seq', [
                new Action('LABEL:CREATE', Executors.TRELLO, {
                    boardId: state.data.boardId,
                    name: 'Testing',
                    color: 'lime'
                }),
                new Action('PREPARE_QUERY_UPDATE_PROCESS_FILED_SET_ID_BY_ID', Executors.BPM, {
                    id: state.data.boardId,
                    field: 'label_testing_trello_id',
                    boardId: state.data.boardId
                }),
                new Action('EXECUTE', Executors.PG)
            ]));
        }

        const s = new State(new Route('seq', [route]));
        p.start(s);
    }

    @p.action('PREPARE_QUERY_UPDATE_PROCESS_FILED_SET_ID_BY_ID')
    updateProcessFieldSetIdById(data: { boardId: string, field: string, data: { id: string } }, state: State) {
        return {
            query: `update process set ${data.field} = \${value} where id= \${id}`,
            args: {
                value: data.data.id,
                id: data.boardId
            },
            name: DB_NAME
        };
    }

    @p.action('UPDATE_VERSION_1.0.2')
    updateVersion1_0_2(data: { boardId: string }, state: State) {
        console.log('updateVersion1_0_2');
    }
}
