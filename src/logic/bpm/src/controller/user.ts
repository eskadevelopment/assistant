import { p } from '../protocol';
import { Action, Route, State } from 'assistant';
import {
    BPM_PLUGIN_ID,
    CUSTOM_FILED_PLUGIN_ID,
    DB_NAME,
    HOST,
    ORGANIZATION_ID,
    SLACK_WORKSPACE,
    SOURCE_BOARD_ID,
    User
} from '../const';
import { Executors } from '../../executors';
import { UtilAction } from './util';

export enum UserAction {
    CREATE = 'USER:CREATE',
    DISABLE = 'USER:DISABLE'
}

enum UserPrivateAction {
    USER_CREATE_PREPARE_MESSAGE = 'USER:CREATE:PREPARE_MESSAGE',
    USER_CREATE_PREPARE_CREATE_QUERY = 'USER:CREATE:PREPARE_CREATE_QUERY',
    PREPARE_PLUGIN_DATA = 'USER:CREATE:PREPARE_PLUGIN_DATA',
    PREPARE_CUSTOM_FIELD = 'USER:CREATE:PREPARE_CUSTOM_FIELD',
    PREPARE_HOOK_DATA = 'USER:CREATE:PREPARE_HOOK_DATA'
}

class UserController {

    @p.action(UserAction.CREATE)
    async create(
        data: { slackId: string, email: string, data: { username: string, fullName: string, id: string } }, state: State
    ) {
        const s = new State(new Route('seq', [
            new Action('BOARD:CREATE', Executors.TRELLO),
            new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'board', fields: ['id'] }),
            new Route('par', [
                new Route('seq', [
                    new Action(UserPrivateAction.PREPARE_PLUGIN_DATA, Executors.BPM),
                    new Route('par', [
                        new Action('BOARD:PLUGIN:ADD', Executors.TRELLO),
                        new Action('BOARD:PLUGIN:ADD', Executors.TRELLO),
                    ]),
                    new Action(UserPrivateAction.PREPARE_CUSTOM_FIELD, Executors.BPM),
                    new Action('CUSTOM_FIELD:CREATE', Executors.TRELLO),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'customField', fields: ['id'] }),
                ]),
                new Route('seq', [
                    new Action('FUNC', Executors.PROCESS, {
                        func: ((args: { id: string, users: Array<string> }, innerState: State) => (
                            args.users.map(memberId => ({ boardId: args.id, memberId, type: 'admin' }))
                        )).toString(),
                        args: {
                            users: [
                                data.data.id,
                                User.DMEHED,
                                User.ISKRIPKA // ivan TODO change to User.HALYNA for tets
                            ]
                        }
                    }),
                    new Route('par', [
                        new Action('BOARD:ADD_MEMBER', Executors.TRELLO),
                        new Action('BOARD:ADD_MEMBER', Executors.TRELLO),
                        new Action('BOARD:ADD_MEMBER', Executors.TRELLO),
                    ]),
                    new Action('TEST', 'logic.process'),
                ]),
                new Route('seq', [
                    new Action(UserPrivateAction.PREPARE_HOOK_DATA, Executors.BPM),
                    new Action('HOOK:CREATE', Executors.TRELLO),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'plugin', fields: ['id'] }),
                ]),
                new Route('seq', [
                    new Action(UserPrivateAction.USER_CREATE_PREPARE_MESSAGE, Executors.BPM, { slackId: data.slackId }),
                    new Action('SEND', Executors.SLACK)
                ]),
                new Route('seq', [
                    new Action('BOARD:GET', Executors.TRELLO),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, {
                        variable: 'board', fields: ['id', 'lists']
                    }),
                ])
            ]),
            new Action(UserPrivateAction.USER_CREATE_PREPARE_CREATE_QUERY, Executors.BPM, {
                id: data.data.id,
                username: data.data.username,
                fullName: data.data.fullName,
                email: data.email,
                slackId: data.slackId
            }),
            new Action('EXECUTE', Executors.PG),
            new Action('WRAP', Executors.HTTP),
            new Action('RESPONSE', Executors.HTTP)
        ]), { responseId: state.data.responseId });

        p.start(s, {
            name: data.data.fullName,
            sourceId: SOURCE_BOARD_ID,
            permission: 'private',
            organizationId: ORGANIZATION_ID
        });

        // insert user to db with board fields
    }

    @p.action(UserPrivateAction.PREPARE_PLUGIN_DATA)
    preparePluginData(data: { id: string }, state: State) {
        return [{
            boardId: data.id,
            pluginId: CUSTOM_FILED_PLUGIN_ID
        }, {
            boardId: data.id,
            pluginId: BPM_PLUGIN_ID
        }];
    }

    @p.action(UserPrivateAction.PREPARE_CUSTOM_FIELD)
    prepareProcessCustomField(data: any, state: State) {
        return {
            boardId: state.data.board.id,
            name: 'Process',
            type: 'text',
            pos: 'top',
            display: true
        };
    }

    @p.action(UserPrivateAction.PREPARE_HOOK_DATA)
    prepareHookData(data: { id: string, name: string }, state: State) {
        return {
            callbackURL: `${HOST}/trello/events/user_board_hook`,
            idModel: data.id,
            description: `${data.name} user board hook`
        };
    }

    @p.action(UserPrivateAction.USER_CREATE_PREPARE_MESSAGE)
    prepareMessage(data: { slackId: string, data: { shortUrl: string } }, state: State) {
        return {
            workspace: SLACK_WORKSPACE,
            channel: data.slackId,
            text: `Для вас была создана личная доска в Trello, ${data.data.shortUrl}`
        };
    }

    @p.action(UserPrivateAction.USER_CREATE_PREPARE_CREATE_QUERY)
    prepareQueryForCreateUser(data: {
        id: string,
        username: string,
        fullName: string,
        email: string,
        slackId: string
    }, state: State) {
        return {
            name: DB_NAME,
            query: `INSERT INTO "user"(id, fname, username, email, slack_id, trello_board_id,
                                       trello_board_backlog_id, trello_board_in_progress_id, trello_board_pending_id,
                                       trello_board_done_id, process_custom_field_id)
                    VALUES (\${id},
                            \${fullName},
                            \${username},
                            \${email},
                            \${slackId},
                            \${userBoardId},
                            \${trelloBoardBacklogId},
                            \${trelloBoardInProgressId},
                            \${trelloBoardPendingId},
                            \${trelloBoardDoneId},
                            \${processCustomFieldId})`,
            args: {
                id: data.id,
                fullName: data.fullName,
                username: data.username,
                email: data.email,
                slackId: data.slackId,
                userBoardId: state.data.board.id,
                trelloBoardBacklogId: state.data.board.lists.find(list => list.name === 'Backlog').id,
                trelloBoardInProgressId: state.data.board.lists.find(list => list.name === 'In Progress').id,
                trelloBoardPendingId: state.data.board.lists.find(list => list.name === 'Pending').id,
                trelloBoardDoneId: state.data.board.lists.find(list => list.name === 'Done').id,
                processCustomFieldId: state.data.customField.id,
            }
        };
    }


    @p.action(UserAction.DISABLE)
    async disable(data: any, state: State) {
        // delete hook
        // remove from all boards
        // remove from all cards
        // add comment to card if user has been removed
    }
}
