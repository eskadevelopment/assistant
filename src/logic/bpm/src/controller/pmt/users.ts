import { p } from '../../protocol';
import { DB_NAME } from '../../const';
import { Action, Route, State } from 'assistant';
import { getUsers } from '../../db/queries';
import { Executors } from '../../../executors';

export enum UsersAction {
    GET_USERS = 'GET_USERS',
}

export class UsersController {


    @p.action(UsersAction.GET_USERS)
    async getUsers(data: {}, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_MANY', Executors.PG, {
                query: getUsers,
                args: {},
                name: DB_NAME,
            }),
            new Action('WRAP', Executors.WS),
            new Action('RESPONSE', Executors.WS),
        ]), {
            responseId: state.data.responseId
        });

        p.start(s);
    }
}
