import { p } from '../../protocol';
import { DB_NAME } from '../../const';
import { Action, Route, State } from 'assistant';
import { getProjects, getProjectsForFilter } from '../../db/queries';
import { Executors } from '../../../executors';

export enum ProjectsAction {
    GET_PROJECTS = 'GET_PROJECTS',
    GET_PROJECTS_FOR_FILTER = 'GET_PROJECTS_FOR_FILTER',
}

export class ProjectsController {


    @p.action(ProjectsAction.GET_PROJECTS)
    async getProjects(data: {}, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_MANY', Executors.PG, {
                query: getProjects,
                args: {},
                name: DB_NAME,
            }),
        ]));

        return await p.subStart(s, {});
    }

    @p.action(ProjectsAction.GET_PROJECTS_FOR_FILTER)
    async getProjectsForFilter(data: {}, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_MANY', Executors.PG, {
                query: getProjectsForFilter,
                args: {},
                name: DB_NAME,
            }),
            new Action('WRAP', Executors.WS),
            new Action('RESPONSE', Executors.WS),
        ]), {
            responseId: state.data.responseId
        });

        p.start(s);
    }
}
