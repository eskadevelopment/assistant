import { p } from '../../protocol';
import { DB_NAME } from '../../const';
import { Action, Route, State } from 'assistant';
import { getResources } from '../../db/queries';
import { Executors } from '../../../executors';

export enum ResourceAction {
    GET_RESOURCES = 'GET_RESOURCES'
}

export class ResourceController {

    @p.action(ResourceAction.GET_RESOURCES)
    async getResource(data: {}, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_MANY', Executors.PG, {
                query: getResources,
                args: {},
                name: DB_NAME,
            }),
            new Action('WRAP', Executors.WS),
            new Action('RESPONSE', Executors.WS),
        ]), {
            responseId: state.data.responseId
        });

        p.start(s);
    }
}
