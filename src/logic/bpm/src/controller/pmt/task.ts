import { p } from '../../protocol';
import { DB_NAME } from '../../const';
import { Action, Route, State } from 'assistant';
import { getAllTasks, getWIPTasks } from '../../db/queries';
import { Executors } from '../../../executors';
import { And, Asc, Desc, FilterPredicate, Or, SQLFilter, SQLPaginator, SQLSort } from '../../db/queries/ORM';

export enum TaskAction {
    GET_WIP_TASKS = 'GET_WIP_TASKS',
    GET_ALL_TASKS = 'GET_ALL_TASKS',
}

export type ComparisonOperator = '<' | '>' | '=' | '<=' | '>=' | '%*' | '*%' | '%*%';

export type RecursiveFilter<AllowedFilter = string> = {
    type: 'or' | 'and',
    filterValues: Array<[AllowedFilter, ComparisonOperator, any] | RecursiveFilter<AllowedFilter>>
};

export type BaseRequest<AllowedFilter = string, AllowedSort = string> = {
    filter: RecursiveFilter<AllowedFilter>,
    sort: Array<[AllowedSort, 'asc' | 'desc']>,
    paginator: {
        limit: number,
        offset: number
    }
};

export class TaskController {

    @p.action(TaskAction.GET_WIP_TASKS)
    async getWipTask(data: {}, state: State) {
        return await p.request('GET_MANY', Executors.PG, {
            query: getWIPTasks,
            args: {},
            name: DB_NAME,
        });
    }

    @p.action(TaskAction.GET_ALL_TASKS)
    async getAllTask(data: BaseRequest, state: State) {

        const filterParser = (frontendFilter: RecursiveFilter): FilterPredicate => {
            const filterValues = frontendFilter.filterValues.map(filterValue => {
                if (Array.isArray(filterValue)) {
                    const [field, operator, value] = filterValue;
                    let SQLOperator;
                    let SQLValue;
                    switch (operator as ComparisonOperator) {
                        case '%*%':
                            SQLValue = `%${value}%`;
                            SQLOperator = 'ILIKE';
                            break;
                        case '%*':
                            SQLValue = `%${value}`;
                            SQLOperator = 'ILIKE';
                            break;
                        case '*%':
                            SQLValue = `%${value}%`;
                            SQLOperator = 'ILIKE';
                            break;
                        default:
                            SQLValue = value;
                            SQLOperator = operator;
                    }
                    return `${field} ${SQLOperator} ${typeof value === 'string' ? `'${SQLValue}'` : SQLValue}`;
                }
                return filterParser(filterValue);
            });

            if (frontendFilter.type === 'or') {
                return new Or(filterValues[0], filterValues[1], ...filterValues.slice(2));
            }
            return new And(filterValues[0], filterValues[1], ...filterValues.slice(2));
        };

        const s = new State(new Route('seq', [
            new Action('GET_MANY', Executors.PG, {
                query: getAllTasks(
                    new SQLFilter(filterParser(data.filter)),
                    new SQLSort(...data.sort.map(sortField => sortField[1] === 'asc'
                        ? new Asc(sortField[0])
                        : new Desc(sortField[0])
                    )),
                    new SQLPaginator(data.paginator.limit || 100, data.paginator.offset || 0)
                ),
                args: {},
                name: DB_NAME,
            }),
            new Action('WRAP', Executors.WS),
            new Action('RESPONSE', Executors.WS),
        ]), {
            responseId: state.data.responseId
        });

        p.start(s);
    }
}
