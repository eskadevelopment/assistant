# PMT

Tool for aggregation, automation and to struct management tasks. 

## Goals
 - Show and planing budgets 
    - Show how much money we spent on project now
    - Show how much money we will spent on this project totally
 - Show difference between fact and plan in cost and time by project
 - Show % project finishing
 - Show forecast project end date
 - Reduce time spent on meetings
 - Get the real time info about projects status
 - Show real bandwidth for each colleague

## Structure
### Frontend
Stand alone application without prod version\
Deployed on [stage](http://192.168.50.67:5000) was using `vagga`\
Built on first version shitbusters without packages but used ui-kit\
Worked Pages:
 - WIP (table Person | Role | Tasks)
 - All tasks 
    - (table Name | Feature | Status | Project | Assignee | Department | Estimate | Spent time(h))
    - Filter by dates, name, department, assignee, project
 - Projects (table Name | Type | SpentTime) 
 - Time spent report 
     - (table Person | Role | Tasks)
     - Filter by date

### Backend
Based on `assistant` now live in bpm like one of a bpm controller
Deployed together with assistant
PMT work like aggregator:
 - use bpm DB for represent data
 - use [ws driver](../../../../../drivers/server/ws/Readme.md) for connect between frontend and backed
    - use apps `pmt`

Dependencies:
 - [ws driver](../../../../../drivers/server/ws/Readme.md)
 - [postgresql driver](../../../../../drivers/postgresql/Readme.md)
 - [Assistant](../../../../../core/protocol/ts/index.ts)
 - [Consts from bpm](../../const.ts)
