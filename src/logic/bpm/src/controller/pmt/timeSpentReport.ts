import { p } from '../../protocol';
import { DB_NAME } from '../../const';
import { Action, Route, State } from 'assistant';
import { getTimeSpentReport } from '../../db/queries';
import { Executors } from '../../../executors';

export enum TimeSpentAction {
    GET_TIME_SPENT_REPORT = 'GET_TIME_SPENT_REPORT',
}

export class TimeSpentController {

    @p.action(TimeSpentAction.GET_TIME_SPENT_REPORT)
    async getTimeSpentReport(data: { filter: { date: string }}, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_MANY', Executors.PG, {
                query: getTimeSpentReport,
                args: {
                    date: data.filter.date
                        || `${new Date().getFullYear()}-${new Date().getMonth()}-${new Date().getDate()}`
                },
                name: DB_NAME,
            }),
            new Action('WRAP', Executors.WS),
            new Action('RESPONSE', Executors.WS),
        ]), {
            responseId: state.data.responseId
        });

        p.start(s);
    }

}
