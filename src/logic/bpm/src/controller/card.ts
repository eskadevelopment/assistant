import { p } from '../protocol';
import { CardStatus, DB_NAME, ObjectType, ProcessStatus, ProgressLabel, SLACK_WORKSPACE } from '../const';
import { Action, Route, State } from 'assistant';
import { getLabels } from '../db/queries';
import { Executors } from '../../executors';
import { HistoryAction } from './history';
import { ProcessAction } from './process';

export enum CardAction {
    REASSIGN = 'USER_CARD_REASSIGN',
    MOVE_TO_BACKLOG = 'USER_CARD_MOVE_TO_BACKLOG',
    MOVE_TO_TO_DO = 'USER_CARD_MOVE_TO_TO_DO',
    MOVE_TO_WIP = 'USER_CARD_MOVE_TO_WIP',
    MOVE_TO_PENDING = 'USER_CARD_MOVE_TO_PENDING',
    MOVE_TO_DONE = 'USER_CARD_MOVE_TO_DONE',
    FLOW = 'USER_CARD_FLOW'
}

export enum PrivateCardAction {
    CARD_LABEL_ADD = 'CARD_LABEL_ADD',
    PROCESS_LABELS_REMOVE = 'PROCESS_LABELS_REMOVE',
    PREPARE_REASSIGN_MESSAGE = 'PREPARE_REASSIGN_MESSAGE',
    PREPARE_FLOW_MESSAGE = 'PREPARE_FLOW_MESSAGE',
    CHECK_TESTING_LABEL = 'CHECK_TESTING_LABEL',
    CHANGE_HISTORY = 'CHANGE_HISTORY ',
}

class CardController {

    @p.action(CardAction.FLOW)
    async flow(
        data: { cardId: string, oldMemberId: string, newMemberId: string, status: CardStatus, data: { id: string } },
        state: State
    ) {
        const s = new State(new Route('seq', [
            new Route('par', [
                new Action('CARD:MEMBER:DELETE', Executors.TRELLO, {
                    memberId: data.oldMemberId,
                    cardId: data.data.id,
                }),
                new Action('CARD:MEMBER:ADD', Executors.TRELLO, {
                    memberId: data.newMemberId,
                    cardId: data.data.id,
                }),
                new Action('EXECUTE', Executors.PG, {
                    query: 'UPDATE card SET user_id = ${userId} WHERE card.id = ${cardId};',
                    args: { cardId: data.data.id, userId: data.newMemberId },
                    name: DB_NAME,
                }),
                new Route('seq', [
                    new Action('GET_ONE', Executors.PG, {
                        query: 'select p.label_testing_trello_id ' +
                            ' from card left join process p on card.process_id = p.id where card.id = ${cardId};',
                        args: {
                            cardId: data.data.id
                        },
                        name: DB_NAME
                    }),
                    new Action(PrivateCardAction.CHECK_TESTING_LABEL, Executors.BPM, {
                        cardId: data.data.id,
                        status: data.status
                    }),
                ])
            ]),
            new Action('GET_ONE', Executors.PG, {
                query: 'SELECT trello_board_id as "boardId",' +
                    ` ${data.status === CardStatus.DONE ? 'trello_board_done_id' : 'trello_board_backlog_id'} as "listId", ` +
                    ` user_board_id as "cardId", card.name as "cardName", slack_id as "slackId", 'top' as pos ` +
                    ' FROM "user" LEFT JOIN card ON card.user_id = "user".id ' +
                    ' WHERE card.id = ${cardId}; ',
                args: { cardId: data.data.id },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('CARD:MOVE', Executors.TRELLO),
                new Route('seq', [
                    new Action(PrivateCardAction.PREPARE_FLOW_MESSAGE, Executors.BPM, {
                        status: data.status
                    }),
                    new Action('SEND', Executors.SLACK),
                ]),
                new Action(PrivateCardAction.CHANGE_HISTORY, Executors.BPM, {
                    oldMemberId: data.oldMemberId,
                    newMemberId: data.newMemberId,
                    status: data.status
                })
            ])
            // TODO: Change history
        ]));

        p.start(s);
    }

    @p.action(PrivateCardAction.PREPARE_FLOW_MESSAGE)
    prepareFlowMessage(
        data: { status: CardStatus, data: { cardName: string, slackId: string, cardId: string } }, state: State
    ) {
        let text: string;

        switch (data.status) {
            case CardStatus.CLARIFY:
                text = `Карточка ${data.data.cardName} требует детального описания.`;
                break;
            case CardStatus.DEV:
                text = `Карточка  ${data.data.cardName} возвращена, нужна доработка(или доописали).`;
                break;
            case CardStatus.DONE:
                text = `Карточка  ${data.data.cardName} возвращена после теста, готово.`;
                break;
            case CardStatus.TEST:
                text = `Вам  ${data.data.cardName} пришла карточка на ревью.`;
                break;
            default:
                text = `Вам  ${data.data.cardName} пришла карточка.`;
                break;
        }

        return {
            text: `${text} Ссылка: https://trello.com/c/${data.data.cardId}`,
            workspace: SLACK_WORKSPACE,
            channel: data.data.slackId
        };
    }

    @p.action(PrivateCardAction.CHANGE_HISTORY)
    chaneHistory(
        data: {
            status: CardStatus,
            oldMemberId: string,
            newMemberId: string,
            data: { cardName: string, slackId: string, cardId: string, boardId: string }
        },
        state: State
    ) {
        new Action(HistoryAction.ADD_HISTORY, Executors.BPM, {
            objId: data.data.cardId,
            status: ProcessStatus.BACKLOG,
            objTypeId: ObjectType.STEP,
            data: {
                userId: data.oldMemberId,
                processId: data.data.boardId
            },
        });
        new Action(HistoryAction.ADD_HISTORY, Executors.BPM, {
            objId: data.data.cardId,
            status: data.status === CardStatus.DONE ? ProcessStatus.DONE : ProcessStatus.BACKLOG,
            objTypeId: ObjectType.STEP,
            data: {
                userId: data.newMemberId,
                processId: data.data.boardId
            },
        });
    }

    @p.action(PrivateCardAction.CHECK_TESTING_LABEL)
    async checkTestingLabel(
        data: { cardId: string, status: CardStatus, data: { label_testing_trello_id: string } },
        state: State
    ) {
        if (data.status === CardStatus.CLARIFY) {
            return;
        }

        const s = new State(new Route('seq', [
            new Action(data.status === CardStatus.TEST ? 'CARD:ADD_LABEL' : 'CARD:DELETE_LABEL', Executors.TRELLO)
        ]));
        p.start(s, {
            labelId: data.data.label_testing_trello_id,
            cardId: data.cardId
        });
    }

    @p.action(CardAction.REASSIGN)
    async reassign(data: { cardId: string, oldMemberId: string, newMemberId: string, boardId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Route('par', [
                new Action('CARD:MEMBER:DELETE', Executors.TRELLO, {
                    memberId: data.oldMemberId,
                    cardId: data.cardId,
                }),
                new Action('CARD:MEMBER:ADD', Executors.TRELLO, {
                    memberId: data.newMemberId,
                    cardId: data.cardId,
                }),
                new Action('EXECUTE', Executors.PG, {
                    query: 'UPDATE card SET user_id = ${userId} WHERE card.id = ${cardId};',
                    args: { cardId: data.cardId, userId: data.newMemberId },
                    name: DB_NAME,
                }),
            ]),
            new Action('GET_ONE', Executors.PG, {
                query: 'SELECT trello_board_id as "boardId", trello_board_backlog_id as "listId", ' +
                    ` user_board_id as "cardId", card.name as "cardName", slack_id as "slackId", 'top' as pos ` +
                    ' FROM "user" LEFT JOIN card ON card.user_id = "user".id ' +
                    ' WHERE card.id = ${cardId}; ',
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('CARD:MOVE', Executors.TRELLO),
                new Route('seq', [
                    new Action(PrivateCardAction.PREPARE_REASSIGN_MESSAGE, Executors.BPM),
                    new Action('SEND', Executors.SLACK),
                ])
            ]),
            new Action(HistoryAction.ADD_HISTORY, Executors.BPM, {
                objId: data.cardId,
                status: ProcessStatus.BACKLOG,
                objTypeId: ObjectType.STEP,
                data: {
                    userId: data.oldMemberId,
                    processId: data.boardId,
                },
            }),
            new Action(HistoryAction.ADD_HISTORY, Executors.BPM, {
                objId: data.cardId,
                status: ProcessStatus.BACKLOG,
                objTypeId: ObjectType.STEP,
                data: {
                    userId: data.newMemberId,
                    processId: data.boardId,
                },
            }),
        ]));

        p.start(s);
    }

    @p.action(PrivateCardAction.PREPARE_REASSIGN_MESSAGE)
    prepareReassignMessage(data: { cardName: string, slackId: string, cardId: string }, state: State) {
        return {
            text: `На вас переназначена карточка "${data.cardName}" ссылка: https://trello.com/c/${data.cardId}`,
            workspace: SLACK_WORKSPACE,
            channel: data.slackId
        };
    }

    @p.action(PrivateCardAction.CARD_LABEL_ADD)
    async addLabel(data: {
        labelName: ProgressLabel,
        data: {
            id: string,
            doneLabelId: string,
            inProgressLabelId: string,
            pendingLabelId: string,
        },
    }, state: State) {
        const getLabelId = (name: ProgressLabel): string => {
            switch (data.labelName) {
                case ProgressLabel.WIP:
                    return data.data.inProgressLabelId;
                case ProgressLabel.Pending:
                    return data.data.pendingLabelId;
                case ProgressLabel.Done:
                    return data.data.doneLabelId;
                default:
                    return '';
            }
        };

        const labelId = getLabelId(data.labelName);

        const s = new State(new Route('seq', [
            new Action('CARD:ADD_LABEL', Executors.TRELLO, {
                labelId,
                cardId: data.data.id,
            }),
        ]));
        p.start(s);
    }

    @p.action(PrivateCardAction.PROCESS_LABELS_REMOVE)
    async removeProcessLabels(
        data: [{
            id: string,
            doneLabelId: string,
            inProgressLabelId: string,
            pendingLabelId: string,
        },
            Array<{ id: string }>
            ],
        state: State
    ) {
        const allLabelsIds = [
            data[0].doneLabelId,
            data[0].inProgressLabelId,
            data[0].pendingLabelId,
        ];
        const labelsIdsForRemove = data[1].filter(({ id }) => allLabelsIds.includes(id));

        labelsIdsForRemove.forEach(({ id }) => {
            const s = new State(new Route('seq', [
                new Action('CARD:DELETE_LABEL', Executors.TRELLO),
            ]));
            p.start(s, { labelId: id, cardId: data[0].id });
        });
        return data[0];
    }

    @p.action(CardAction.MOVE_TO_BACKLOG)
    async moveToBacklog(data: { cardId: string, boardId: string, listId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: getLabels,
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('PASS', Executors.PROCESS),
                new Action('CARD:LABEL:GET_ALL', Executors.TRELLO),
                new Route('seq', [
                    new Action('FUNC', Executors.PROCESS, {
                        func: ((args: any) => ({
                            cardId: args.id,
                            status: args.status
                        })).toString(),
                        args: {
                            status: ProcessStatus.BACKLOG,
                        }
                    }),
                    new Action(HistoryAction.ADD_STEP_HISTORY, Executors.BPM),
                ]),
            ]),
            new Action(PrivateCardAction.PROCESS_LABELS_REMOVE, Executors.BPM),
        ]));

        p.start(s);
    }

    @p.action(CardAction.MOVE_TO_TO_DO)
    async moveToToDo(data: { cardId: string, boardId: string, listId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: getLabels,
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('PASS', Executors.PROCESS),
                new Action('CARD:LABEL:GET_ALL', Executors.TRELLO),
                new Route('seq', [
                    new Action('FUNC', Executors.PROCESS, {
                        func: ((args: any) => ({
                            cardId: args.id,
                            status: args.status
                        })).toString(),
                        args: {
                            status: ProcessStatus.TODO,
                        }
                    }),
                    new Action(HistoryAction.ADD_STEP_HISTORY, Executors.BPM),
                ]),
            ]),
            new Action(PrivateCardAction.PROCESS_LABELS_REMOVE, Executors.BPM),
        ]));

        p.start(s);
    }

    @p.action(CardAction.MOVE_TO_WIP)
    async moveWIP(data: { cardId: string, boardId: string, listId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: getLabels,
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('PASS', Executors.PROCESS),
                new Action('CARD:LABEL:GET_ALL', Executors.TRELLO),
                new Route('seq', [
                    new Action('FUNC', Executors.PROCESS, {
                        func: ((args: any) => ({
                            cardId: args.id,
                            status: args.status
                        })).toString(),
                        args: {
                            status: ProcessStatus.IN_PROGRESS,
                        }
                    }),
                    new Action(HistoryAction.ADD_STEP_HISTORY, Executors.BPM),
                ]),
            ]),
            new Action(PrivateCardAction.PROCESS_LABELS_REMOVE, Executors.BPM),
            new Action(PrivateCardAction.CARD_LABEL_ADD, Executors.BPM, {
                labelName: ProgressLabel.WIP,
            }),
        ]));

        p.start(s);
    }

    @p.action(CardAction.MOVE_TO_PENDING)
    async movePending(data: { cardId: string, boardId: string, listId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: getLabels,
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('PASS', Executors.PROCESS),
                new Action('CARD:LABEL:GET_ALL', Executors.TRELLO),
                new Route('seq', [
                    new Action('FUNC', Executors.PROCESS, {
                        func: ((args: any) => ({
                            cardId: args.id,
                            status: args.status
                        })).toString(),
                        args: {
                            status: ProcessStatus.PENDING,
                        }
                    }),
                    new Action(HistoryAction.ADD_STEP_HISTORY, Executors.BPM),
                ]),
            ]),
            new Action(PrivateCardAction.PROCESS_LABELS_REMOVE, Executors.BPM),
            new Action(PrivateCardAction.CARD_LABEL_ADD, Executors.BPM, {
                labelName: ProgressLabel.Pending,
            }),
        ]));

        p.start(s);
    }

    @p.action(CardAction.MOVE_TO_DONE)
    async moveToDone(data: { cardId: string, boardId: string, listId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: getLabels,
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Route('par', [
                new Action('PASS', Executors.PROCESS),
                new Action('CARD:LABEL:GET_ALL', Executors.TRELLO),
                new Route('seq', [
                    new Action('FUNC', Executors.PROCESS, {
                        func: ((args: any) => ({
                            cardId: args.id,
                            status: args.status
                        })).toString(),
                        args: {
                            status: ProcessStatus.DONE,
                        }
                    }),
                    new Action(HistoryAction.ADD_STEP_HISTORY, Executors.BPM),
                ]),
            ]),
            new Action(PrivateCardAction.PROCESS_LABELS_REMOVE, Executors.BPM),
            new Action(PrivateCardAction.CARD_LABEL_ADD, Executors.BPM, {
                labelName: ProgressLabel.Done,
            }),
            new Action(ProcessAction.FINISH_STEP, Executors.BPM, {
                cardId: data.cardId,
                boardId: data.boardId,
                listId: data.listId
            })
        ]));

        p.start(s);
    }

}
