import { p } from '../protocol';
import { Action, Route, State } from 'assistant';
import { DB_NAME, ORGANIZATION_ID, TrelloCard } from '../const';
import { Executors } from '../../executors';
import { ProcessAction } from './process';

export enum TemplateAction {
    CREATE = 'TEMPLATE:CREATE',
    DELETE = 'TEMPLATE:DELETE',
    USE = 'TEMPLATE:USE',
    GET_MANY = 'TEMPLATE:GET_MANY'
}

enum TemplateUsePrivateAction {
    PREPARE_BOARD_DATA = 'TEMPLATE:USE:PREPARE_BOARD_DATA',
    AFTER_BOARD_CREATE = 'AFTER_BOARD_CREATE',
    GET_NEW_BOARD_URL = 'GET_NEW_BOARD_URL',
    COPY_MEMBERS = 'COPY_MEMBERS',
    PREPARE_GET_CARDS = 'PREPARE_GET_CARDS',
    SET_EXECUTORS = 'SET_EXECUTORS',
    PREPARE_DATA_FOR_SUBPROCESS = 'PREPARE_DATA_FOR_SUBPROCESS',
    PREPARE_LABELS = 'PREPARE_LABELS',
    CREATE_LIST_BY_CHECKLIST = 'CREATE_LIST_BY_CHECKLIST',
    PREPARE_CARD_IN_LIST_BY_CHECKLIST = 'PREPARE_CARD_IN_LIST_BY_CHECKLIST'
}

class TemplateController {

    @p.action(TemplateAction.CREATE)
    async create(data: { boardId: string, boardName: string }, state: State) {
        return {
            name: DB_NAME,
            query: 'INSERT INTO template(id, name) VALUES (${boardId}, ${boardName}) ' +
                'ON CONFLICT (id) DO UPDATE SET name = ${boardName}, enabled = true',
            args: data
        };
    }

    @p.action(TemplateAction.DELETE)
    async delete(data: { boardId: string }, state: State) {
        return {
            name: DB_NAME,
            query: 'UPDATE template SET enabled = false where id = ${id}',
            args: {
                id: data.boardId
            }
        };
    }

    @p.action(TemplateAction.USE)
    async use(
        data: {
            userId: string,
            parentId?: string,
            parentCardId?: string,
            parentName?: string,
            data: { name: string, id: string },
        },
        state: State
    ) {
        const s = new State(new Route('seq', [
            new Action(TemplateUsePrivateAction.PREPARE_BOARD_DATA, Executors.BPM),
            new Action('BOARD:CREATE', Executors.TRELLO),
            new Action(TemplateUsePrivateAction.AFTER_BOARD_CREATE, Executors.BPM, { userId: data.userId }),
            new Route('par', [
                new Route('seq', [
                    new Action('BOARD:ADD_MEMBER', Executors.TRELLO),
                    new Action(TemplateUsePrivateAction.GET_NEW_BOARD_URL, Executors.BPM),
                    new Action('WRAP', Executors.HTTP),
                    new Action('RESPONSE', Executors.HTTP),
                ]),
                new Route('seq', [
                    new Action('BOARD:MEMBERS:GET', Executors.TRELLO, { id: data.data.id }),
                    new Action(TemplateUsePrivateAction.COPY_MEMBERS, Executors.BPM),
                    new Route('par', [
                        new Action('BOARD:CARD:GET_ALL', Executors.TRELLO, {
                            id: data.data.id,
                            fields: ['id', 'idMembers', 'idList', 'pos']
                        }),
                        new Route('seq', [
                            new Action(TemplateUsePrivateAction.PREPARE_GET_CARDS, Executors.BPM),
                            new Action('BOARD:CARD:GET_ALL', Executors.TRELLO),
                        ])
                    ]),
                    new Action(TemplateUsePrivateAction.SET_EXECUTORS, Executors.BPM),
                ])
            ])
        ]), { responseId: state.data.responseId });
        if (data.parentId) {
            s.route.route.push(
                new Action(TemplateUsePrivateAction.PREPARE_DATA_FOR_SUBPROCESS, Executors.BPM, {
                    parentId: data.parentId,
                    parentCardId: data.parentCardId,
                    userId: data.userId
                }),
                new Action(ProcessAction.START_PROCESS, Executors.BPM),
                new Action('GET_ONE', Executors.PG, {
                    name: DB_NAME,
                    query: 'select label_in_progress_trello_id, label_subprocess_trello_id from process where id = ${processId}',
                    args: {
                        processId: data.parentId
                    }
                }),
                new Action(TemplateUsePrivateAction.PREPARE_LABELS, Executors.BPM, { cardId: data.parentCardId }),
                new Route('par', [
                    new Action('CARD:ADD_LABEL', Executors.TRELLO),
                    new Action('CARD:ADD_LABEL', Executors.TRELLO),
                ]),
                new Action('CARD:GET', Executors.TRELLO, {
                    id: data.parentCardId,
                    checklists: 'all'
                }),
                new Action(TemplateUsePrivateAction.CREATE_LIST_BY_CHECKLIST, Executors.BPM)
            );
        }
        p.start(s, { ...data.data, parentName: data.parentName || undefined });

    }

    @p.action(TemplateUsePrivateAction.CREATE_LIST_BY_CHECKLIST)
    async createListByChecklist(
        data: { checklists: Array<{ name: string, checkItems: Array<{ name: string }> }> },
        state: State
    ) {
        if (!data.checklists.length) {
            return;
        }

        const s = new State(
            new Route('seq', [
                new Route('par', data.checklists.map(checklists =>
                        new Route('seq', [
                            new Action('LIST:CREATE', Executors.TRELLO, {
                                name: checklists.name,
                                boardId: state.data.board.id,
                                pos: 'bottom'
                            }),
                            new Action(TemplateUsePrivateAction.PREPARE_CARD_IN_LIST_BY_CHECKLIST, Executors.BPM, {
                                items: checklists.checkItems
                            }),
                            new Route('par', checklists.checkItems.map(item =>
                                new Action('CARD:CREATE', Executors.TRELLO)
                            ))
                        ])
                    )
                )
            ])
        );
        p.start(s);
    }

    @p.action(TemplateUsePrivateAction.PREPARE_CARD_IN_LIST_BY_CHECKLIST)
    prepareCardInListByChecklist(data: { items: Array<{ name: string }>, data: { id: string } }, state: State) {
        return data.items.map(item => ({
            name: item.name,
            listId: data.data.id,
            pos: 'bottom'
        }));
    }

    @p.action(TemplateUsePrivateAction.PREPARE_GET_CARDS)
    async prepareGetCards(data: any, state: State) {
        return {
            id: state.data.board.id,
            fields: ['id', 'idMembers', 'idList', 'pos']
        };
    }

    @p.action(TemplateUsePrivateAction.PREPARE_LABELS)
    async prepareSubProcessLabelsData(
        data: {
            cardId: string,
            data: {
                label_in_progress_trello_id: string,
                label_subprocess_trello_id: string
            }
        }, state: State
    ) {
        return [
            {
                labelId: data.data.label_in_progress_trello_id,
                cardId: data.cardId,
            },
            {
                labelId: data.data.label_subprocess_trello_id,
                cardId: data.cardId,
            }];
    }

    @p.action(TemplateUsePrivateAction.PREPARE_BOARD_DATA)
    async prepareBoardData(data: { id: string, name: string, parentName: string }, state: State) {
        return {
            sourceId: data.id,
            name: data.parentName || `${data.name} ${new Date().toISOString()}`,
            permission: 'org',
            organizationId: ORGANIZATION_ID
        };
    }

    @p.action(TemplateUsePrivateAction.AFTER_BOARD_CREATE)
    async afterBoardCreate(data: { userId: string, data: { id: string, url: string } }, state: State) {
        state.data.board = {
            id: data.data.id,
            url: data.data.url,
        };

        state.data.user = {
            id: data.userId
        };

        return {
            boardId: data.data.id,
            memberId: data.userId,
            type: 'admin'
        };
    }

    @p.action(TemplateUsePrivateAction.GET_NEW_BOARD_URL)
    async getNewBoardUrl(data: any, state: State) {
        return {
            url: state.data.board.url
        };
    }

    @p.action(TemplateUsePrivateAction.COPY_MEMBERS)
    async copyMembers(data: Array<{ id: string }>, state: State) {
        const s = new State(new Route('seq', [
            new Route('par',
                data
                    .filter(elem => elem.id !== state.data.user.id)
                    .map(elem => new Action('BOARD:ADD_MEMBER', Executors.TRELLO, {
                        boardId: state.data.board.id,
                        memberId: elem.id
                    }))
            )]
        ));
        await p.start(s, {});
    }

    @p.action(TemplateUsePrivateAction.SET_EXECUTORS)
    async setExecutors(
        data: [
            Array<TrelloCard>, // template
            Array<TrelloCard>  // process
        ],
        state: State
    ) {
        const compareFunc = (a: TrelloCard, b: TrelloCard): number => {
            return a.id > b.id ? 1 : -1;
        };

        const templateCards = data[0].sort(compareFunc);
        const processCards = data[1]
            .filter(card => card.name !== 'Parent process')
            .sort(compareFunc);

        const cards = processCards
            .map((card, index) => ({
                cardId: card.id,
                memberId: templateCards[index].idMembers.length > 0 ? templateCards[index].idMembers[0] : null
            }))
            .filter(card => card.memberId);

        const s = new State(
            new Route('seq', [
                new Route('par', cards.map(
                    card => new Action('CARD:MEMBER:ADD', Executors.TRELLO, card))
                )]
            )
        );
        await p.start(s, {});
    }


    @p.action(TemplateUsePrivateAction.PREPARE_DATA_FOR_SUBPROCESS)
    async prepareDataForSubprocess(data: { parentId: string, parentCardId: string, userId: string }, state: State) {
        return {
            parentId: data.parentId,
            parentCardId: data.parentCardId,
            boardId: state.data.board.id,
            userId: data.userId
        };
    }

    @p.action(TemplateAction.GET_MANY)
    async getMany(data: Array<{ id: string, name: string }>, state: State) {
        return data.map(elem => ({ id: elem.id, text: elem.name }));
    }

}
