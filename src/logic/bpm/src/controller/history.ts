import { p } from '../protocol';
import { DB_NAME, ObjectType, ProcessStatus } from '../const';
import { Action, Route, State } from 'assistant';
import { addHistory, getCardDataForHistory } from '../db/queries';
import { Executors } from '../../executors';

export enum HistoryAction {
    ADD_HISTORY = 'ADD_HISTORY',
    ADD_STEP_HISTORY = 'ADD_STEP_HISTORY',
}

class HistoryController {

    @p.action(HistoryAction.ADD_HISTORY)
    async add(data: {
        objId: string,
        status: ProcessStatus,
        objTypeId: ObjectType,
        data: {
            userId: string,
            processId: string,
        },
    }, state: State) {
        const s = new State(new Route('seq', [
            new Action('EXECUTE', Executors.PG, {
                query: addHistory,
                args: {
                    objId: data.objId,
                    status: data.status,
                    objTypeId: data.objTypeId,
                    userId: data.data.userId,
                    processId: data.data.processId,
                },
                name: DB_NAME,
            }),
        ]));

        p.start(s);
    }

    @p.action(HistoryAction.ADD_STEP_HISTORY)
    async addStep(data: { cardId: string, status: ProcessStatus }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                query: getCardDataForHistory,
                args: { cardId: data.cardId },
                name: DB_NAME,
            }),
            new Action(HistoryAction.ADD_HISTORY, Executors.BPM, {
                objId: data.cardId,
                objTypeId: ObjectType.STEP,
                status: data.status,
            }),
        ]));

        p.start(s);
    }
}
