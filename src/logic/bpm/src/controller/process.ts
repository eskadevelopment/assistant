import { p } from '../protocol';
import { DB_NAME, SLACK_WORKSPACE } from '../const';
import { Action, Route, State } from 'assistant';
import { Executors } from '../../executors';
import { UtilAction } from './util';
import { getParentProcessInfo } from '../db/queries';

export enum ProcessAction {
    START_PROCESS = 'START_PROCESS',
    FINISH_PROCESS = 'FINISH_PROCESS',
    START_STAGE = 'START_STAGE',
    FINISH_STAGE = 'FINISH_STAGE',
    START_STEP = 'START_STEP',
    FINISH_STEP = 'FINISH_STEP',
    CHANGE_STEP_TO_PROCESS = 'CHANGE_STEP_TO_PROCESS'
}

enum StartProcessPrivateAction {
    PREPARE_QUERY_FOR_CREATE_PROCESS = 'SP:PREPARE_QUERY_FOR_CREATE_PROCESS',
    GET_FIRST_LIST_ID = 'SP:GET_FIRST_LIST_ID',
    PREPARE_PARENT_PROCESS_CARD = 'SP:PREPARE_PARENT_PROCESS_CARD',
    PREPARE_PARENT_ATTACHMENT = 'SP:PREPARE_PARENT_ATTACHMENT',
    SET_OWNER = 'SP:SET_OWNER'
}

enum FinishProcessPrivateAction {
    ARCHIVE_ALL_CARD = 'FP:ARCHIVE_ALL_CARD',
    SEND_TO_ALL_MEMBER = 'FP:SEND_TO_ALL_MEMBER',
    MARK_DONE_PARENT_CARD = 'FP:MARK_DONE_PARENT_CARD'
}

enum FinishStagePrivateAction {
    RETURN_NEXT_LIST_ID = 'RETURN_NEXT_LIST_ID'
}

enum StartStepPrivateAction {
    PREPARE_TEXT_CUSTOM_FIELD = 'SS:PREPARE_TEXT_CUSTOM_FIELD',
    CHECK_EXIST_QUERY = 'SS:CHECK_EXIST_QUERY',
    CHECK_RULES = 'SS:CHECK_RULES',
    PREPARE_MESSAGE = 'SS:PREPARE_MESSAGE',
    PREPARE_CARD_CREATE = 'SS:PREPARE_CARD_CREATE',
    PREPARE_CREATE_QUERY = 'SS:PREPARE_CREATE_QUERY',
    PREPARE_ATTACHMENTS = 'SS:PREPARE_ATTACHMENTS'
}

enum FinishStepPrivateAction {
    PREPARE_GET_LIST = 'FS:PREPARE_GET_LIST',
    CHECK_IS_LIST_DONE = 'FS:CHECK_IS_LIST_DONE'
}


class ProcessController {

    @p.action(ProcessAction.START_PROCESS)
    async startProcess(
        data: { userId: string, boardId: string, parentId: string, parentCardId: string }, state: State
    ) {
        const s = new State(new Route('seq', [
            new Route('par', [
                new Route('seq', [
                    new Action('BOARD:GET', Executors.TRELLO),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, {
                        variable: 'board', fields: ['id', 'name', 'lists']
                    }),
                ]),
                new Route('seq', [
                    new Action('LABEL:CREATE', Executors.TRELLO, {
                        boardId: data.boardId,
                        name: 'WIP',
                        color: 'yellow'
                    }),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'labelWIP', fields: ['id'] }),
                ]),
                new Route('seq', [
                    new Action('LABEL:CREATE', Executors.TRELLO, {
                        boardId: data.boardId,
                        name: 'Pending',
                        color: 'orange'
                    }),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'labelPending', fields: ['id'] }),
                ]),
                new Route('seq', [
                    new Action('LABEL:CREATE', Executors.TRELLO, {
                        boardId: data.boardId,
                        name: 'Done',
                        color: 'green'
                    }),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'labelDone', fields: ['id'] }),
                ]),
                new Route('seq', [
                    new Action('LABEL:CREATE', Executors.TRELLO, {
                        boardId: data.boardId,
                        name: 'subprocess',
                        color: 'sky'
                    }),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'labelSub', fields: ['id'] }),
                ]),
                new Route('seq', [
                    new Action('LABEL:CREATE', Executors.TRELLO, {
                        boardId: data.boardId,
                        name: 'Testing',
                        color: 'lime'
                    }),
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'labelTest', fields: ['id'] }),
                ]),
            ]),
            new Action(StartProcessPrivateAction.PREPARE_QUERY_FOR_CREATE_PROCESS, Executors.BPM, {
                parentId: data.parentId,
                parentCardId: data.parentCardId,
            }),
            new Action('EXECUTE', Executors.PG),
            new Action('WRAP', Executors.HTTP),
            new Action('RESPONSE', Executors.HTTP),
            // STORE TO HISTORY
            new Action(StartProcessPrivateAction.GET_FIRST_LIST_ID, Executors.BPM),
            new Route('par', [
                new Route('seq', [
                    new Action('LIST:GET_CARDS', Executors.TRELLO),
                    new Action(StartProcessPrivateAction.SET_OWNER, Executors.BPM, { userId: data.userId }),
                    new Action(ProcessAction.START_STAGE, Executors.BPM)
                ]),
            ])
        ]), { responseId: state.data.responseId });

        if (data.parentId) {
            (<Route>s.route.route[s.route.route.length - 1]).route.push(
                new Route('seq', [
                    new Action(StartProcessPrivateAction.PREPARE_PARENT_PROCESS_CARD, Executors.BPM),
                    new Action('CARD:CREATE', Executors.TRELLO),
                    new Action(StartProcessPrivateAction.PREPARE_PARENT_ATTACHMENT, Executors.BPM, {
                        parentCardId: data.parentCardId,
                        parentId: data.parentId
                    }),
                    new Route('par', [
                        new Action('CARD:ATTACH', Executors.TRELLO),
                        new Action('CARD:ATTACH', Executors.TRELLO)
                    ])
                ])
            );
        }

        p.start(s, {
            id: data.boardId
        });
    }

    @p.action(StartProcessPrivateAction.SET_OWNER)
    setOwner(data: { userId: string, data: Array<{ id: string, idMembers: Array<string> }> }, state: State) {
        data.data.forEach(card => {
            if (card.idMembers.length === 0) {
                card.idMembers = [data.userId];
                const s = new State(new Route('seq', [
                    new Action('CARD:MEMBER:ADD', Executors.TRELLO)
                ]));
                p.start(s, {
                    cardId: card.id,
                    memberId: data.userId
                });
            }
        });
        return data.data;
    }

    @p.action(StartProcessPrivateAction.PREPARE_PARENT_ATTACHMENT)
    prepareParentAttachment(data: { parentCardId: string, parentId: string, data: { id: string } }, state: State) {
        return [{
            name: 'Parent process',
            cardId: data.data.id,
            url: `https://trello.com/b/${data.parentId}`
        }, {
            name: 'Subprocess',
            cardId: data.parentCardId,
            url: `https://trello.com/b/${state.data.board.id}`
        }];
    }

    @p.action(StartProcessPrivateAction.PREPARE_PARENT_PROCESS_CARD)
    prepareParentProcessCard(data: any, state: State) {
        return {
            name: 'Parent process',
            listId: state.data.board.lists[0].id,
            idLabels: [state.data.labelDone.id],
            pos: 'bottom'
        };
    }

    @p.action(StartProcessPrivateAction.GET_FIRST_LIST_ID)
    getFirstListId(data: any, state: State) {
        return {
            listId: state.data.board.lists[0].id
        };
    }

    @p.action(StartProcessPrivateAction.PREPARE_QUERY_FOR_CREATE_PROCESS)
    prepareQueryForCreateProcess(data: { parentId: string, parentCardId: string }, state: State) {
        return {
            name: DB_NAME,
            query: `INSERT INTO process(id, parent_id, parent_card_id, name, label_in_progress_trello_id,
                                        label_pending_trello_id, label_done_trello_id, label_subprocess_trello_id,
                                        label_testing_trello_id)
                    VALUES (\${id},
                            \${parentId},
                            \${parentCardId},
                            \${name},
                            \${labelInProgress},
                            \${labelPending},
                            \${labelDone},
                            \${labelSub},
                            \${labelTest})
                    ON CONFLICT (id) DO NOTHING`,
            args: {
                id: state.data.board.id,
                parentId: data.parentId || null,
                parentCardId: data.parentCardId || null,
                name: state.data.board.name,
                labelInProgress: state.data.labelWIP.id,
                labelPending: state.data.labelPending.id,
                labelDone: state.data.labelDone.id,
                labelSub: state.data.labelSub.id,
                labelTest: state.data.labelTest.id
            }
        };
    }

    @p.action(ProcessAction.FINISH_PROCESS)
    finishProcess(data: { processId: string, data: { listId: string } }, state: State) {
        if (data.data.listId) {
            return;
        }
        const s = new State(new Route('seq', [
            new Route('par', [
                new Route('seq', [
                    new Action('GET_MANY', Executors.PG, {
                        name: DB_NAME,
                        query: 'SELECT user_board_id as id from card where process_id = ${processId}',
                        args: {
                            processId: data.processId,
                        }
                    }),
                    new Action(FinishProcessPrivateAction.ARCHIVE_ALL_CARD, Executors.BPM)
                ]),
                new Route('seq', [
                    new Action('GET_ONE', Executors.PG, {
                        name: DB_NAME,
                        query: 'select JSON_AGG(distinct slack_id) as "slackIds", ' +
                            ' first(process.name) as name, first(process.id) as id' +
                            ' from card' +
                            '   left join "user" on card.user_id = "user".id' +
                            '   left join process on card.process_id = process.id' +
                            ' where process_id = ${processId};',
                        args: {
                            processId: data.processId
                        }
                    }),
                    new Action(FinishProcessPrivateAction.SEND_TO_ALL_MEMBER, Executors.BPM)
                ]),
                new Route('seq', [
                    new Action('GET_ONE', Executors.PG, {
                        name: DB_NAME,
                        query: getParentProcessInfo,
                        args: {
                            processId: data.processId,
                        }
                    }),
                    new Action(FinishProcessPrivateAction.MARK_DONE_PARENT_CARD, Executors.BPM),
                    // TODO start FINISH_STEP after finish sub process or CHECK and FINISH STAGE
                    // new Action('CARD:GET', Executors.TRELLO),
                    // new Action(ProcessAction.FINISH_STEP, Executors.BPM, {
                    //     cardId: '',
                    //     listId: '',
                    //     boardId: ''
                    // })
                ])
            ]),
        ]));

        p.start(s, {});
        // TODO save history
    }

    @p.action(FinishProcessPrivateAction.ARCHIVE_ALL_CARD)
    archiveAllCards(data: Array<{ id: string }>, state: State) {
        data.forEach(card => {
            const s = new State(new Route('seq', [
                new Action('CARD:ARCHIVE', Executors.TRELLO)
            ]));
            p.start(s, { id: card.id });
        });
    }

    @p.action(FinishProcessPrivateAction.SEND_TO_ALL_MEMBER)
    sendToAllMembers(data: { id: string, name: string, slackIds: Array<string> }, state: State) {
        data.slackIds.forEach(slackId => {
            const s = new State(new Route('seq', [
                new Action('SEND', Executors.SLACK)
            ]));
            p.start(s, {
                text: `Процесс "${data.name}" завершился, ссылка: https://trello.com/b/${data.id}`,
                workspace: SLACK_WORKSPACE,
                channel: slackId
            });
        });
    }

    @p.action(FinishProcessPrivateAction.MARK_DONE_PARENT_CARD)
    markDoneParentCard(
        data: { parent_card_id: string, label_done_trello_id: string, label_in_progress_trello_id: string },
        state: State
    ) {
        if (data === null) {
            return;
        }
        const s = new State(new Route('seq', [
            new Action('CARD:ADD_LABEL', Executors.TRELLO, {
                labelId: data.label_done_trello_id,
                cardId: data.parent_card_id,
            }),
            new Action('CARD:DELETE_LABEL', Executors.TRELLO, {
                labelId: data.label_in_progress_trello_id,
                cardId: data.parent_card_id,
            }),
        ]));
        p.start(s, {});

        return {
            id: data.parent_card_id
        };
    }


    @p.action(ProcessAction.START_STAGE)
    startStage(
        data: Array<{ id: string, name: string, idMembers: Array<string>, url: string, idBoard: string }>, state: State
    ) {
        console.log(ProcessAction.START_STAGE, data);
        data.forEach(card => {
            const s = new State(
                new Route('seq', [
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, {
                        variable: 'card',
                        fields: ['id', 'name', 'idMembers', 'url', 'idBoard']
                    }),
                    new Action(StartStepPrivateAction.CHECK_EXIST_QUERY, Executors.BPM),
                    new Action('GET_ONE', Executors.PG),
                    new Action(StartStepPrivateAction.CHECK_RULES, Executors.BPM),
                    new Action(ProcessAction.START_STEP, Executors.BPM),
                ]));
            p.start(s, card);
        });
    }

    @p.action(StartStepPrivateAction.CHECK_EXIST_QUERY)
    checkExistQuery(data: any, state: State) {
        return {
            name: DB_NAME,
            query: 'SELECT (SELECT true FROM card WHERE id = ${id}) or' +
                ' (SELECT true from process where parent_card_id = ${id}) as exist',
            args: { id: state.data.card.id }
        };
    }

    @p.action(StartStepPrivateAction.CHECK_RULES)
    checkRules(data: { exist: boolean }, state: State) {
        return { isStart: !data.exist && state.data.card.idMembers && state.data.card.idMembers.length > 0 };
    }

    @p.action(ProcessAction.FINISH_STAGE)
    finishStage(data: { isDone: boolean }, state: State) {
        if (!data.isDone) {
            return;
        }

        const s = new State(new Route('seq', [
            new Action('BOARD:GET', Executors.TRELLO),
            new Action(FinishStagePrivateAction.RETURN_NEXT_LIST_ID, Executors.BPM, { listId: state.data.list.listId }),
            new Route('par', [
                new Route('seq', [
                    new Action('LIST:GET_CARDS', Executors.TRELLO),
                    new Action(ProcessAction.START_STAGE, Executors.BPM),
                ]),
                new Action(ProcessAction.FINISH_PROCESS, Executors.BPM, { processId: state.data.process.processId })
            ])
        ]));
        p.start(s, {
            id: state.data.process.processId
        });
        // save history
    }

    @p.action(FinishStagePrivateAction.RETURN_NEXT_LIST_ID)
    returnNextListId(data: { listId: string, data: { lists: Array<{ id: string }> } }, state: State) {
        let i = 0;
        data.data.lists.forEach((list, index) => {
            if (list.id === data.listId) {
                i = index;
            }
        });

        return {
            listId: i === data.data.lists.length - 1 ? null : data.data.lists[i + 1].id
        };
    }

    @p.action(ProcessAction.START_STEP)
    async startStep(data: { isStart: boolean }, state: State) {
        if (!data.isStart) {
            return;
        }
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG),
            new Action(StartStepPrivateAction.PREPARE_CARD_CREATE, Executors.BPM), // TODO mutate state (refactor it)
            new Action('CARD:CREATE', Executors.TRELLO),
            new Route('par', [
                new Route('seq', [
                    new Action(StartStepPrivateAction.PREPARE_CREATE_QUERY, Executors.BPM),
                    new Action('EXECUTE', Executors.PG),
                ]),
                new Route('seq', [
                    new Action(StartStepPrivateAction.PREPARE_ATTACHMENTS, Executors.BPM),
                    new Route('par', [
                        new Action('CARD:ATTACH', Executors.TRELLO),
                        new Action('CARD:ATTACH', Executors.TRELLO),
                    ]),
                ]),
                new Route('seq', [
                    new Action(UtilAction.STORE_IN_STATE, Executors.BPM, {
                        variable: 'newCard',
                        fields: ['id', 'name', 'shortUrl']
                    }),
                    new Action('BOARD:GET', Executors.TRELLO, { id: state.data.card.idBoard }),
                    new Route('par', [
                        new Route('seq', [
                            new Action(StartStepPrivateAction.PREPARE_TEXT_CUSTOM_FIELD, Executors.BPM),
                            new Action('CARD:CUSTOM_FIELD', Executors.TRELLO)
                        ]),
                        new Route('seq', [
                            new Action(StartStepPrivateAction.PREPARE_MESSAGE, Executors.BPM),
                            new Action('SEND', Executors.SLACK)
                        ]),
                    ]),
                ])
            ]),
        ]), { card: state.data.card });
        p.start(s, {
            name: DB_NAME,
            query: 'SELECT trello_board_backlog_id, process_custom_field_id, slack_id ' +
                'FROM "user" where id = ${id}',
            args: {
                id: state.data.card.idMembers[0]
            }
        });
    }

    @p.action(StartStepPrivateAction.PREPARE_CARD_CREATE)
    async prepareCardCreate(
        data: { trello_board_backlog_id: string, process_custom_field_id: string, slack_id: string }, state: State
    ) {
        state.data.processCustomFieldId = data.process_custom_field_id;
        state.data.slackId = data.slack_id;
        return {
            listId: data.trello_board_backlog_id,
            keepFromSource: 'all',
            idCardSource: state.data.card.id,
            pos: 'top',
        };
    }

    @p.action(StartStepPrivateAction.PREPARE_ATTACHMENTS)
    async prepareAttachments(data: { id: string, url: string }, state: State) {
        return [{
            cardId: state.data.card.id,
            url: data.url,
            name: 'linked user card'
        }, {
            cardId: data.id,
            url: state.data.card.url,
            name: 'linked process card'
        }];
    }

    @p.action(StartStepPrivateAction.PREPARE_CREATE_QUERY)
    prepareCreateQuery(data: { id: string }, state: State) {
        return {
            name: DB_NAME,
            query: `INSERT INTO card(id, user_board_id, name, user_id, status, process_id)
                    VALUES (\${id}, \${userBoardId}, \${name}, \${userId}, \${status}, \${processId})
                    ON CONFLICT (id) DO NOTHING`,
            args: {
                id: state.data.card.id,
                userBoardId: data.id,
                name: state.data.card.name,
                userId: state.data.card.idMembers[0],
                status: 1,
                processId: state.data.card.idBoard
            }
        };
    }

    @p.action(StartStepPrivateAction.PREPARE_MESSAGE)
    prepareMessage(data: { name: string }, state: State) {
        return {
            workspace: SLACK_WORKSPACE,
            channel: state.data.slackId,
            text: `У вас добавилась новая карточка "${state.data.newCard.name}" из процесса "${data.name}", ` +
                `ссылка: ${state.data.newCard.shortUrl}`,
        };
    }

    @p.action(StartStepPrivateAction.PREPARE_TEXT_CUSTOM_FIELD)
    prepareTextForCustomField(data: { name: string }, state: State) {
        return {
            cardId: state.data.newCard.id,
            customFieldId: state.data.processCustomFieldId,
            value: {
                text: data.name
            }
        };
    }

    // TODO FIX THIS SHIT
    @p.action(ProcessAction.FINISH_STEP)
    async finishStep(data: { cardId: string, boardId: string, listId: string }, state: State) {
        const s = new State(new Route('seq', [
            new Action('GET_ONE', Executors.PG, {
                name: DB_NAME,
                query: 'SELECT label_done_trello_id as "labelDoneId", c.id, process.id as "processId" from process ' +
                    ' left join card c on process.id = c.process_id where c.user_board_id = ${id}',
                args: {
                    id: data.cardId
                }
            }),
            new Action(UtilAction.STORE_IN_STATE, Executors.BPM, {
                variable: 'process',
                fields: ['processId', 'labelDoneId']
            }),
            new Action('CARD:GET', Executors.TRELLO),
            new Action(FinishStepPrivateAction.PREPARE_GET_LIST, Executors.BPM),
            new Route('par', [
                new Action(UtilAction.STORE_IN_STATE, Executors.BPM, { variable: 'list', fields: ['listId'] }),
                new Action('LIST:GET_CARDS', Executors.TRELLO),
            ]),
            new Action(FinishStepPrivateAction.CHECK_IS_LIST_DONE, Executors.BPM),
            new Action(ProcessAction.FINISH_STAGE, Executors.BPM)
        ]));
        p.start(s, {});
        // save history
    }

    @p.action(FinishStepPrivateAction.PREPARE_GET_LIST)
    async getList(data: { idList: string, idBoard: string }, state: State) {
        return {
            listId: data.idList,
            fields: ['labels']
        };
    }

    @p.action(FinishStepPrivateAction.CHECK_IS_LIST_DONE)
    async checkIsListDone(data: [any, Array<{ labels: Array<{ id: string }> }>], state: State) {
        return {
            isDone: data[1].every(
                elem => elem.labels.find(label => label.id === state.data.process.labelDoneId)
            )
        };
    }

    @p.action(ProcessAction.CHANGE_STEP_TO_PROCESS)
    async changeStepToProcess(data: any, state: State) {
        // build route
        // update parent route
        // save history
    }

}
