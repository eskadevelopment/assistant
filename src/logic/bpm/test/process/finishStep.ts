import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('FINISH_STEP', 'logic.bpm'),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test'),
]), {});

p.start(s, {
    cardId: '5d5d166be241a572d68acc43',
    boardId: '5d5d1650b188355950a3bae9',
    listId: '5d5d1661fdb1e022d0e41c93'
});
