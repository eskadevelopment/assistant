import { Route, State, Action } from 'assistant';

import { p } from '../common';
import { CardAction } from '../../src/controller/card';

const s = new State(new Route('seq', [
    new Action(CardAction.REASSIGN, 'logic.bpm'),
]));

p.start(s, {
    cardId: '5d5a783887f642447c0480d0',
    newMemberId: '5bac88e4a600ed58855cbfca',
    oldMemberId: '5b7a938726aa708a071359d4',
});
