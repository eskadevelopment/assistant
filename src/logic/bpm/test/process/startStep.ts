import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('START_STEP', 'logic.bpm'),
]), {
    card: {
        id: '5d5511b37fd89e33d5b3d4ec',
        name: 'Card for move',
        idMembers: ['5bac88e4a600ed58855cbfca'],
        url: 'https://trello.com/c/g4JMu44x/2-card-for-move',
        idBoard: '5d514a3a3e1a43525bc7a279'
    }
});

p.start(s, {
    isStart: true
});
