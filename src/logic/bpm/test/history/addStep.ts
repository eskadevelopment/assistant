import { Route, State, Action } from 'assistant';

import { p } from '../common';
import { ProcessStatus } from '../../src/const';
import { Executors } from '../../executors';
import { HistoryAction } from '../../src/controller/history';

const s = new State(new Route('seq', [
    new Action(HistoryAction.ADD_STEP_HISTORY, Executors.BPM),
]));

p.start(s, {
    cardId: '5d5a783887f642447c0480d0',
    status: ProcessStatus.DONE,
});
