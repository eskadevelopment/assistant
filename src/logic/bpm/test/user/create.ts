import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('USER:CREATE', 'logic.bpm'),
]));

p.start<{ slackId: string, email: string, data: { username: string, fullName: string, id: string } }>(s, {
    email: 'd.mehed@eska.global',
    slackId: 'U6EEPDHMY',
    data: {
        fullName: 'Alexandr Shevchenko',
        username: 'axelsheva94',
        id: '5bac88e4a600ed58855cbfca'
    }
});
