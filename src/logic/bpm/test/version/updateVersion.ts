import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { Executors } from '../../executors';
import { VersionAction } from '../../src/controller/version';

const s = new State(new Route('seq', [
    new Action(VersionAction.UPDATE_VERSION, Executors.BPM),
]), {});

p.start(s, {
    oldVersion: '1.0.0',
    boardId: '5d5d1650b188355950a3bae9',
});
