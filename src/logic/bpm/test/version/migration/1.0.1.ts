import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../../common';
import { Executors } from '../../../executors';

const s = new State(new Route('seq', [
    new Action('UPDATE_VERSION_1.0.1', Executors.BPM),
]), {});

p.start(s, {
    boardId: '5d8380eb265e9c5bd3eb011e',
});
