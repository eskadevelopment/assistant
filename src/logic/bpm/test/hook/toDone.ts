import { Route, State, Action } from 'assistant';

import { p } from '../common';
import { CardAction } from '../../src/const';
import { Executors } from '../..';

const s = new State(new Route('seq', [
    new Action(CardAction.MOVE_TO_DONE, Executors.BPM),
]));

p.start(s, {
    cardId: '5d5a789a5fec2d60899cf546',
    idLabels: [],
});
