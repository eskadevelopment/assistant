import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('TEMPLATE:USE', 'logic.bpm', {
        userId: '57e0e2b146e580c39d02db7e'
    }),
]));

p.start<{ name: string, id: string }>(s, {
    id: '5d514a3a3e1a43525bc7a279',
    name: 'Test343'
});
