import { Action, Route, State } from 'assistant';
import { p } from './common';

const WORKSPACE = 'test33428407';
const USERNAME = 'dmehed';

class Util {

    @p.action('TEST:REMOVE_ALL_UNUSED_BOARDS')
    clear(data: Array<{ id: string, name: string }>, state: State) {
        data.forEach(elem => {
            if (elem.name !== 'Alexandr Shevchenko' && elem.name !== 'template') {
                p.start(new State(new Route('seq', [
                    new Action('BOARD:DELETE', 'driver.trello'),
                ])), { id: elem.id });
            }
        });
    }
}

const s = new State(new Route('seq', [
    new Action('ORG:BOARD:GET_ALL', 'driver.trello'),
    new Action('TEST:REMOVE_ALL_UNUSED_BOARDS', 'test'),
]));

p.start(s, {
    id: 'test33428407',
    fields: ['id', 'name'],
});
