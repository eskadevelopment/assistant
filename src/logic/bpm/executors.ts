export enum Executors {
    BPM = 'logic.bpm',
    HTTP = 'driver.server.http',
    WS = 'driver.server.ws',
    TRELLO = 'driver.trello',
    PG = 'driver.postgresql',
    SLACK = 'driver.slack',
    PROCESS = 'logic.process'
}
