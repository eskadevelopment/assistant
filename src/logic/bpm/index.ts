#! /usr/bin/env ts-node-dev
import './src/controller/card';
import './src/controller/history';
import './src/controller/process';
import './src/controller/template';
import './src/controller/user';
import './src/controller/util';
import './src/controller/version';
import './src/controller/pmt/index';

