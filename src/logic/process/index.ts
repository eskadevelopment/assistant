#! /usr/bin/env ts-node
import { State } from 'assistant';

import { p } from './src/protocol';
import { merge } from './src/util';
import { ProcessAction } from './actions';

const POOL = new Map<string, { data: Array<any>, stateData: {}, count: number }>();

const barrier = (data: any, state: State) => {
    console.log('barrier.currentAction', state.currentAction);
    const currentBarrier: Array<string> = state.data.barriers[state.currentAction.id];

    let executedFunctions = POOL.get(state.currentAction.id);
    if (!executedFunctions) {
        executedFunctions = {
            data: new Array<any>(currentBarrier.length),
            stateData: {},
            count: 1
        };
        executedFunctions.data[currentBarrier.indexOf(state.prevAction.id)] = data;
        executedFunctions.stateData = merge({}, executedFunctions.stateData, state.data);
        POOL.set(state.currentAction.id, executedFunctions);
    } else {
        executedFunctions.data[currentBarrier.indexOf(state.prevAction.id)] = data;
        executedFunctions.stateData = merge({}, executedFunctions.stateData, state.data);
        executedFunctions.count += 1;
    }

    if (executedFunctions.count === currentBarrier.length) {
        POOL.delete(state.currentAction.id);
        state.data = executedFunctions.stateData;
        p.next(executedFunctions.data, state);
    }
};

p.actions.set('BARRIER', barrier);

class Test {

    @p.action(ProcessAction.TEST)
    test(data: any, state: State) {
        console.log('test.currentAction', state.currentAction);
        return Math.random();
    }
}

class Utils {

    @p.action(ProcessAction.PASS)
    test(data: any, state: State) {
        return data;
    }

    @p.action(ProcessAction.FUNC)
    async addStep(data: { func: string, data: object, args: object }, state: State) {
        return eval(data.func)({ ...data.data, ...data.args }, state);
    }
}
