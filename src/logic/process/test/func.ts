import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from './common';

const s = new State(new Route('seq', [
    new Action('FUNC', 'logic.process', {
        func: ((data) => [data.test.test1, data.test.test2]).toString(),
    }),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test'),
]), {});

p.start(s, {
    test: {
        test1: 1,
        test2: 'test'
    }
});
