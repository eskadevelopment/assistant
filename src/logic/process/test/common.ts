import { Protocol, State } from 'assistant';

export const p = new Protocol('test');

class Assert {

    @p.action('ASSERT')
    assert(data: any, state: State) {
        console.log(data.data, {...data, data: undefined });
    }

    @p.action('LOG')
    log(data: any, state: State) {
        console.log('LOG', data, state);
    }

    @p.action('EXIT')
    exit(data: any, state: State) {
        process.exit(0);
    }
}
