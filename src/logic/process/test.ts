import { Route, Action, Protocol, State } from 'assistant';

const p = new Protocol(null);

const s = new State(new Route('seq', [
    new Route('par', [
        new Action('TEST', 'logic.process', '1.1'),
        new Action('TEST', 'logic.process', '1.2'),
    ]),
    new Action('TEST', 'logic.process', '3'),
]), {});

p.start(s, {});
