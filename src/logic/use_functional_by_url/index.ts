#! /usr/bin/env ts-node-dev
import { Action, Route, State } from 'assistant';

import { p } from './src/protocol';
import { Actions } from './actions';
import { Executors } from './executors';

class UseFunctionalByUrl {

    @p.action(Actions.CREATE_TRELLO_CARD_BY_URL)
    async createTrelloCardByUrl(data: any, state: State) {
        await p.request('CARD:CREATE', Executors.TRELLO, data);
    }
}

p.start(new State(new Route('seq', [
    new Route('par', [
        new Action('REGISTER', Executors.HTTP, {
            method: 'POST',
            uri: '/trello/create_card',
            handler: {
                action: Actions.CREATE_TRELLO_CARD_BY_URL,
                executor: p.executorName
            }
        })
    ])
])));
