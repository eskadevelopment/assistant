export enum Executors {
    HTTP = 'driver.server.http',
    WS = 'driver.server.ws',
    TRELLO = 'driver.trello',
    SLACK = 'driver.slack',
}
