import Telegraf, { Composer, ContextMessageUpdate } from 'telegraf';

class TelegramBot {
    private bot: Telegraf<ContextMessageUpdate>;
    private bot_token = '925262802:AAHm2xISovAtnWnW5n_AcSUvUaOIkGXehlY';
    private options = {
        telegram: {
            agent: null,        // https.Agent instance, allows custom proxy, certificate, keep alive, etc.
            webhookReply: true  // Reply via webhook
        },
    };

    constructor() {
        this.bot = new Telegraf(this.bot_token, this.options);
        this.init();
    }

    private init = async () => {
        this.bot.start((ctx) => ctx.reply('Welcome!'));
        this.bot.help((ctx) => ctx.reply('Send me a sticker'));
        this.bot.on('sticker', (ctx) => ctx.reply('👍'));
        this.bot.on('text', (ctx, next) => {
            console.log(ctx.message);
            next();
        });
        this.bot.hears('hi', (ctx) => ctx.reply('Hey there'));

        try {
            await this.bot.launch();
            console.log('Bot has been started...');
        } catch (e) {
            console.log('Launch error: ', e);
        }
    };
}

new TelegramBot();
