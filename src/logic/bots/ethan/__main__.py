import math
import os
import subprocess
from random import random
from typing import Any

from assistant import Protocol, State, Action, Route

WORK_DIR = '/work/src/logic/bots/ethan/slack_hooks'  # submodule

TEMPLATES = dict(
    SH_TEMPLATE='sh {0}.sh {1}',
    JS_TEMPLATE='node {0}.js {1}',
    TS_TEMPLATE='ts-node {0}.ts {1}',
    PY_TEMPLATE='python3 {0}.py {1}'
)

app_id = math.trunc(random() * 100000000)
p = Protocol('logic.bots.ethan', max_poll_interval_ms=30 * 60 * 1000)


def get_prepared_message(message: str, channel: str, workspace: str):
    return {
        'text': message,
        'channel': channel,
        'workspace': workspace
    }


def check_rules(user, channel, command, args):
    if user in [
        'U6EEPDHMY',  # Dima
        'U9V0GEQ03',  # Igor
        'UBXEFUZLJ',  # Miha
        'U6NQ22UJV',  # Ivan
        'UC8NLV0PM',  # Onlexiy
        'UD350KH5M',  # Sasha
        'UD4BH6EUC',  # Halyna
        'UDAMQLC1L',  # Dima ddk
        'UFL2Y9CRX',  # Igor ddk
        'UFKGVU9KN',  # Miha ddk
        'UFKR8CUJ1',  # Ivan ddk
        'UED8VMRHC',  # Onlexiy ddk
        'UEC0L7GS1',  # Sasha ddk
        'UEAD12AKG',  # Halyna ddk
        None  # Assistant bot
    ] and channel in [
        'CA02W23S7',  # test_bots
        'GFX1T8H0U',  # ethan
        'CKABWJ1J8',  # test_bots ddk,
        'CK7JNE517',  # ethan ddk,
    ]:
        return True
    return False


@p.action('TEST')
def test(data: Any, state: State):
    print('test', data, state)
    return {'test': 'test2'}


@p.action('PROCESS')
def process(data: Any, state: State):
    try:
        print('PROCESS', data)
        words = data['text'].split(' ')
        executor = 'SH'  # words[1]
        command = words[0]
        args = words[1:]
        if executor.upper() == 'SH' and not check_rules(data['user'], data['channel'], command, args):
            return get_prepared_message('Ты тварь дрожащая и права не имеешь', data['channel'], data['workspace'])

        try:
            s = State(Route('seq', [Action('SEND', 'driver.slack')]))
            p.start(s, get_prepared_message('Ethan start task {}'.format(command), data['channel'], data['workspace']))

            os.chdir(WORK_DIR)
            output = subprocess.check_output(
                TEMPLATES[executor.upper() + '_TEMPLATE'].format(command, ' '.join(args)), shell=True
            )
            return get_prepared_message('Output: ' + output.decode(), data['channel'], data['workspace'])
        except subprocess.CalledProcessError as e:
            print('[logic][messenger_hooks][ethan]', e.output)
            return get_prepared_message(
                'Ethan failed task {} with output: {}'.format(command, e.output), data['channel'], data['workspace']
            )

    except Exception as e:
        print('[logic][messenger_hooks][ethan]', e)
        return get_prepared_message('Trouble with Ethan {}'.format(e), data['channel'], data['workspace'])


p.run_consumer_worker()
