from assistant import Protocol, State
from typing import Any

latest_commit_sha = '09346897507390fc95dfadc60c336c6db184c4a8'
p = Protocol('logic.mart2')


@p.action('CHANGE_LOG')
def change_log(data: Any, state: State):
    return {
        'owner': 'AraiEzzra',
        'repo': 'DDKCORE',
        'sha': data['commit_sha'] or latest_commit_sha
    }


p.run_consumer_worker()
