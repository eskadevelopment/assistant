import { Router } from 'express';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

export const apiRouter = Router();
apiRouter.get('/', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, s);
}));
