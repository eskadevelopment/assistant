import { Router } from 'express';
import { testRouter } from './test/router';
import { apiRouter } from './api/router';
import { googleRouter } from './google/router';
import { startListeners } from './util';
import { bpmRouter } from './bpm/router';

export enum Application {
    TEST = 'test',
    API = 'api',
    GOOGLE = 'google',
    BPM = 'bpm'
}

export const APPS = new Map<Application, Router>();

APPS.set(Application.TEST, testRouter);
APPS.set(Application.API, apiRouter);
APPS.set(Application.GOOGLE, googleRouter);
APPS.set(Application.BPM, bpmRouter);

startListeners(APPS);
