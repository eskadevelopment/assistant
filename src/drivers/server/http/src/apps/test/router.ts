import { Router } from 'express';
import { Action, State, Route } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

export const testRouter = Router();
testRouter.get('/', async (req, res) => {
    const s = new State(new Route('seq', [
        new Action('SEND', 'driver.slack'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, {
        'workspace': 'sk-consultingteam',
        'channel': 'CA02W23S7',
        'text': 'Test From Webserver'
    });
});
