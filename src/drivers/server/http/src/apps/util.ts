import { Application as ExpressApp, Router } from 'express';
import { Application } from './index';
const bodyParser = require('body-parser');
const express = require('express');

export const startListeners = (apps: Map<Application, Router>) => {
    const baseRouter = Router();
    [...apps.entries()].forEach(([name, app]) => {
        baseRouter.use(`/${name}`, app);
        app.use('/static', express.static(`${__dirname}/${name}/static`));
        console.log(`Router ${name} init on /${name}`);
    });
    (async () => {
        await getApp(baseRouter).listen(8090);
        console.log('Server start on port 8090');
    })();
};

export const getApp = (baseRouter: Router): ExpressApp => {
    const app = express();
    app.use(bodyParser());
    app.use((req, res, next) => {
        console.log('Request to:', req.originalUrl);
        next();
    });
    app.use('/webserver', baseRouter);
    return app;
};
