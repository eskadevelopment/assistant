import { Response } from 'express';

export type ResponseId = string;
const REQUEST_POOL = new Map<ResponseId, Response>();

export const saveToPool = (res: Response): ResponseId => {
    const requestId = Math.random().toString();
    REQUEST_POOL.set(requestId, res);
    return requestId;
};

export const execute = (responseId: ResponseId, body: any): void => {
    const res = REQUEST_POOL.get(responseId);
    if (res) {
        REQUEST_POOL.delete(responseId);
        res.json(body);
    }
};
