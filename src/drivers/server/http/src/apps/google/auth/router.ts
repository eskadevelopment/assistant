import { Router } from 'express';
import { Action, State, Route } from 'assistant';
import { saveToPool } from '../../pool';
import { p } from '../../../protocol';

export const authRouter = Router();
authRouter.get('/code', async (req, res) => {
    const s = new State(new Route('seq', [
        new Action('SAVE_CODE', 'driver.google.auth'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, {
        'code': req.body.code
    });
});
