import { Router } from 'express';
import { authRouter } from './auth/router';
import { calendarRouter } from './calendar/router';

export const googleRouter = Router();
googleRouter.use('/auth', authRouter);
googleRouter.use('/calendar', calendarRouter);

googleRouter.get('/google1df7f6b9563f0386.html', async (req, res) => {
    res.send('google-site-verification: google1df7f6b9563f0386.html');
});
