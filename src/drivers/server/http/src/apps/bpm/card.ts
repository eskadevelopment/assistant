import { Router } from 'express';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

export const cardRouter = Router();

cardRouter.post('/reassign', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('USER_CARD_REASSIGN', 'logic.bpm'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, req.body);
}));

cardRouter.post('/flow', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('GET_ONE', 'driver.postgresql', {
            query: 'SELECT id from card where user_board_id = ${cardId}',
            args: {
                cardId: req.body.cardId,
            },
            name: 'bpm'
        }),
        new Action('USER_CARD_FLOW', 'logic.bpm', req.body),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, req.body);
}));

cardRouter.post('/add_to_process', ((req, res) => {
    console.log(req.body);
    const s = new State(new Route('seq', [
        new Action('START_STAGE', 'logic.bpm'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, [req.body]);
}));
