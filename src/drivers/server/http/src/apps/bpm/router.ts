import { Router } from 'express';
import { templateRouter } from './template';
import { userRouter } from './user';
import { cardRouter } from './card';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

export const bpmRouter = Router();
bpmRouter.use('/template', templateRouter);
bpmRouter.use('/user', userRouter);
bpmRouter.use('/card', cardRouter);

bpmRouter.post('/start_process', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('START_PROCESS', 'logic.bpm'),
    ]), { responseId: saveToPool(res) });
    p.start(s, req.body);
}));

bpmRouter.post('/is_active_process', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('GET_ONE', 'driver.postgresql'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, {
        name: 'bpm',
        query: 'SELECT (SELECT id FROM process WHERE id = ${id}) as exist',
        args: { id: req.body.boardId }
    });
}));

bpmRouter.post('/update_version', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('UPDATE_VERSION', 'logic.bpm'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, req.body.oldVersion);
}));
