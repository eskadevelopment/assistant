const HOST = 'https://bpm.skc.today';
// const HOST = 'https://dmehed.skc.today';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('sendAddUser').addEventListener('click', () => {
        const username = document.getElementById('username').value;
        const email = document.getElementById('email').value;
        const slackId = document.getElementById('slackId').value;
        fetch(`${HOST}/webserver/bpm/user/create`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username,
                email,
                slackId
            })
        })
            .then(res => res.json())
            .then((data) => {
                const t = window.TrelloPowerUp.iframe();
                if (data.success) {
                    t.alert({
                        message: `Добавлен новый юзер`,
                        duration: 5,
                        display: 'success'
                    });
                } else {
                    t.alert({
                        message: `Что-то пошло не так`,
                        duration: 5,
                        display: 'error'
                    });
                }
                t.closeModal();
            });
    });
});

