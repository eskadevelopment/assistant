/* global TrelloPowerUp */
// we can access Bluebird Promises as follows
const Promise = TrelloPowerUp.Promise;
const HOST = 'https://bpm.skc.today';
// const HOST = 'https://dmehed.skc.today';
/*

 Trello Data Access

 The following methods show all allowed fields, you only need to include those you want.
 They all return promises that resolve to an object with the requested fields.

 Get information about the current board
 t.board('id', 'name', 'url', 'shortLink', 'members')

 Get information about the current list (only available when a specific list is in context)
 So for example available inside 'attachment-sections' or 'card-badges' but not 'show-settings' or 'board-buttons'
 t.list('id', 'name', 'cards')

 Get information about all open lists on the current board
 t.lists('id', 'name', 'cards')

 Get information about the current card (only available when a specific card is in context)
 So for example available inside 'attachment-sections' or 'card-badges' but not 'show-settings' or 'board-buttons'
 t.card('id', 'name', 'desc', 'due', 'closed', 'cover', 'attachments', 'members', 'labels', 'url', 'shortLink', 'idList')

 Get information about all open cards on the current board
 t.cards('id', 'name', 'desc', 'due', 'closed', 'cover', 'attachments', 'members', 'labels', 'url', 'shortLink', 'idList')

 Get information about the current active Trello member
 t.member('id', 'fullName', 'username')
*/

const GLITCH_ICON = './images/glitch.svg';
const WHITE_ICON = './images/icon-white.svg';
const GRAY_ICON = './images/icon-gray.svg';

const checkIsUserBoard = board => board.members.some(member => member.fullName === board.name);
const checkIsActiveCard = card => card.attachments.some(attach => attach.name === 'linked user card');
const checkIsCardSubprocess = card => card.labels.some(label => label.name === 'subprocess');


const checkIsActiveProcessQuery = (t) => (
    t.get('board', 'shared', 'isActiveProcess', false)
        .then(isActiveProcess => {
            if (!isActiveProcess) {
                fetch(
                    `${HOST}/webserver/bpm/is_active_process`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            'boardId': t.getContext()['board']
                        })
                    }
                ).then(function (response) {
                    return response.json();
                }).then(function (data) {
                    if (data.success) {
                        if (data.data.exist) {
                            return t.set('board', 'shared', 'isActiveProcess', true);
                        }
                    }
                })
            }
        })
);

const renderTemplateDropdown = (t, callbackFactory) => (
    fetch(`${HOST}/webserver/bpm/template/get_many`).then(function (response) {
        return response.json();
    }).then(function (result) {
        if (result.success) {
            const templates = result.data;
            return t.popup({
                title: 'Шаблоны',
                items: templates.map(function (item) {
                    return {
                        text: item.text,
                        callback: callbackFactory(item),
                    };
                }),
                search: {
                    count: 10,
                    placeholder: 'Поиск шаблонов',
                    empty: 'Шаблоны не найдены'
                }
            });
        } else {
            return t.alert({
                message: 'Что-то пошло не так',
                duration: 5,
                display: 'error'
            });
        }
    })
);

const renderUserDropdown = (t, callbackFactory) => (
    t.board('members').then(function ({ members }) {
        return t.popup({
            title: 'Исполнители',
            items: members.map(function (item) {
                return {
                    text: item.fullName,
                    callback: callbackFactory(item),
                };
            }),
            search: {
                count: 10,
                placeholder: 'Поиск исполнителей',
                empty: 'Исполнитель не найден'
            }
        });
    })
);


const useTemplateCallback = (t) =>
    renderTemplateDropdown(t, item => (t) => {
        t.closePopup();
        t.alert({
            message: 'Шаблон использован, подождите пару секунд',
            duration: 5,
            display: 'success'
        });
        return fetch(`${HOST}/webserver/bpm/template/use`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                'templateId': item.id,
                'userId': t.getContext()['member']
            })
        }).then(function (res) {
            return res.json();
        }).then(function (result) {
            if (result.success) {
                return t.navigate({
                    url: result.data.url
                });
            }
            return t.alert({
                message: 'Что-то пошло не так',
                duration: 5,
                display: result.errors || 'error'
            });
        })
    });

const openInstructionCallback = (t) =>
    t.modal({
        url: `${HOST}/webserver/bpm/static/help.html`,
        accentColor: '#F2D600',
        title: 'Инструкция',
        height: 500
    });

const openAddUserCallback = (t) =>
    t.modal({
        url: `${HOST}/webserver/bpm/static/add_user.html`,
        accentColor: '#F2D600',
        title: 'Добавить пользователя',
        height: 500
    });

const startProcessCallback = function (t) {
    t.alert({
        message: 'Процесс запущен',
        duration: 5,
        display: 'success'
    });
    t.set('board', 'shared', 'isActiveProcess', true);
    return fetch(`${HOST}/webserver/bpm/start_process`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'boardId': t.getContext()['board'],
            'userId': t.getContext()['member'],
        })
    }).then(function (res) {
        return res.json();
    }).then(function (data) {
        setTimeout(init);
    }).catch(function (res) {
        t.set('board', 'shared', 'isActiveProcess', false);
        t.alert({
            message: 'Что-то пошло не так',
            duration: 5,
            display: 'error'
        });
    });
};

const addCardToProcess = function (t) {
    return t.card('id', 'name', 'members', 'attachments', 'url')
        .then(function (card) {
            if (!card.members.length) {
                t.alert({
                    message: 'Должен быть назначен исполнитель',
                    duration: 5,
                    display: 'error'
                });
                return;
            } else if (card.members.length > 1) {
                t.alert({
                    message: 'Должен быть 1 исполнитель',
                    duration: 5,
                    display: 'error'
                });
                return;
            }

            t.set('card', 'shared', 'isActiveProcess', true);
            t.alert({
                message: 'Карточка добавленна в процесс',
                duration: 5,
                display: 'success'
            });


            return fetch(`${HOST}/webserver/bpm/card/add_to_process`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: card.id,
                    name: card.name,
                    idMembers: card.members.map(member => member.id),
                    url: card.url,
                    idBoard: t.getContext()['board']
                })
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
            }).catch(function (res) {
                t.set('card', 'shared', 'isActiveProcess', false);
                t.alert({
                    message: 'Что-то пошло не так',
                    duration: 5,
                    display: 'error'
                });
                console.log(`Error: Card don't added to process: ${res.message}`);
            });
        });
};

const setStatus = (t, status, newMemberId) => {
    const oldMemberId = t.getContext()['member'];
    // const oldMemberId = '5cefa8653acdb55835c3f461';
    return t.set('card', 'shared', { oldMemberId, status })
        .then(() =>
            fetch(`${HOST}/webserver/bpm/card/flow`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    cardId: t.getContext()['card'],
                    oldMemberId,
                    newMemberId,
                    status
                })
            })
                .then(res => res.json())
                .then(() => {
                    t.alert({
                        message: `Отправленно`, // TODO проработать сообщения
                        duration: 5,
                        display: 'success'
                    })
                })
                .catch(() => {
                    t.alert({
                        message: 'Что-то пошло не так',
                        duration: 5,
                        display: 'error'
                    });
                })
        )
};

const toTest = (t) =>
    renderUserDropdown(t, reviewer => (t) => {
        t.closePopup();
        return setStatus(t, 'TEST', reviewer.id);
    });

const toClarify = (t) =>
    renderUserDropdown(t, reviewer => (t) => {
        t.closePopup();
        return setStatus(t, 'CLARIFY', reviewer.id);
    });

const review = (t, status) => {
    return t.get('card', 'shared', 'oldMemberId').then((oldMemberId) => setStatus(t, status, oldMemberId));
};

const reassignCard = (t) =>
    renderUserDropdown(t, newMember => (t) => {
        return t.card('id', 'members', 'attachments')
            .then(function (card) {
                if (!card.members.length) {
                    t.alert({
                        message: 'Не назначен прошлый исполнитель',
                        duration: 5,
                        display: 'error'
                    });
                    return t.closePopup();
                } else if (card.members.length > 1) {
                    t.alert({
                        message: 'Должен быть 1 исполнитель',
                        duration: 5,
                        display: 'error'
                    });
                    return t.closePopup();
                }

                const oldMember = card.members[0];

                if (oldMember.id === newMember.id) {
                    t.alert({
                        message: 'Нельзя переназначить на того-же исполнителя',
                        duration: 5,
                        display: 'error'
                    });
                    return t.closePopup();
                }

                if (!card.attachments.find(attach => attach.name === 'linked user card')) {
                    t.alert({
                        message: 'Карта должна быть в процессе',
                        duration: 5,
                        display: 'error'
                    });
                    return t.closePopup();
                }

                t.closePopup();
                return fetch(`${HOST}/webserver/bpm/card/reassign`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        'cardId': card.id,
                        'oldMemberId': oldMember.id,
                        'newMemberId': newMember.id,
                        'boardId': t.getContext()['board']
                    })
                })
                    .then(res => res.json())
                    .then(() => {
                        t.alert({
                            message: `Назначен новый исполнитель ${newMember.fullName}`,
                            duration: 5,
                            display: 'success'
                        });
                    }).catch(() => {
                        t.alert({
                            message: 'Что-то пошло не так',
                            duration: 5,
                            display: 'error'
                        });
                    })
            });
    });

const createTemplateCallback = (t) => {
    return t.board('name').get('name')
        .then((boardName) => {
            fetch(`${HOST}/webserver/bpm/template/create`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    'boardId': t.getContext()['board'],
                    'boardName': boardName
                })
            }).then(function (res) {
                return res.json();
            }).then(function (data) {
                t.set('board', 'shared', 'isActiveProcess', false);
                t.alert({
                    message: 'Шаблон успешно добавлен/обновлен',
                    duration: 5,
                    display: 'success'
                });

            }).catch(function (res) {
                t.alert({
                    message: 'Что-то пошло не так',
                    duration: 5,
                    display: 'error'
                });
                console.log(res);
            });
        });
};

const deleteTemplateCallback = (t) => {
    fetch(`${HOST}/webserver/bpm/template/delete`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            'boardId': t.getContext()['board'],
        })
    }).then(function (res) {
        return res.json();
    }).then(function (data) {
        t.set('board', 'shared', 'isActiveProcess', false);
        t.alert({
            message: 'Шаблон успешно удален',
            duration: 5,
            display: 'success'
        });

    }).catch(function (res) {
        t.alert({
            message: 'Что-то пошло не так',
            duration: 5,
            display: 'error'
        });
        console.log(res);
    });
};

const createSubProcesss = (t) =>
    renderTemplateDropdown(t, item => (t) => {
        t.closePopup();
        t.alert({
            message: 'Создаеться подпроцесс',
            duration: 5,
            display: 'success'
        });
        t.set('card', 'shared', 'isActiveProcess', true);
        return t.card('id', 'name', 'members', 'attachments', 'url')
            .then(card => fetch(`${HOST}/webserver/bpm/template/use`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    templateId: item.id,
                    userId: card.members.length > 0 ? card.members[0].id : t.getContext()['member'],
                    parentCardId: card.id,
                    parentId: t.getContext()['board'],
                    parentName: card.name
                })
            }).then(function (res) {
                return res.json();
            }).then(function (result) {
                if (result.success) {
                    return t.navigate({
                        url: result.data.url
                    });
                }
                return t.alert({
                    message: 'Что-то пошло не так',
                    duration: 5,
                    display: result.errors || 'error'
                });
            }).catch(function (res) {
                t.set('card', 'shared', 'isActiveProcess', false);
                t.alert({
                    message: 'Что-то пошло не так',
                    duration: 5,
                    display: 'error'
                });
                console.log(res);
            }));
    });

const init = () => {
    TrelloPowerUp.initialize({
        'board-buttons': function (t, options) {
            checkIsActiveProcessQuery(t);
            window.TrelloBPMPlugin = t;
            return t.get('board', 'shared', 'isActiveProcess', false)
                .then(isActiveProcess => {
                    const CONST_BUTTONS = [
                        // TODO был удален потому что не дает базовой доски
                        //     {
                        //     icon: GLITCH_ICON,
                        //     text: 'Использовать шаблон',
                        //     condition: 'edit',
                        //     callback: useTemplateCallback
                        // },
                        {
                            text: 'HELP',
                            condition: 'edit',
                            callback: openInstructionCallback
                        }, {
                            text: 'Add user',
                            condition: 'edit',
                            callback: openAddUserCallback
                        }];

                    const DYNAMIC_BUTTONS = [
                        // { TODO был удален потому что не дает базовой доски
                        // icon: GRAY_ICON,
                        // text: isActiveProcess ? 'Обновить процесс' : 'Запустить процесс',
                        //     condition: 'edit',
                        //     callback: startProcessCallback
                        // },
                        {
                        icon: GRAY_ICON,
                        text: 'Сохранить шаблон',
                        condition: 'edit',
                        callback: createTemplateCallback
                    }, {
                        icon: GRAY_ICON,
                        text: 'Удалить шаблон',
                        condition: 'edit',
                        callback: deleteTemplateCallback
                    }];
                    return isActiveProcess ? CONST_BUTTONS : CONST_BUTTONS.concat(DYNAMIC_BUTTONS);
                });
        },
        // 'card-badges': (t, opts) => Promise.all([t.cards('customFieldItems', 'labels'), t.board('customFields'), t.card('name')])
        //     .then(function ([cards, board, card]) {
        //         if (card.name !== 'Parent process' && card.name !== 'calcSP') {
        //             return [];
        //         }
        //
        //         const SPField = board.customFields.find(cF => cF.name === 'Story point' && cF.type === 'text');
        //         if (!SPField) {
        //             return [];
        //         }
        //         const cardsWithSP = cards
        //             .filter(card => card.customFieldItems.filter(item => item.idCustomField === SPField.id).length);
        //         const cardsWIP = cardsWithSP.filter(card => card.labels.find(label => label.name === 'WIP'));
        //         const cardsDone = cardsWithSP.filter(card => card.labels.find(label => label.name === 'Done'));
        //         const calculateSumSP = (cards) => cards
        //             .reduce(
        //                 (sum, card) => sum +
        //                     Number(
        //                         card.customFieldItems
        //                             .find(item => item.idCustomField === SPField.id)
        //                             .value
        //                             .text
        //                     ),
        //                 0
        //             );
        //
        //         const counters = {
        //             Total: calculateSumSP(cardsWithSP),
        //             WIP: calculateSumSP(cardsWIP),
        //             Done: calculateSumSP(cardsDone),
        //             Rest: calculateSumSP(cardsWithSP) - calculateSumSP(cardsDone),
        //         };
        //
        //         return Object.keys(counters).map(key => ({
        //             dynamic: () => ({
        //                 title: key,
        //                 text: `${key}: ${counters[key]}`,
        //                 refresh: 10,
        //             })
        //         }));
        //     }),
        'card-buttons': function (t, opts) {
            return Promise.all([
                t.get('board', 'shared', 'isActiveProcess', false),
                t.card('attachments', 'labels'),
                t.get('card', 'shared', 'status', 'DEV'),
                t.board('name', 'members')
            ]).then(([isActiveProcess, card, cardStatus, board]) => {
                    const isUserBoard = checkIsUserBoard(board);
                    const isActiveCard = checkIsActiveCard(card);
                    const isCardSubprocess = checkIsCardSubprocess(card);

                    console.log(isActiveProcess, isUserBoard, isActiveCard, cardStatus);

                    if (isUserBoard) {
                        switch (cardStatus) {
                            case 'DEV':
                                return [
                                    {
                                        icon: WHITE_ICON,
                                        text: 'TO TEST',
                                        callback: toTest,
                                        condition: 'edit'
                                    }, {
                                        icon: WHITE_ICON,
                                        text: 'TO CLARIFY',
                                        callback: toClarify,
                                        condition: 'edit'
                                    }
                                ];
                            case 'TEST':
                                return [{
                                    icon: WHITE_ICON,
                                    text: 'Accept',
                                    callback: (t) => review(t, 'DONE'),
                                    condition: 'edit'
                                }, {
                                    icon: WHITE_ICON,
                                    text: 'Decline',
                                    callback: (t) => review(t, 'DEV'),
                                    condition: 'edit'
                                }];
                            case 'CLARIFY':
                                return [{
                                    icon: WHITE_ICON,
                                    text: 'TO DEV',
                                    callback: (t) => review(t, 'DEV'),
                                    condition: 'edit'
                                }];
                            case 'DONE':
                                return [];
                            default:
                                return [];
                        }
                    }

                    // if (isActiveProcess) {
                    if (isCardSubprocess) {
                        return [];
                    }

                    if (isActiveCard) {
                        return [{
                            icon: WHITE_ICON,
                            text: 'Assign',
                            callback: reassignCard,
                            condition: 'edit'
                        }];
                    } else {
                        return [{
                            icon: GLITCH_ICON,
                            text: 'Add cart to process',
                            callback: addCardToProcess,
                            condition: 'edit'
                        }, {
                            icon: GRAY_ICON,
                            text: 'Subprocess',
                            callback: createSubProcesss,
                            condition: 'edit'
                        }];
                    }
                    // }
                    // return [];
                }
            )
        }
    })
};

init();
