// const HOST = 'https://bpm.skc.today';
// TODO For test use it
const HOST = 'https://dmehed.skc.today';

const LATEST_VERSION = '1.0.1';

const updateVersion = (oldVersion) =>
    fetch(`${HOST}/webserver/bpm/update_version`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            oldVersion,
            boardId: t.getContext()['board']
        })
    }).then(function (res) {
        return res.json();
    }).then(function (result) {
        if (result.success) {
            t.alert({
                message: 'Подождите пару секунд идет обновление версии доски для бпм',
                duration: 5,
                display: 'warning'
            });
            return t.set('board', 'shared', 'version', LATEST_VERSION);
        }
        return t.alert({
            message: 'Что-то пошло не так',
            duration: 5,
            display: result.errors || 'error'
        });
    });

document.addEventListener('DOMContentLoaded', () => {
    const t = window.TrelloPowerUp.iframe();

    t.get('board', 'shared', 'version', '1.0.0').then(version => {
        if (version !== LATEST_VERSION) {
            return updateVersion();
        }
    });
}
