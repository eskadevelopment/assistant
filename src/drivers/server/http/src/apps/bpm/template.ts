import { Router } from 'express';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

export const templateRouter = Router();

templateRouter.post('/use', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('GET_ONE', 'driver.postgresql', {
            name: 'bpm',
            query: 'select id, name from template where id = ${id}',
            args: { id: req.body.templateId }
        }),
        new Action('TEMPLATE:USE', 'logic.bpm', {
            parentId: req.body.parentId,
            parentCardId: req.body.parentCardId,
            parentName: req.body.parentName,
            userId: req.body.userId
        }),
    ]), { responseId: saveToPool(res) });
    p.start(s, {});
}));

templateRouter.post('/create', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('TEMPLATE:CREATE', 'logic.bpm'),
        new Action('EXECUTE', 'driver.postgresql'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, {
        boardId: req.body.boardId,
        boardName: req.body.boardName
    });
}));

templateRouter.post('/delete', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('TEMPLATE:DELETE', 'logic.bpm'),
        new Action('EXECUTE', 'driver.postgresql'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, {
        boardId: req.body.boardId,
    });
}));

templateRouter.get('/get_many', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('GET_MANY', 'driver.postgresql', {
            name: 'bpm',
            query: 'select id, name from template where enabled = true'
        }),
        new Action('TEMPLATE:GET_MANY', 'logic.bpm'),
        new Action('WRAP', 'driver.server.http'),
        new Action('RESPONSE', 'driver.server.http')
    ]), { responseId: saveToPool(res) });
    p.start(s, {});
}));
