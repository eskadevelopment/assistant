import { Router } from 'express';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

export const userRouter = Router();

userRouter.post('/create', ((req, res) => {
    const s = new State(new Route('seq', [
        new Action('USER:GET', 'driver.trello'),
        new Action('USER:CREATE', 'logic.bpm', {
            slackId: req.body.slackId,
            email: req.body.email
        })
    ]), { responseId: saveToPool(res) });
    p.start(s, {
        username: req.body.username,
    });
}));
