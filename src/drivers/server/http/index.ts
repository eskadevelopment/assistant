#! /usr/bin/env ts-node-dev
import { Action, Route, State } from 'assistant';
import './src/apps';
import { p } from './src/protocol';
import { execute, saveToPool } from './src/apps/pool';
import { apiRouter } from './src/apps/api/router';

class WebserverDriver {

    @p.action('RESPONSE')
    response(data: any, state: State) {
        console.log('RESPONSE', data);
        execute(state.data.responseId, data);
    }

    @p.action('WRAP')
    wrap(data: any, state: State) {
        if (data && data.errors) {
            return {
                success: false,
                errors: data.errors
            };
        }
        return {
            success: true,
            data: data && data.data ? data.data : data,
        };
    }

    @p.action('REGISTER')
    register(
        data: {
            method: 'POST' | 'GET' | 'PUT' | 'DELETE',
            uri: string,
            handler: {
                action: string,
                executor: string
            }
        },
        state: State
    ) {
        apiRouter[data.method.toLowerCase()].call(apiRouter, data.uri, ((req, res) => {
            const s = new State(new Route('seq', [
                new Action(data.handler.action, data.handler.executor),
                new Action('WRAP', 'driver.server.http'),
                new Action('RESPONSE', 'driver.server.http')
            ]), { responseId: saveToPool(res) });
            p.start(s, req.body);
        }));
    }
}
