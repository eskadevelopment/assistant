#! /usr/bin/env ts-node
import { State } from 'assistant';
import './src/apps';
import { p } from './src/protocol';
import { execute } from './src/apps/pool';
import { server } from './src/apps';

class WebserverDriver {

    @p.action('RESPONSE')
    response(data: any, state: State) {
        console.log('WS RESPONSE', data);
        execute(state.data.responseId, data);
    }

    @p.action('WRAP')
    wrap(data: any, state: State) {
        return {
            success: true,
            data: data,
        };
    }

    @p.action('BROADCAST')
    broadcast(data: { code: string, data: any }, state: State) {
        server
            .getMiddleware()
            .emitEvent(data.code, data.data);
    }
}

