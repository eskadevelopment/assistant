import { EVENT_METHODS, RPC_METHODS } from './socket/rpcHolder';

const buildDecorator = (methodContainer: { [methodName: string]: any }) => {
    return (rpcName: string) => {
        return function (target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
            let fn = descriptor.value;
            return {
                configurable: true,

                get() {
                    let boundFn = fn.bind(this);
                    Reflect.defineProperty(this, propertyKey, {
                        value: boundFn,
                        configurable: true,
                        writable: true
                    });

                    methodContainer[rpcName] = boundFn;

                    return function () {
                        return boundFn.apply(this, arguments);
                    };
                }
            };
        };
    };
};

export const EVENT = buildDecorator(EVENT_METHODS);
export const RPC = buildDecorator(RPC_METHODS);
