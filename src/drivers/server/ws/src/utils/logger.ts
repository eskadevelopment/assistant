import winston, { transports, createLogger } from 'winston';
const { combine, json, simple } = winston.format;

const levels = {
    archive: 0,
    error: 1,
    warn: 2,
    info: 3,
    http: 4,
    verbose: 5,
    debug: 6,
    silly: 7,
    trace: 8,
};

const console = new transports.Console({
    level: 'debug',
});

export const logger = createLogger({
    levels,
    format: combine(
        json(),
        simple()
    ),
    transports: [
        console,
    ]
});
