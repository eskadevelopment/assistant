import { Server, ServerOptions } from 'socket.io';
import { CONNECT_CHANNEL } from './channels';
import { SocketMiddleware } from './middleware';

const io = require('socket.io');

export class SocketServer {

    private readonly port: number;
    private readonly config: ServerOptions;

    public socket: Server;
    public middleware: SocketMiddleware;

    constructor(port: number, config: ServerOptions) {
        this.port = port;
        this.config = config;
    }

    getMiddleware() {
        return this.middleware;
    }

    run() {
        this.socket = io(this.port, this.config);
        this.middleware = new SocketMiddleware(this.socket);

        console.log(`[SocketServer][run] Server has been started successfully on port ${this.port}`);

        this.socket.on(CONNECT_CHANNEL, (socket: any) => this.middleware.onConnect(socket));
    }
}
