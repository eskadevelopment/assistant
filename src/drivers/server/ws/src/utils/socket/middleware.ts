import ResponseEntity from '../../model/response';
import { DISCONNECT_CHANNEL, MESSAGE_CHANNEL } from './channels';
import { RPC_METHODS } from './rpcHolder';
import { Message, MessageType } from '../../model/message';
import { Server } from 'socket.io';
import { SocketConnectionPool } from './connectionPool';
import { Socket } from '../../model/types';
import { logger } from '../logger';

type RequestProcessor = {
    socket: any,
    cleaner: any
};

const SOCKET_CONNECTIONS_LIMIT = 100;

export class SocketMiddleware {

    private requests: Map<string, RequestProcessor>;

    public connectionPool: SocketConnectionPool;
    public socketServer: Server;

    constructor(socketServer: Server) {
        this.requests = new Map();
        this.connectionPool = new SocketConnectionPool(new Set());
        this.socketServer = socketServer;
    }

    getConnectionPool(): SocketConnectionPool {
        return this.connectionPool;
    }

    onConnect(socket: any) {
        logger.debug(`[SocketMiddleware][onConnect] Socket %s connected', ${JSON.stringify(socket.handshake)}`);
        this.addSocketConnection(socket);
        socket.on(MESSAGE_CHANNEL, (data: Message<any>) => this.onClientMessage(data, socket));
    }

    addSocketConnection(socket: any) {
        if (this.getConnectionPool().getConnectionsNumber() === SOCKET_CONNECTIONS_LIMIT) {
            const response = new ResponseEntity({ errors: ['Maximum socket connections exceeded'] });
            socket.emit(MESSAGE_CHANNEL, response);
            socket.disconnect();
        } else {
            this.connectionPool.addConnection(socket);
            socket.on(DISCONNECT_CHANNEL, () => this.connectionPool.removeConnection(socket));
        }
    }

    onClientMessage<T>(message: Message<T>, socket: any) {
        const { headers } = message;

        if (headers.type === MessageType.REQUEST) {
            this.processMessage(message, socket);
        } else {
            message.body = new ResponseEntity({ errors: ['Invalid message type'] }) as any;
            socket.emit(MESSAGE_CHANNEL, message);
        }
    }

    processMessage<T>(message: Message<T>, socket: any) {
        const method = RPC_METHODS[message.code];
        if (typeof method === 'function') {
            method(message, socket);
        } else {
            const errors = new ResponseEntity({ errors: ['Invalid request. Code not found'] });
            const errorMessage = new Message(MessageType.RESPONSE, message.code, errors, message.headers.id);
            socket.emit(MESSAGE_CHANNEL, errorMessage);
        }
    }

    emitEvent(code: string, data: any, socketClient?: any) {
        const message = new Message(MessageType.EVENT, code, data);
        const connections = this.connectionPool.getAllConnections();
        if (connections) {
            connections
                .filter(socket => socket.id !== socketClient.id)
                .forEach((socket: Socket) => socket.emit(MESSAGE_CHANNEL, message));
        }
    }

    emitToClient(requestId: string, code: string, data: any, socketClient?: any) {
        const message = new Message(MessageType.RESPONSE, code, data, requestId);

        socketClient
            ? socketClient.emit(MESSAGE_CHANNEL, message)
            : this.emitAndClear(message);

    }

    emitAndClear<T>(message: Message<T>) {
        if (this.requests.has(message.headers.id)) {
            const requestProcessor = this.requests.get(message.headers.id);
            const { socket, cleaner } = requestProcessor;
            logger.debug(`[SocketMiddleware][emitAndClear] Clearing request with id: ${message.headers.id}`);
            clearTimeout(cleaner);
            socket.emit(MESSAGE_CHANNEL, message);
            this.requests.delete(message.headers.id);
        }
    }
}
