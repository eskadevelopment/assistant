import { Socket } from '../../model/types';
import { logger } from '../../utils/logger';

export class SocketConnectionPool {

    public socketPool: Set<Socket>;

    constructor(socketPool: Set<Socket>) {
        this.socketPool = socketPool;
    }

    getAllConnections(): Array<Socket> {
        const sockets = Array.from(this.socketPool.values());
        return sockets.reduce((a, socket: Socket) => a.concat(socket), []);
    }

    getConnectionsNumber(): number {
        return this.socketPool.size;
    }

    addConnection(socket: Socket): void {
        this.socketPool.add(socket);
        logger.debug(`[SocketConnectionPool][addConnection] Adding socket connection`);
    }

    removeConnection(socket: Socket) {
        logger.debug(`[SocketConnectionPool][removeConnection] 'Removing socket connection`);
        this.socketPool.delete(socket);
    }
}
