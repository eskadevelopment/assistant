export type Address = string;
export type PublicKey = string;
export type Timestamp = number;

export type Socket = any;
export type Secret = { secret: string };
export type Amount = { amount: number };

export type Error = string;

export type Filter = {
    limit: number,
    offset: number,
    arraySlice?: number
};

export type SortType = 'ASC' | 'DESC';

export type Sort = [string, SortType];

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
