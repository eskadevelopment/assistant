import { server } from './index';

export type ResponseId = string;

const REQUEST_POOL = new Map<ResponseId, any>();

export const saveToPool = (requestId: string, code: string, socket: any) => {
    REQUEST_POOL.set(requestId, {
        code,
        socket: socket
    });
    return requestId;
};

export const execute = (responseId: ResponseId, body: any): void => {
    const responseContext = REQUEST_POOL.get(responseId);
    if (responseContext) {
        REQUEST_POOL.delete(responseId);
        server
            .getMiddleware()
            .emitToClient(responseId, responseContext.code, body, responseContext.socket);
    }
};
