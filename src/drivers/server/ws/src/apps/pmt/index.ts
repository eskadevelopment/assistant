import { RPC } from '../../utils/decorators';
import { Message } from '../../model/message';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

class PMTController {

    constructor() {
        this.getWIPTask = this.getWIPTask.bind(this);
        this.getAllTask = this.getAllTask.bind(this);
        this.getTimeSpentReport = this.getTimeSpentReport.bind(this);
        this.getUsers = this.getUsers.bind(this);
        this.getProjects = this.getProjects.bind(this);
        this.getProjectsForFilter = this.getProjectsForFilter.bind(this);
        this.getResources = this.getResources.bind(this);
    }


    @RPC('GET_WIP_TASKS')
    getWIPTask(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_WIP_TASKS', 'logic.bpm'),
                new Action('WRAP', 'driver.server.ws'),
                new Action('RESPONSE', 'driver.server.ws'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s);
    }

    @RPC('GET_ALL_TASKS')
    getAllTask(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_ALL_TASKS', 'logic.bpm'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s, message.body);
    }

    @RPC('GET_TIME_SPENT_REPORT')
    getTimeSpentReport(message: Message<{ filter: { date: string } }>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_TIME_SPENT_REPORT', 'logic.bpm'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s, {
            filter: message.body.filter
        });
    }

    @RPC('GET_USERS')
    getUsers(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_USERS', 'logic.bpm'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s);
    }

    @RPC('GET_PROJECTS')
    getProjects(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_PROJECTS', 'logic.bpm'),
                new Action('WRAP', 'driver.server.ws'),
                new Action('RESPONSE', 'driver.server.ws'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s);
    }

    @RPC('GET_PROJECTS_FOR_FILTER')
    getProjectsForFilter(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_PROJECTS_FOR_FILTER', 'logic.bpm'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s);
    }

    @RPC('GET_RESOURCES')
    getResources(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
                new Action('GET_RESOURCES', 'logic.bpm'),
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket) }
        );
        p.start(s);
    }

}


export default new PMTController();
