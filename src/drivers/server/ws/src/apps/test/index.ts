import { RPC } from '../../utils/decorators';
import { Message } from '../../model/message';
import { server } from '../index';
import { Action, Route, State } from 'assistant';
import { saveToPool } from '../pool';
import { p } from '../../protocol';

class TestController {

    constructor() {
        this.test = this.test.bind(this);
        this.test2 = this.test2.bind(this);
    }

    @RPC('TEST')
    test(message: Message<any>, socket: any) {
        console.log('test');
        server
            .getMiddleware()
            .emitToClient(message.headers.id, message.code, { 'test': 'test' }, socket);
    }

    @RPC('TEST2')
    test2(message: Message<any>, socket: any) {
        const s = new State(new Route('seq', [
            new Action('WRAP', 'driver.server.ws'),
            new Action('RESPONSE', 'driver.server.ws')
            ]),
            { responseId: saveToPool(message.headers.id, message.code, socket)  }
        );
        p.start(s, s);
    }

}


export default new TestController();
