import { Action, State } from 'assistant';
import { Protocol } from 'assistant/protocol';
import { Route } from 'assistant';

const p = new Protocol(null);

const s = new State(new Route('seq', [ new Action('WRAP', 'driver.server.ws') ]), {});

p.start(s, {
    test: 'test'
});
