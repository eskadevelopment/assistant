// run every business day's morning at 11 hours (including UTC time)

export const DEFAULT_HOUR = '8';
export const HOUR_AT = process.env.HOUR_AT || DEFAULT_HOUR;
export const SPENT_TIME_PATTERN = `0 ${HOUR_AT} * * 1,2,3,4,5`;
