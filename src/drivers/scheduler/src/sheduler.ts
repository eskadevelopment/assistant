import { Job } from 'node-schedule';
import { Task } from '../src/model/task';

interface TaskScheduler {

    registerAndRun(pattern: string | Date, job: Job): boolean;

    register(pattern: string | Date, job: Job): boolean;

    run(name: string): boolean;

    isTaskRegistered(name: string): boolean;

    isTaskStarted(name: string): boolean;

    remove(name: string): boolean;

    cancel(name: string): boolean;

}

export class SchedulerService implements TaskScheduler {

    private registeredTasks: Map<string, Task>;
    private startedTasks: Map<string, Job>;

    constructor() {
        this.registeredTasks = new Map();
        this.startedTasks = new Map();
    }

    register(pattern: string | Date, job: Job) {
        if(!this.registeredTasks.has(job.name)){
            this.registeredTasks.set(job.name, new Task(pattern, job));
            return true;
        }
        return false;
    }

    registerAndRun(pattern: string | Date, job: Job): boolean {
        return this.register(pattern, job) && this.run(job.name);
    }

    run(name: string): boolean {
        const task = this.registeredTasks.get(name);
        if (task) {
            task.job.schedule(task.pattern);
            return true;
        }
        return false;
    }

    cancel(name: string): boolean {
        const startedJob = this.startedTasks.get(name);
        if (startedJob) {
            startedJob.cancel();
            return this.startedTasks.delete(name);
        }
        return false;
    }

    remove(name: string): boolean {
        this.cancel(name);
        return this.registeredTasks.delete(name);
    }

    isTaskRegistered(name: string): boolean {
        return this.registeredTasks.has(name);
    }

    isTaskStarted(name: string): boolean {
        return this.startedTasks.has(name);
    }


}

export default new SchedulerService();
