import { Job } from 'node-schedule';

export class Task {

    pattern: string | Date;
    job: Job;

    constructor(pattern: string | Date, job: Job) {
        this.pattern = pattern;
        this.job = job;
    }
}
