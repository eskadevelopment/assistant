import { Action, Route, State } from 'assistant';
import { getLastBusinessDay } from '../util/date';
import { Job } from 'node-schedule';
import { p } from '../protocol';

const SPENT_TIME_JOB_NAME = 'SPENT_TIME';

const actions = [
    new Action('SPENT_TIME', 'logic.spent_time'),
    new Action('GET_MANY', 'driver.postgresql'),
    new Action('PROCESS', 'logic.spent_time')
];

export const spentTimeExecutor = () => {
    const state = new State(new Route('seq', actions), { spentTimeDate: getLastBusinessDay() });
    p.start(state, {});
};

export const spentTimeJob = new Job(SPENT_TIME_JOB_NAME, spentTimeExecutor);

