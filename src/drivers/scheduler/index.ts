#! /usr/bin/env ts-node
import scheduler from './src/sheduler';
import { spentTimeJob } from './src/tasks/spentTime';
import { State } from 'assistant';
import { p } from './src/protocol';
import { Task } from './src/model/task';
import { SPENT_TIME_PATTERN } from './src/config';

// scheduler.registerAndRun('*/10 * * * * *', spentTimeJob);
scheduler.registerAndRun(SPENT_TIME_PATTERN, spentTimeJob);

class SchedulerDriver {

    @p.action('REGISTER_AND_RUN')
    registerAndRun(data: Task, state: State) {
        return scheduler.registerAndRun(data.pattern, data.job);
    }

    @p.action('REGISTER')
    register(data: Task, state: State) {
        return scheduler.register(data.pattern, data.job);
    }

    @p.action('RUN')
    run(data: { name: string }, state: State) {
        return scheduler.run(data.name);
    }

    @p.action('IS_TASK_REGISTERED')
    isTaskRegistered(data: { name: string }, state: State) {
        return scheduler.isTaskRegistered(data.name);
    }

    @p.action('IS_TASK_STARTED')
    isTaskStarted(data: { name: string }, state: State) {
        return scheduler.isTaskStarted(data.name);
    }

    @p.action('REMOVE')
    remove(data: { name: string }, state: State) {
        return scheduler.remove(data.name);
    }

    @p.action('CANCEL')
    cancel(data: { name: string }, state: State) {
        return scheduler.cancel(data.name);
    }
}
