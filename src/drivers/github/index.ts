#! /usr/bin/env ts-node
import { Action, Route, State } from 'assistant';
import { Protocol } from 'assistant/protocol';
import Octokit = require('@octokit/rest');

const express = require('express');
const bodyParser = require('body-parser');

const p = new Protocol('driver.github');

enum WORKSPACE {
    ESKA = 'sk-consultingteam',
    DDK = 'sk-ddk',
    PRM = 'upc2',
    ESKA_TEST = 'eska-test'
};

const octokit = new Octokit();

const getRepoListCommits = async (body: Octokit.ReposListCommitsParams) => {
    const result = await octokit.repos.listCommits(body);
    return result.data.map(commit => ({
        author: commit.author ? commit.author.login : 'Unknown',
        message: commit.commit.message
    }));
};

export enum GithubEventsEnum {
    RELEASE = 'release',
    CREATE = 'create',
    ISSUES = 'issues',
    PUSH = 'push',
}

const app = express();
app.use(bodyParser());
app.post('/github/events', (req, res) => {
    res.send('ok');
    const event = req.headers['x-github-event'];
    const webhookBody: any = req.body;
    switch (event) {
        case GithubEventsEnum.RELEASE:
            const release: Octokit.ReposGetReleaseResponse = webhookBody.release;

            const tag = release.tag_name;
            const url = release.html_url;
            const author = release.author.login;
            const name = release.name;

            const s1 = new State(new Route('seq', [new Action('SEND', 'driver.slack')]), {});
            p.start(s1, {
                text: `Created tag ${tag} \n author: ${author}\n url: ${url} \n description: ${name}`,
                workspace: WORKSPACE.DDK,
                channel: 'CFKFBA1V1'
            });
            break;
        case GithubEventsEnum.CREATE:
            console.log('create', webhookBody);
            break;
        case GithubEventsEnum.ISSUES:
            console.log(webhookBody.issue);
            break;
        case GithubEventsEnum.PUSH:

            const commits: any = webhookBody.commits;
            const headCommit = webhookBody.head_commit;

            if (webhookBody.ref === 'refs/heads/master') {
                const s2 = new State(new Route('seq', [new Action('SEND', 'driver.slack')]), {});
                console.log('commits', commits)
                p.start(s2, {
                    text: `Master update ${headCommit.message} ${commits.map(commit => commit.url).join(', ')}`,
                    workspace: WORKSPACE.ESKA,
                    channel: 'GMLRGLW9F'
                });
                console.log('push', webhookBody);
            }
            break;
        default:
            console.log(event);
    }
});


class GithubActions {

    @p.action('REPO_LIST_COMMITS')
    async repoListCommits(data: any, state: State) {
        console.log('REPO_LIST_COMMITS', data, state);
        return await getRepoListCommits(data);
    }
}

app.listen(Number(process.env.PRODUCER_PORT || '8084'), process.env.PRODUCER_HOST || '0.0.0.0', () => {
    console.log(
        `Github producer start on ${process.env.PRODUCER_HOST || '0.0.0.0'}:${process.env.PRODUCER_PORT || '8084'}`
    );
});
