import { Route, State } from 'assistant';
import { Protocol } from 'assistant/protocol';
import { Action } from 'assistant/models/action';

const p = new Protocol(null);

const s = new State(new Route('seq', [
    new Action('REPO_LIST_COMMITS', 'driver.github')
]), {
    'init': 'init'
});

p.start(s, {
    owner: 'AraiEzzra',
    repo: 'DDKCORE',
    page: 1
});
