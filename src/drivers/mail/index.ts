#! /usr/bin/env ts-node
import { Action, Protocol, State } from 'assistant';
import MailListener from './src/MailListener';
import OAuth2, { OAuth2UserOptions } from './src/OAuth2';
import { ParsedMail } from 'mailparser';
import credentials from './config/credentials.json';
import user from './config/user.json';
import { Route } from '../../core/protocol/ts';

const mailListenerUser = async (user: OAuth2UserOptions) => {
    const oAuth2 = new OAuth2({
        clientId: credentials.installed.client_id,
        clientSecret: credentials.installed.client_secret
    });
    const token = await oAuth2.getTokenByUser(user);

    const mailListener = new MailListener({
        mailbox: 'INBOX',
        imap: {
            user: user.login,
            password: user.password,
            xoauth2: token
        }
    });

    mailListener.on('connected', () => {
        console.log(`driver.mail[connected] by ${user.login}`);
    });

    mailListener.on('disconnected', () => {
        console.log(`driver.mail[disconnected] by ${user.login}`);
    });

    mailListener.on('error', error => {
        console.log('driver.mail[error]', error);
    });

    mailListener.on('mail', (mail: ParsedMail) => {
        const state = new State(new Route('seq', [new Action('SEND', 'driver.slack')]), {});
        p.start(state, {
            text: `New mail ${mail.subject}`,
            workspace: 'sk-ddk',
            channel: 'CKABWJ1J8'
        });
    });

    mailListener.connect();
};

const p = new Protocol('driver.mail');

try {
    mailListenerUser(user);
} catch (e) {
    console.log(e);
}


