import xoauth2 from 'xoauth2';

export type OAuth2Options = {
    clientId: string,
    clientSecret: string,
}

export type OAuth2UserOptions = {
    login: string,
    password: string,
    refreshToken: string
}

export default class OAuth2 {

    private options: OAuth2Options;

    constructor(options: OAuth2Options) {
        this.options = options;
    }

    getTokenByUser(user: OAuth2UserOptions): Promise<string> {
        const generator = xoauth2.createXOAuth2Generator({
            user: user.login,
            clientId: this.options.clientId,
            clientSecret: this.options.clientSecret,
            refreshToken: user.refreshToken
        });

        return new Promise((resolve, reject) => {
            generator.getToken((err, token) => {
                err ? reject(err) : resolve(token);
            });
        });
    }
}
