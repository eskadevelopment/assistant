import { EventEmitter } from 'events';
import { simpleParser } from 'mailparser';
import Imap, { Config, Box } from 'imap';
import ReadableStream = NodeJS.ReadableStream;

export type MailListenerOptions = {
    mailbox: string,
    imap: Config
}

const FETCH_OPTIONS = {
    bodies: '',
    struct: true
};

const defaultImapOptions = {
    host: 'imap.gmail.com',
    port: 993,
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
};

export default class MailListener extends EventEmitter {

    public readonly imap: Imap;

    private readonly options: MailListenerOptions;

    private box: Box;

    constructor(options: MailListenerOptions) {
        super();

        this.options = options;
        this.imap = new Imap({
            ...defaultImapOptions,
            ...options.imap
        });

        this.imap.once('ready', () => this.onReady());
        this.imap.once('close', () => this.onClose());
        this.imap.on('error', error => this.onError(error));
    }

    private openBox(mailbox: string, readOnly: boolean = true): Promise<Box> {
        return new Promise((resolve, reject) => {
            this.imap.openBox(mailbox, readOnly, (error, box) => {
                error ? reject(error) : resolve(box);
            });
        });
    }

    private fetch(source: any): Promise<ReadableStream> {
        return new Promise((resolve, reject) => {
            const f = this.imap.seq.fetch(source, FETCH_OPTIONS);

            f.on('message', msg => {
                msg.on('body', source => resolve(source));
            });

            f.once('error', error => reject(error));
        });
    }

    private async onReady() {
        try {
            this.box = await this.openBox(this.options.mailbox);
            this.imap.on('mail', count => this.onMail(count));
        } catch (error) {
            this.onError(error);
        }
    }

    private async onMail(count: number) {
        try {
            const query = `${this.box.messages.total}:${this.box.messages.total + count}`;
            const source = await this.fetch(query);
            const mail = await simpleParser(source);

            this.emit('mail', mail);

        } catch (error) {
            this.onError(error);
        }
    }

    private onClose() {
        this.emit('disconnected');
    }

    private onError(error: any) {
        this.emit('error', error);
    }

    connect() {
        this.imap.connect();
    }

    end() {
        this.imap.end();
    }
}
