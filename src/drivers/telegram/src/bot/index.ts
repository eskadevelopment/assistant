import Telegraf, { ContextMessageUpdate } from 'telegraf';
import { Protocol, Action, Route, State } from 'assistant';
import { WORKSPACE } from 'slack-driver/src/configs';
import { BOT_CONFIGS } from 'config';

const p = new Protocol(null);
const state = new State(new Route('seq', [
    new Action('SEND', 'driver.telegram'),
]), {});

const slackState = new State(new Route('seq', [
    new Action('SEND', 'driver.slack')
]));

class TelegramBot {
    public bot: Telegraf<ContextMessageUpdate>;
    private bot_token = BOT_CONFIGS.bender;
    private options = {
        telegram: {
            agent: null,
            webhookReply: true
        },
    };

    constructor() {
        this.bot = new Telegraf(this.bot_token, this.options);
        this.init();
    }

    private init = async () => {
        this.bot.start((ctx) => ctx.reply('Welcome!'));
        this.bot.help((ctx) => ctx.reply('Send me a sticker'));
        this.bot.on('sticker', (ctx) => ctx.reply('👍'));
        this.bot.on('text', (ctx, next) => {
            const { from, text } = ctx.message;
            console.log(ctx.message);
            p.start(state, {
                text: `${from.first_name} ${from.last_name}: ${text}`
            });
            p.start(slackState, {
                workspace: WORKSPACE.ESKA_TEST,
                text: `${from.first_name} ${from.last_name}: ${text}`,
                channel: 'CKECMHK1B',
            });
            next();
        });
        this.bot.hears('hi', (ctx) => ctx.reply('Hey there'));

        try {
            await this.bot.launch();
            console.log('Bot has been started...');
        } catch (e) {
            console.log('Launch error: ', e);
        }
    };
}

export default new TelegramBot();
