#! /usr/bin/env ts-node
import { State } from 'assistant';
import { p } from './src/protocol';

class TelegramDriver {
    @p.action('SEND')
    async test(data: any, state: State) {
        console.log('SEND: ', data.text);
    }
}
