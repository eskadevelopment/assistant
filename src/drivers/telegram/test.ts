import { Route, State } from 'assistant';
import { Protocol } from 'assistant/protocol';
import { Action } from 'assistant/models/action';

const p = new Protocol(null);

const s = new State(new Route('seq', [new Action('SEND', 'driver.telegram')]), {});

p.start(s, {
    text: `TEST`,
});
