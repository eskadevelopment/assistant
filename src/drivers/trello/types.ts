export type TrelloId = string;

export enum TrelloPos {
    TOP = 'top',
    BOT = 'bottom'
}

export enum TrelloColors {
    GREEN = 'green',
    YELLOW = 'yellow',
    ORANGE = 'orange',
    RED = 'red',
    PURPLE = 'purple',
    BLUE = 'blue',
    SKY = 'sky',
    LIME = 'lime',
    PINK = 'pink',
    black = 'black',
    NULL = 'null',
}

export enum TrelloCardObjects {
    ALL = 'all',
    ATTACHMENTS = 'attachments',
    CHECKLISTS = 'checklists',
    COMMENTS = 'comments',
    DUE = 'due',
    LABELS = 'labels',
    MEMBERS = 'members',
    STICKERS = 'stickers',
}

export enum TrelloUserRole {
    ADMIN = 'admin',
    NORMAL = 'normal',
    OBSERVER = 'observer',
}

export enum Permission {
    ORG = 'org',
    PRIVATE = 'private',
    PUBLIC = 'public'
}

export enum CustomFieldType {
    CHECKBOX = 'checkbox',
    DATE = 'date',
    DROPDOWN = 'list',
    NUMBER = 'number',
    TEXT = 'text'
}

export type CreateHookData = { callbackURL: string, idModel: TrelloId, description: string };
export type DeleteHookData = { id: TrelloId };
export type AddLabelToCardData = { cardId: TrelloId, labelId: TrelloId };
export type DeleteLabelFromCardData = { cardId: TrelloId, labelId: TrelloId };
export type AddMemberToCardData = { cardId: TrelloId, memberId: TrelloId, data?: any };
export type DeleteMemberFromCardData = { cardId: TrelloId, memberId: TrelloId, data?: any };
export type MoveCardData = { cardId: TrelloId, boardId: string, listId: string, pos?: TrelloPos };
export type GetAttachmentsData = { cardId: TrelloId, data?: any };
export type RemoveAttachmentData = { cardId: TrelloId, attachmentId: string, data?: any };
export type AddAttachmentData = { cardId: TrelloId, data?: any, attachment: {
    name?: string,
    mimeType?: string,
    url?: string,
} };
export type AddMemberToBoardData = { boardId: TrelloId, memberId: TrelloId, type: TrelloUserRole };
export type CreateLabelData = { boardId: TrelloId, name: string, color: TrelloColors };
export type CreateBoardData = {
    name: string,
    description?: string,
    organizationId: TrelloId,
    permission?: Permission,
    sourceId?: TrelloId
};
export type CreateListData = { boardId: TrelloId, name: string, pos?: TrelloPos, sourceListId?: TrelloId };
export type CreateCardData = {
    name: string,
    listId: TrelloId,
    desc?: string,
    due?: Date,
    dueComplete?: boolean,
    pos?: TrelloPos | number,
    idMembers?: Array<TrelloId>,
    idLabels?: Array<TrelloId>,
    urlSource?: string,
    // fileSource?: File,
    idCardSource?: TrelloId,
    keepFromSource?: Array<TrelloCardObjects> | string

};

export const DEFAULT_CARD_FIELDS = [
    'badges',
    'checkItemStates',
    'closed',
    'dateLastActivity',
    'desc',
    'descData',
    'due',
    'email',
    'idBoard',
    'idChecklists',
    'idLabels',
    'idList',
    'idMembers',
    'idShort',
    'idAttachmentCover',
    'manualCoverAttachment',
    'labels',
    'name',
    'pos',
    'shortUrl',
    'url'
];
