const request = require('request');
const querystring = require('querystring');

type Option = {
    url: string,
    method: string,
    formData?: {
        key: string,
        token: string,
        url?: string,
        file?: string
    },
    json?: object,
    key?: string,
    token?: string
};

export class TrelloClient {

    private readonly apiKey: string;
    private readonly token: string;
    private readonly baseUrl: string;

    constructor(key: string, token: string, baseUrl?: string) {
        if (!key) {
            throw new Error('Application API key is required');
        }

        this.apiKey = key;
        this.token = token;
        this.baseUrl = baseUrl || 'https://api.trello.com/1';
    }

    public getToken() {
        return this.token;
    }

    async get<T>(uri: string, data: any): Promise<T> {
        Array.prototype.unshift.call(arguments, 'GET');
        return this.request.apply(this, arguments);
    };

    async post<T>(uri: string, data: any): Promise<T> {
        Array.prototype.unshift.call(arguments, 'POST');
        return this.request.apply(this, arguments);
    };

    async put<T>(uri: string, data: any): Promise<T> {
        Array.prototype.unshift.call(arguments, 'PUT');
        return this.request.apply(this, arguments);
    };

    async del<T>(uri: string, data: any): Promise<T> {
        Array.prototype.unshift.call(arguments, 'DELETE');
        return this.request.apply(this, arguments);
    };

    private async request<T>(method, uri, args): Promise<T> {

        let url = this.baseUrl + (uri[0] === '/' ? '' : '/') + uri;

        if (method === 'GET') {
            url += '?' + querystring.stringify(this.addAuthArgs(this.parseQuery(uri, args)));
        }

        const options: Option = {
            url: url,
            method: method
        };

        if (args.attachment) {
            options.formData = {
                key: this.apiKey,
                token: this.token
            };

            if (typeof args.attachment === 'string' || args.attachment instanceof String) {
                options.formData.url = args.attachment;
            } else {
                options.formData.file = args.attachment;
            }
        } else {
            options.json = this.addAuthArgs(this.parseQuery(uri, args));
        }

        return new Promise(((resolve, reject) => {
            request[method === 'DELETE' ? 'del' : method.toLowerCase()](options, function (err, response, body) {
                if (!err && response.statusCode >= 400) {
                    err = new Error(body);
                    err.statusCode = response.statusCode;
                    err.responseBody = body;
                    err.statusMessage = require('http').STATUS_CODES[response.statusCode];
                    console.error(JSON.stringify(err.responseBody));
                    console.error(err.stack);
                    reject(err);
                } else {
                    resolve(body);
                }
            });
        }));
    };

    private addAuthArgs(args: Option) {
        args.key = this.apiKey;

        if (this.token) {
            args.token = this.token;
        }

        return args;
    };

    private parseQuery(uri: string, args: Option) {
        if (uri.indexOf('?') !== -1) {
            const ref = querystring.parse(uri.split('?')[1]);
            console.log(ref);
            Object.keys(ref).forEach(key => {
                args[key] = ref[key];
            });
        }
        Object.keys(args).filter(key => args[key] === undefined).forEach(key => {
            delete args[key];
        });
        return args;
    };
}
