import { OAuth } from 'oauth';

const requestUrl = 'https://trello.com/1/OAuthGetRequestToken';
const accessUrl = 'https://trello.com/1/OAuthGetAccessToken';
const authorizeUrl = 'https://trello.com/1/OAuthAuthorizeToken';

export class TrelloOauth {
    private oauth: OAuth;
    private readonly appName: string;

    constructor(key, secret, loginCallback, appName) {
        this.oauth = new OAuth(requestUrl, accessUrl, key, secret, '1.0', loginCallback, 'HMAC-SHA1');
        this.appName = appName;
    };

    getRequestToken(callback) {
        const appName = this.appName;

        this.oauth.getOAuthRequestToken((error, token, tokenSecret, results) => {
            if (error) {
                return callback(error, null);
            }

            callback(null, {
                oauth_token: token,
                oauth_token_secret: tokenSecret,
                redirect: authorizeUrl + ('?oauth_token=' + token + '&name=' + appName)
            });
        });
    };

    getAccessToken(bag, callback) {
        const token = bag.oauth_token;
        const tokenSecret = bag.oauth_token_secret;
        const verifier = bag.oauth_verifier;

        this.oauth.getOAuthAccessToken(token, tokenSecret, verifier, (error, accessToken, accessTokenSecret, results) => {
            if (error) {
                return callback(error, null);
            }

            callback(null, {
                oauth_token: token,
                oauth_token_secret: tokenSecret,
                oauth_access_token: accessToken,
                oauth_access_token_secret: accessTokenSecret
            });
        });
    };
}
