import { p } from '../protocol';
import { Action, Route, State } from 'assistant';

enum UserBoardList {
    BACKLOG = 'Backlog',
    TO_DO = 'TO DO',
    WIP = 'In Progress',
    PENDING = 'Pending',
    DONE = 'Done'
}


export const userBoardHooks = (action: any) => {
    const translationKey = action.display.translationKey;
    console.log('WEBHOOK:KEY:', translationKey);

    switch (translationKey) {
        case 'action_added_list_to_board':
            console.log('action_added_list_to_board');
            break;
        case 'action_move_card_from_list_to_list':
        case 'action_move_card_to_board':
            console.log(action.data);
            const card = action.data.card;
            const listAfter = action.data.listAfter || action.data.list;
            let stateAction = '';
            switch (listAfter.name) {
                case UserBoardList.BACKLOG:
                    stateAction = 'USER_CARD_MOVE_TO_BACKLOG';
                    break;
                case UserBoardList.TO_DO:
                    stateAction = 'USER_CARD_MOVE_TO_TO_DO';
                    break;
                case UserBoardList.WIP:
                    stateAction = 'USER_CARD_MOVE_TO_WIP';
                    break;
                case UserBoardList.PENDING:
                    stateAction = 'USER_CARD_MOVE_TO_PENDING';
                    break;
                case UserBoardList.DONE:
                    stateAction = 'USER_CARD_MOVE_TO_DONE';
                    break;
                default:
                    stateAction = 'USER_CARD_MOVE_TO_BACKLOG';
            }
            const s = new State(new Route('seq', [new Action(stateAction, 'logic.bpm')]));
            // console.log(card);
            console.log(action.display.entities.card);
            p.start(s, {
                cardId: card.id,
                boardId: action.data.board.id,
                listId: card.idList,
            });
            break;
        default:
            return;
    }
};
