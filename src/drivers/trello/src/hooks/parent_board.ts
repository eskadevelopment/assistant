export const parentBoardHooks = (action: any) => {
    const translationKey = action.display.translationKey;
    console.log('WEBHOOK:KEY:', translationKey);

    switch (translationKey) {
        case 'action_added_list_to_board':
            console.log('action_added_list_to_board');
            break;
        default:
            return;
    }
};
