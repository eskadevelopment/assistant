import { p } from '../protocol';
import { CreateListData, TrelloId } from '../../types';
import { State } from 'assistant';
import { trello, trelloClient } from '../client';

export enum TrelloListAction {
    CREATE = 'LIST:CREATE',
    GET_CARDS = 'LIST:GET_CARDS'
}

class TrelloListController {

    @p.action(TrelloListAction.CREATE)
    async createList(data: CreateListData, state: State) {
        return await trelloClient.post('/lists', {
            idBoard: data.boardId,
            name: data.name,
            pos: data.pos,
            idListSource: data.sourceListId
        }, );
    }

    @p.action(TrelloListAction.GET_CARDS)
    async getCards(data: { listId: TrelloId, fields: Array<string> }, state: State) {
        return await trelloClient.get(`/lists/${data.listId}/cards`, {
            fields: data.fields ? data.fields.join(',') : undefined
        });
    }
}
