import { p } from '../protocol';
import {
    AddAttachmentData,
    AddLabelToCardData,
    AddMemberToCardData,
    CreateCardData,
    DEFAULT_CARD_FIELDS,
    DeleteLabelFromCardData,
    DeleteMemberFromCardData,
    GetAttachmentsData,
    MoveCardData,
    RemoveAttachmentData,
    TrelloId,
    TrelloPos
} from '../../types';
import { State } from 'assistant';
import { trello, trelloClient } from '../client';

export enum TrelloCardAction {
    GET = 'CARD:GET',
    CREATE = 'CARD:CREATE',
    LABEL_GET_ALL = 'CARD:LABEL:GET_ALL',
    ADD_LABEL = 'CARD:ADD_LABEL',
    DELETE_LABEL = 'CARD:DELETE_LABEL',
    MEMBER_ADD = 'CARD:MEMBER:ADD',
    MEMBER_DELETE = 'CARD:MEMBER:DELETE',
    MOVE = 'CARD:MOVE',
    ATTACHMENTS_GET_ALL = 'CARD:ATTACHMENTS:GET_ALL',
    ATTACHMENTS_REMOVE = 'CARD:ATTACHMENTS:REMOVE',
    ATTACHMENTS_ADD = 'CARD:ATTACHMENTS:ADD',
    ATTACH = 'CARD:ATTACH',
    CUSTOM_FIELD = 'CARD:CUSTOM_FIELD',
    ARCHIVE = 'CARD:ARCHIVE'
}


class TrelloCardController {

    // TODO optimize
    @p.action(TrelloCardAction.GET)
    async get(data: { id: TrelloId, fields: Array<string>, checklists: 'all' | 'none' }, state: State) {
        return await trelloClient.get(`/cards/${data.id}`, {
            fields: DEFAULT_CARD_FIELDS.concat(data.fields || []).join(','),
            checklists: data.checklists || 'none',
            checklist_fields: 'all'
        });
    }

    @p.action(TrelloCardAction.CREATE)
    async createCard(data: CreateCardData, state: State) {
        return await trelloClient.post('/card/', {
            name: data.name,
            idList: data.listId,
            desc: data.desc,
            due: data.due,
            dueComplete: data.dueComplete || false,
            pos: data.pos || TrelloPos.TOP,
            idCardSource: data.idCardSource,
            idLabels: data.idLabels && data.idLabels.join(','),
            idMembers: data.idMembers && data.idMembers.join(','),
            keepFromSource: data.keepFromSource &&
                (Array.isArray(data.keepFromSource) ? data.keepFromSource.join(',') : data.keepFromSource),
            urlSource: data.urlSource
        });
    }

    @p.action(TrelloCardAction.LABEL_GET_ALL)
    async getLabels(data: { id: TrelloId }, state: State) {
        return await trelloClient.get(`/cards/${data.id}/labels`, {});
    }

    @p.action(TrelloCardAction.ADD_LABEL)
    async addLabelToCard(data: AddLabelToCardData, state: State) {
        return await trello.addLabelToCard(data.cardId, data.labelId);
    }

    @p.action(TrelloCardAction.DELETE_LABEL)
    async deleteLabelFromCard(data: DeleteLabelFromCardData, state: State) {
        return await trello.deleteLabelFromCard(data.cardId, data.labelId);
    }

    @p.action(TrelloCardAction.MEMBER_ADD)
    async addMemberToCard(data: AddMemberToCardData, state: State) {

        return await trelloClient.post(
            `/cards/${data.cardId}/idMembers`,
            { value: data.memberId },
        );
    }

    @p.action(TrelloCardAction.MEMBER_DELETE)
    async deleteMemberFromCard(data: DeleteMemberFromCardData, state: State) {
        return await trelloClient.del(
            `/cards/${data.cardId}/idMembers/${data.memberId}`,
            {},
        );
    }

    @p.action(TrelloCardAction.MOVE)
    async move(data: MoveCardData, state: State) {
        return await trelloClient.put(
            `/cards/${data.cardId}`,
            { idBoard: data.boardId, idList: data.listId, pos: data.pos || undefined },
        );
    }

    @p.action(TrelloCardAction.ATTACHMENTS_GET_ALL)
    async getAttachments(data: GetAttachmentsData, state: State) {
        const cardId = data.cardId || data.data.cardId;

        return await trelloClient.get(
            `/cards/${cardId}/attachments`,
            {},
        );
    }

    @p.action(TrelloCardAction.ATTACHMENTS_REMOVE)
    async removeAttachments(data: RemoveAttachmentData, state: State) {
        const cardId = data.cardId || data.data.cardId;
        const attachmentId = data.attachmentId || data.data.attachmentId;

        return await trelloClient.del(
            `/cards/${cardId}/attachments/${attachmentId}`,
            {},
        );
    }

    // TODO duplicated ?
    @p.action(TrelloCardAction.ATTACHMENTS_ADD)
    async addAttachment(data: AddAttachmentData, state: State) {
        const cardId = data.cardId || data.data.cardId;
        const attachment = data.attachment || data.data.attachment;

        return await trelloClient.post(
            `/cards/${cardId}/attachments`,
            attachment,
        );
    }

    @p.action(TrelloCardAction.ATTACH)
    async attach(data: { cardId: TrelloId, name?: string, url?: string }, state: State) {
        return await trelloClient.post(`/cards/${data.cardId}/attachments`, {
            name: data.name,
            url: data.url
        });
    }

    @p.action(TrelloCardAction.CUSTOM_FIELD)
    async customField(data: { cardId: TrelloId, customFieldId: TrelloId, value: { [type: string]: any } }, state: State) {
        return await trelloClient.put(`/card/${data.cardId}/customField/${data.customFieldId}/item`, {
            value: data.value
        });
    }

    @p.action(TrelloCardAction.ARCHIVE)
    async archive(data: { id: TrelloId }, state: State) {
        return await trelloClient.put(`/cards/${data.id}`, {
            closed: true
        });
    }
}
