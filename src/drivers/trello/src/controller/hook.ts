import { p } from '../protocol';
import { CreateHookData, DeleteHookData } from '../../types';
import { State } from 'assistant';
import { trello, trelloClient } from '../client';

export enum TrelloHookAction {
    CREATE = 'HOOK:CREATE',
    DELETE = 'HOOK:DELETE',
    GET_ALL = 'HOOK:GET_ALL'
}

class TrelloHookActions {

    @p.action(TrelloHookAction.CREATE)
    async createHook(data: CreateHookData, state: State) {
        return await trello.addWebhook(
            data.description,
            data.callbackURL,
            data.idModel
        );
    }

    @p.action(TrelloHookAction.DELETE)
    async deleteHook(data: DeleteHookData, state: State) {
        return await trello.deleteWebhook(data.id);
    }

    @p.action(TrelloHookAction.GET_ALL)
    async getAll(data: { token?: string }, state: State) {
        return await trelloClient.get(`/tokens/${data.token || trelloClient.getToken()}/webhooks`, {})
    }

}
