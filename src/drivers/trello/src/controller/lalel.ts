import { p } from '../protocol';
import { CreateLabelData } from '../../types';
import { State } from 'assistant';
import { trello } from '../client';

export enum TrelloLabelAction {
    CREATE = 'LABEL:CREATE'
}

class TrelloLabelController {
    /*
        Labels are defined per board, and can be applied to the cards on that board.
     */
    @p.action(TrelloLabelAction.CREATE)
    async createLabel(data: CreateLabelData, state: State) {
        return await trello.addLabelOnBoard(data.boardId, data.name, data.color);
    }
}
