import { p } from '../protocol';
import { State } from 'assistant';
import { trelloClient } from '../client';

export enum TrelloOrgAction {
    BOARD_GET_ALL = 'ORG:BOARD:GET_ALL',
    MEMBERS_GET_ALL = 'ORG:MEMBERS:GET_ALL',
}


class TrelloOrgController {

    @p.action(TrelloOrgAction.BOARD_GET_ALL)
    async getAllBoard(data: { id: string, filter: Array<string>, fields: Array<string> }, state: State) {
        return await trelloClient.get(`/organizations/${data.id}/boards`, {
            fields: data.fields ? data.fields.join(',') : undefined,
            filter: data.filter ? data.filter.join(',') : undefined,
        });
    }

    @p.action(TrelloOrgAction.MEMBERS_GET_ALL)
    async getAllMembers(data: { id: string }, state: State) {
        return await trelloClient.get(`/organizations/${data.id}/members`, { });
    }
}
