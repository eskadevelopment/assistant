import { p } from '../protocol';
import { State } from 'assistant';
import { trelloClient } from '../client';

export enum TrelloUserAction {
    GET = 'USER:GET'
}

class TrelloUserController {

    @p.action('USER:GET')
    async get(data: { id?: string, username?: string, fields: Array<string> }, state: State) {
        return await trelloClient.get(`/members/${data.id || data.username}`, {
            fields: data.fields ? data.fields.join(',') : undefined,
        });
    }
}
