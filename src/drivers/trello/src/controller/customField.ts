import { p } from '../protocol';
import { CustomFieldType, TrelloColors, TrelloPos } from '../../types';
import { State } from 'assistant';
import { trelloClient } from '../client';

export enum TrelloCustomFieldsAction {
    CREATE = 'CUSTOM_FIELD:CREATE'
}

class TrelloCustomFieldsController {

    @p.action(TrelloCustomFieldsAction.CREATE)
    async create(
        data: {
            boardId: string,
            name: string,
            type: CustomFieldType,
            options?: Array<{ color: TrelloColors, value: { text: string }, pos?: TrelloPos }>, // DROPDOWN type only
            pos?: TrelloPos,
            display: boolean
        },
        state: State
    ) {
        return await trelloClient.post(`/customFields`, {
            idModel: data.boardId,
            modelType: 'board',
            name: data.name,
            type: data.type,
            pos: data.pos,
            options: data.options,
            display: data.display
        });
    }
}
