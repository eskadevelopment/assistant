import { p } from '../protocol';
import { AddMemberToBoardData, CreateBoardData, Permission, TrelloId } from '../../types';
import { State } from 'assistant';
import { trello, trelloClient } from '../client';

export enum TrelloBoardAction {
    CREATE = 'BOARD:CREATE',
    GET = 'BOARD:GET',
    CARD_GET_ALL = 'BOARD:CARD:GET_ALL',
    ADD_MEMBER = 'BOARD:ADD_MEMBER',
    DELETE = 'BOARD:DELETE',
    MEMBERS_GET = 'BOARD:MEMBERS:GET',
    PLUGIN_ADD = 'BOARD:PLUGIN:ADD'
}

class TrelloBoardController {

    @p.action(TrelloBoardAction.CREATE)
    async createBoard(data: CreateBoardData, state: State) {
        return await trelloClient.post('boards/', {
            name: data.name,
            desc: data.description,
            idOrganization: data.organizationId,
            prefs_permissionLevel: data.permission || Permission.ORG,
            idBoardSource: data.sourceId,
            keepFromSource: 'all'
        });
    }

    @p.action(TrelloBoardAction.GET)
    async get(data: { id: TrelloId }, state: State) {
        return await trelloClient.get(`boards/${data.id}`, {
            lists: 'open'
        });
    }

    @p.action(TrelloBoardAction.CARD_GET_ALL)
    async getCards(data: { id: TrelloId, fields: Array<string> }, state: State) {
        return await trelloClient.get(`boards/${data.id}/cards`, {
            fields: data.fields ? data.fields.join(',') : undefined,
        });
    }

    @p.action(TrelloBoardAction.ADD_MEMBER)
    async addMemberToBoard(data: AddMemberToBoardData, state: State) {
        return await trello.addMemberToBoard(data.boardId, data.memberId, data.type || 'normal');
    }

    @p.action(TrelloBoardAction.DELETE)
    async delete(data: { id: TrelloId }, state: State) {
        return await trelloClient.del(`boards/${data.id}`, {});
    }

    @p.action(TrelloBoardAction.MEMBERS_GET)
    async getMembers(data: { id: TrelloId }, state: State) {
        return await trelloClient.get(`/boards/${data.id}/members`, {});
    }

    @p.action(TrelloBoardAction.PLUGIN_ADD)
    async addPlugin(data: { boardId: TrelloId, pluginId }, state: State) {
        return await trelloClient.post(`/boards/${data.boardId}/boardPlugins`, {
            idPlugin: data.pluginId
        });
    }
}
