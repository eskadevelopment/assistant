const express = require('express');
const bodyParser = require('body-parser');
import { userBoardHooks } from './hooks/user_board';
import { parentBoardHooks } from './hooks/parent_board';

const app = express();
app.use(bodyParser());

const headHandler = (req, res) => { res.send(); };
app.head('/trello/events/user_board_hook', headHandler);
app.head('/trello/events/parent_board_hook', headHandler);

app.post('/trello/events/user_board_hook', (req, res) => {
    userBoardHooks(req.body.action);
    res.json({ 'success': true });
});

app.post('/trello/events/parent_board_hook', (req, res) => {
    parentBoardHooks(req.body.action);
    res.json({ 'success': true });
});

app.listen(Number(process.env.PRODUCER_PORT || 8083), process.env.PRODUCER_HOST || '0.0.0.0', () => {
    console.log(
        `Trello producer start on ${process.env.PRODUCER_HOST || '0.0.0.0'}:${process.env.PRODUCER_PORT || 8083}`
    );
});
