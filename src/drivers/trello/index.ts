#! /usr/bin/env ts-node
import './src/app';

import './src/controller/board';
import './src/controller/card';
import './src/controller/customField';
import './src/controller/hook';
import './src/controller/lalel';
import './src/controller/list';
import './src/controller/org';
import './src/controller/user';
