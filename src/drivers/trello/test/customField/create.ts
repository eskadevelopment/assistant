import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { CustomFieldType, TrelloColors, TrelloPos } from '../../types';

const s = new State(new Route('seq', [
        new Action('CUSTOM_FIELD:CREATE', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    boardId: '5d5ccfa356e3b81af7d42871',
    name: 'test2',
    type: CustomFieldType.DROPDOWN,
    options: [
        {
            color: TrelloColors.RED,
            value: {
                text: 't1'
            },
        },
        {
            color: TrelloColors.BLUE,
            value: {
                text: 't2'
            }
        }
    ],
    pos: TrelloPos.TOP,
    display: true
});
