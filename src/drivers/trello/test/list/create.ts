import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('LIST:CREATE', 'driver.trello'),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test')
]));

p.start(s, {
    name: 'test',
    boardId: '5d94494adaf6bc2460598a83',
    pos: 'bottom'
});
