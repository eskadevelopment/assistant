import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('LIST:GET_CARDS', 'driver.trello'),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test')
]));

p.start(s, {
    listId: '5d5d16565a68f031ff81998a',
    fields: ['id', 'labels']
});
