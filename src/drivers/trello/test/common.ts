import { Protocol, State } from 'assistant';

export const HOST = 'https://dmehed.skc.today';

export const p = new Protocol('test');

class Assert {

    @p.action('ASSERT')
    assert(data: any, state: State) {
        console.log(data.data, {...data, data: undefined });
    }

    @p.action('LOG')
    log(data: any, state: State) {
        if (data.data) {
            console.log(data.name, data.data);
        } else {
            console.log(data);
        }
    }

    @p.action('EXIT')
    exit(data: any, state: State) {
        process.exit(0);
    }
}
