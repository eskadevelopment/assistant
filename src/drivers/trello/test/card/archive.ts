import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { CustomFieldType, TrelloColors, TrelloPos } from '../../types';

const s = new State(new Route('seq', [
        new Action('CARD:ARCHIVE', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    id: '5d5e4d505ddb2c155d83583b',
});
