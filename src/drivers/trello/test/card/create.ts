import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { CreateCardData, TrelloPos } from '../../types';

const s = new State(new Route('seq', [new Action('CARD:CREATE', 'driver.trello')]));

p.start<CreateCardData>(s, {
    name: 'tesxt',
    listId: '5d5293cb13cba87a43e562cd',
    keepFromSource: 'all',
    idCardSource: '5d5511b37fd89e33d5b3d4ec',
    pos: TrelloPos.TOP
});
