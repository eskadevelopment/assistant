import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { TrelloId } from '../../types';

const s = new State(new Route('seq', [new Action('CARD:CUSTOM_FIELD', 'driver.trello')]));

p.start<{ cardId: TrelloId, customFieldId: TrelloId, value: { [type: string]: any } }>(s, {
    cardId: '5d5511b37fd89e33d5b3d4ec',
    customFieldId: '5d5598d854de694568e49f70',
    value: { text: 'test2' }
});
