import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [new Action('CARD:ATTACH', 'driver.trello')]));

p.start<{ cardId: string, url: string, name: string }>(s, {
    cardId: '5d5511b37fd89e33d5b3d4ec',
    url: 'https://trello.com/c/qDJZiVAE/3-test',
    name: 'linked user card'
});
