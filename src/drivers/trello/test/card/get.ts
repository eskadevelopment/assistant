import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
        new Action('CARD:GET', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    id: '5d92f3fb95e7da7cecaaa2cf',
    checklists: 'all'
});
