import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { AddLabelToCardData } from '../../types';

const s = new State(new Route('seq', [new Action('CARD:DELETE_LABEL', 'driver.trello')]));

p.start<AddLabelToCardData>(s, {
    cardId: '5d528179d1943d736be1308b',
    labelId: '5d514a3aaf988c41f28b9cb0'
});
