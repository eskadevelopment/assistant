import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { TrelloPos } from '../../types';

const s = new State(new Route('seq', [
        new Action('CARD:MOVE', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    boardId: '5d8380eb265e9c5bd3eb011e',
    listId: '5d8380f518979d6bf6c99c87',
    cardId: '5d8b09473f02c727c5545c5b',
    pos: TrelloPos.TOP
});

//
// p.start(s, {
//     boardId: '5d8380eb265e9c5bd3eb011e',
//     listId: '5d8b093cee7fd53bb44b092c',
//     cardId: '5d8b09473f02c727c5545c5b',
// });
