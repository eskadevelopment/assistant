import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { HOST, p } from '../common';

const s = new State(new Route('seq', [
    new Action('HOOK:CREATE', 'driver.trello'),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test')
]));

p.start(s, {
    callbackURL: `${HOST}/trello/events/user_board_hook`,
    idModel: '5d760cadca110f3726238190',
    description: 'Test'
});
