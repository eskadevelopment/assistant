import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
    new Action('HOOK:DELETE', 'driver.trello'),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test')
]));

p.start(s, {
    id: '5d77a6c4692d6a0c9a21e551'
});
