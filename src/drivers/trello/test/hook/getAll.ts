import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { HOST, p } from '../common';

const s = new State(new Route('seq', [
    new Action('HOOK:GET_ALL', 'driver.trello'),
    new Action('LOG', 'test'),
    new Action('EXIT', 'test')
]));

p.start(s);
