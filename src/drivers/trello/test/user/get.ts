import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
        new Action('USER:GET', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    username: 'dmehed',
    fields: ['id', 'fullName', 'username'],
});
