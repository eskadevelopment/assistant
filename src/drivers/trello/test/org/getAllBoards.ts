import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
        new Action('ORG:BOARD:GET_ALL', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    id: 'test33428407',
    fields: ['id', 'name'],
});
