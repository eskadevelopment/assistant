import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [
        new Action('BOARD:PLUGIN:ADD', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    boardId: 'URnZBy9G',
    pluginId: '5ba513b889f21e5db6864ab1'
});
