import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { CreateBoardData, Permission } from '../../types';

const s = new State(new Route('seq', [new Action('BOARD:CREATE', 'driver.trello')]));

p.start(s, {
    name: 'test4',
    organizationId: '5ba5126204c0df5b4b63b923',
    description: 'BOARD DESCRIPTION',
    permission: Permission.ORG,
    sourceId: '5d514a3a3e1a43525bc7a279'
} as CreateBoardData);
