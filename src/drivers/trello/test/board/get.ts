import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';

const s = new State(new Route('seq', [new Action('BOARD:GET', 'driver.trello')]));

p.start(s, {
    id: 'aKP8C2Qd'
});
