import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { CreateBoardData, Permission } from '../../types';

const s = new State(new Route('seq', [new Action('BOARD:DELETE', 'driver.trello')]));

p.start(s, {
    id: 'T2XqvKf2'
});
