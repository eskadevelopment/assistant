import { Route, State } from 'assistant';
import { Action } from 'assistant/models/action';
import { p } from '../common';
import { CustomFieldType, TrelloColors, TrelloPos } from '../../types';

const s = new State(new Route('seq', [
        new Action('BOARD:CARD:GET_ALL', 'driver.trello'),
        new Action('LOG', 'test'),
        new Action('EXIT', 'test')
    ])
);

p.start(s, {
    id: 'x08TJtU2',
    fields: ['id', 'idMembers']
});
