import { scheduleJob } from 'node-schedule';
import { Protocol, Action, Route, State } from 'assistant';
import { WORKSPACE } from './src/configs';
import { Accessory } from './src/model/accessory';
import { Option } from './src/model/option';
import { Block } from './src/model/block';
import { Type } from './src/types';
import { Text } from './src/model/text';

const p = new Protocol(null);

const s = new State(new Route('seq', [new Action('SEND', 'driver.slack')]), {});

p.start(s, {
    workspace: 'sk-consultingteam',
    text: `test ts slack-driver`,
    channel: 'CA02W23S7', // test_bots eska
});

// p.start(this, s, {
//     workspace: 'sk-ddk',
//     text: `test ts slack-driver`,
//     channel: 'CKABWJ1J8', // test_bots sk-ddk
// });

const header: Block = createHeader('Spent time!');
const card1: Block = createBlock(
    'https://trello.com/c/957iYm8w/32-create-every-work-morning-requester-for-get-work-info-about-previous-work-day-prepare-interactive-message',
    'Create every work morning requester for get work info about previous work day (prepare interactive message)',
    '957iYm8w'
);
const card2: Block = createBlock(
    'https://trello.com/c/OGfreBBQ/35-prepare-logic-for-get-all-active-card-by-user-for-yesterday',
    'Prepare logic for get all active card by user for yesterday',
    'OGfreBBQ'
);

const blocks: Array<Block> = [header, card1, card2];

// TODO replace to schedule driver
function startScheduler() {
    scheduleJob('*/10 * * * * *', () => {
        console.log(new Date());
        console.log('The answer to life, the universe, and everything!');
        console.log(JSON.stringify(blocks.map(block => block.serialize())));
        p.start(s, {
            workspace: WORKSPACE.DDK,
            type: Type.SECTION,
            blocks: blocks.map(block => block.serialize()),
            channel: 'UDAMQLC1L', // test_bots
        });
    });
}

function createBlock(cardName: string, cardLink: string, blockId: string): Block {
    return new Block({
        type: Type.SECTION,
        text: createLinkCard(cardName, cardLink),
        accessory: getAccessory('hours'),
        blockId: blockId,
    });
}

function createLinkCard(cardLink: string, cardName: string) {
    return new Text({ type: Type.MRKDWN, text: `*<${cardLink}|${cardName}>*` });
}

function getAccessory(name: string) {
    let placeholder: Text = new Text({ type: Type.PLAIN_TEXT, emoji: true, text: name });
    return new Accessory({
        type: Type.STATIC_SELECT,
        placeholder: placeholder,
        options: getHoursOptions(24),
        actionId: 'selectTimeSpent',
    });
}

function getHoursOptions(hours: number): Array<Option> {
    const options: Array<Option> = [];
    for (let i = 1; i <= hours; i++) {
        let optionText: Text = new Text({ type: Type.PLAIN_TEXT, emoji: true, text: `${i}` });
        options.push(new Option({ text: optionText, value: `${i}` }));
    }
    return options;
}

function createHeader(text: string): Block {
    return new Block({
        type: Type.SECTION,
        text: { type: Type.MRKDWN, text: text },
        blockId: 'header'
    });
}

// startScheduler();
