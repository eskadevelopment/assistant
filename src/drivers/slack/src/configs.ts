import { SlackAppConfig } from '../src/types';

export enum WORKSPACE {
    ESKA = 'sk-consultingteam',
    DDK = 'sk-ddk',
    PRM = 'upc2',
    ESKA_TEST = 'eska-test'
}

export const configs = new Map<WORKSPACE, SlackAppConfig>();

configs.set(WORKSPACE.ESKA, {
    signingSecret: 'a1546ea7dd41a655ff55550d3e007647',
    token: 'xoxb-217963717521-648556705794-8F1Vz3KBZnjOcYvYlkkO5lm2',
    endpoints: { events: '/slack/eska/' },
    port: 8080
});

configs.set(WORKSPACE.DDK, {
    signingSecret: 'ca68923fec8e337216ba6eb617ed1cce',
    token: 'xoxb-451202232324-645572967571-uRqMVuIlJpQ73obisJREE01Q',
    endpoints: { events: '/slack/sk-ddk/' },
    port: 8081
});

// configs.set(WORKSPACE.PRM, {
//     signingSecret: '',
//     token: 'xoxp-217963717521-386566318342-385669233442-a3acb9662866af224ce9a8e1ba6f9e48',
//     endpoints: { events: '/slack/prm/' },
//     port: 8088
// });

configs.set(WORKSPACE.ESKA_TEST, {
    signingSecret: '19b10729f77a7c07e4e72f956cebf8f8',
    token: 'xoxb-649017671346-663334384086-gFj3ByFCvCeVYLrmpMyhlioH',
    endpoints: { events: '/slack/eska-test/' },
    port: 8082
});
