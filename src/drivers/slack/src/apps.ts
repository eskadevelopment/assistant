import { App } from '@slack/bolt'; // DOCS https://slack.dev/bolt/concepts#web-api
import { WebClient } from '@slack/web-api';
import { SlackAppConfig } from './types';
import { configs, WORKSPACE } from './configs';
import { onMessageHooks } from './hooks/message';
import { onActionHooks } from './hooks/action';

const getApp = (config: SlackAppConfig): App => {
    const app = new App(config);
    app.client = new WebClient(config.token);
    return app;
};

export const APPS = new Map<WORKSPACE, App>();

APPS.set(WORKSPACE.ESKA, getApp(configs.get(WORKSPACE.ESKA)));
APPS.set(WORKSPACE.DDK, getApp(configs.get(WORKSPACE.DDK)));
APPS.set(WORKSPACE.ESKA_TEST, getApp(configs.get(WORKSPACE.ESKA_TEST)));

export const startListeners = () => {
    [...APPS.keys()].forEach((workspace) => {
        (async () => {
            await APPS.get(workspace).start(configs.get(workspace).port);
        })();
    });
};

startListeners();
[...APPS.entries()].forEach(([workspace, app]: [WORKSPACE, App]) => {
    onMessageHooks(workspace, app);
    onActionHooks(workspace, app);
});
