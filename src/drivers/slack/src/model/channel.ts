export enum ChannelType {
    DIRECT = 'U',
    GROUP = 'G',
    CHANNEL = 'C'
}

export class Channel {
    type: ChannelType;
    target: string;

    constructor(target: string) {
        this.target = target;

        switch (this.target[0]) {
            case ChannelType.DIRECT:
                this.type = ChannelType.DIRECT;
                break;
            case ChannelType.GROUP:
                this.type = ChannelType.GROUP;
                break;
            default:
                this.type = ChannelType.CHANNEL;
        }
    }
}
