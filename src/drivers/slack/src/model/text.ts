import { Type } from '../types';

export class Text {

    type?: Type;
    emoji?: boolean;
    text: any;

    constructor(data: Text) {
        this.type = data.type;
        this.emoji = data.emoji;
        this.text = data.text;
    }
}
