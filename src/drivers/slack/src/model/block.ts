import { Text } from './text';
import { Type } from '../types';
import { Accessory } from '../model/accessory';

export type BlockStructure = {
    type?: Type;
    text?: Text;
    accessory?: Accessory;
    blockId: string;
};

export class Block {
    type?: Type;
    text?: Text;
    accessory?: Accessory;
    blockId: string;

    constructor(data: BlockStructure) {
        this.type = data.type;
        this.text = data.text;
        this.accessory = data.accessory;
        this.blockId = data.blockId;
    }

    serialize() {
        return {
            ...this,
            accessory: this.accessory && this.accessory.serialize(),
            block_id: this.blockId,
            blockId: undefined
        };
    }
}
