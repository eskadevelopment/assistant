import { Type } from '../types';
import { Option } from '../model/option';
import { Text } from '../model/text';

export type AccessoryStructure = {
    type: Type;
    placeholder: Text;
    options: Array<Option>;
    actionId: string;
};

export class Accessory {
    type: Type;
    placeholder: Text;
    options: Array<Option>;
    actionId: string;


    constructor(data: AccessoryStructure) {
        this.type = data.type;
        this.placeholder = data.placeholder;
        this.options = data.options;
        this.actionId = data.actionId;
    }

    serialize() {
        return { ...this, action_id: this.actionId, actionId: undefined };
    }
}
