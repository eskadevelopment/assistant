import { Text } from './text';

export class Option {
    text: Text;
    value: string;

    constructor(data: Option) {
        this.text = data.text;
        this.value = data.value;
    }
}
