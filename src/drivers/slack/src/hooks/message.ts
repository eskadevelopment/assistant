import { App } from '@slack/bolt';
import { p } from '../protocol';
import { WORKSPACE } from '../configs';
import { State, Action, Route } from 'assistant';

export const onMessageHooks = (workspace: WORKSPACE, app: App) => {
    app.message(async ({ message, context }) => {
        if (message.text && message.text.split(' ')[0] === 'ethan') {
            const s = new State(new Route('seq', [
                new Action('PROCESS', 'logic.bots.ethan'),
                new Action('SEND', 'driver.slack')
            ]), {});
            p.start(s, {
                text: message.text.slice('ethan'.length + 1),
                channel: message.channel,
                user: message.user,
                workspace: workspace,
            });
        }
        console.log('message', message);
    });
};
