import { App, StaticSelectAction } from '@slack/bolt';
import { p } from '../protocol';
import { WORKSPACE } from '../configs';
import { Action, Route, State } from 'assistant';
import { getDateString } from './util';

const state = new State(new Route('seq', [
    new Action('PERSIST', 'logic.spent_time'),
    new Action('EXECUTE', 'driver.postgresql')
]));

const slackState = new State(new Route('seq', [
    new Action('SEND', 'driver.slack')
]));

export const onActionHooks = (workspace: WORKSPACE, app: App) => {
    app.action('selectTimeSpent', async ({ action, context, ack, body }) => {
        // @ts-ignore
        const { user, message } = body;
        const data = action as StaticSelectAction;

        // TODO: extract this to another place
        const cardInfo = data.block_id.split('|');
        const cardId = cardInfo[0];
        const cardName = cardInfo[1];
        const date = cardInfo[2];

        p.start(slackState, {
            workspace: WORKSPACE.ESKA,
            channel: user.id,
            text: `You have spent ${data.selected_option.value} hours at ${getDateString(new Date(date))} on task: ${cardName}`
        });

        p.start(state, {
            slackId: user.id,
            hours: Number(data.selected_option.value),
            date: new Date(date),
            cardId: cardId // TODO: temporary solution this is card_id in Trello
        });
        //@ts-ignore
        ack();
    });

};
