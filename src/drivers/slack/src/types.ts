import { WORKSPACE } from './configs';
import { ChatPostMessageArguments, WebAPICallResult } from '@slack/web-api';

export type SlackAppConfig = {
    signingSecret: string;
    token: string;
    endpoints?: { events: string },
    port: number;
};

export enum Type {
    SECTION = 'section',
    DIVIDER = 'divider',
    STATIC_SELECT = 'static_select',
    MRKDWN = 'mrkdwn',
    PLAIN_TEXT = 'plain_text',
}

export type SendData = ChatPostMessageArguments & { workspace: WORKSPACE };

export type OpenImResponse = WebAPICallResult & {
    channel: {
        id: string
    }
};
