#! /usr/bin/env ts-node
import { APPS } from './src/apps';
import { p } from './src/protocol';
import { OpenImResponse, SendData } from './src/types';
import { State } from 'assistant';
import { Channel, ChannelType } from './src/model/channel';

const DIRECT_CHANNELS = new Map<string, string>();


class SlackDriver {

    @p.action('SEND')
    async send(data: SendData, state: State) {
        console.log('SEND', data);
        const app = APPS.get(data.workspace);
        try {
            const channel = new Channel(data.channel);
            if (channel.type === ChannelType.DIRECT) {
                let directChannel = DIRECT_CHANNELS.get(channel.target);
                if (!directChannel) {
                    const res = <OpenImResponse>(await app.client.im.open({ user: channel.target }));
                    directChannel = res.channel.id;
                    DIRECT_CHANNELS.set(channel.target, directChannel);
                }
                channel.target = directChannel;
            }

            await app.client.chat.postMessage({
                blocks: data.blocks,
                channel: channel.target,
                text: data.text,
                as_user: true
            });
        } catch (e) {
            console.log(JSON.stringify(e.data));
            console.log(e.stack);
        }

    }
}
