#! /usr/bin/env ts-node
import { State } from 'assistant';
import { p } from './src/protocol';
import { Calendar } from './src/calendar';
import { Credentials, OAuth2ClientOptions } from 'google-auth-library';

class GoogleCalendarDriver {

    @p.action('LIST')
    async list(data: { token: Credentials, appInfo: OAuth2ClientOptions }, state: State) {
        const calendar = new Calendar(data.token, data.appInfo);
        console.log(await calendar.getEventsList());
        return calendar.getEventsList();
    }

    @p.action('WATCH')
    async watch(data: { token: Credentials, appInfo: OAuth2ClientOptions }, state: State) {
        const calendar = new Calendar(data.token, data.appInfo);
        await calendar.watch(state.data.calendarId);
    }
}
