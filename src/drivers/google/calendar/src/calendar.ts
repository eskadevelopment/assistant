import uuid from 'uuid/v4';
import { calendar_v3, google } from 'googleapis';
import { Credentials, OAuth2ClientOptions } from 'google-auth-library';
import { taskRefreshChannel } from './task';

export class Calendar {
    calendar: calendar_v3.Calendar;

    constructor(token: Credentials, appInfo: OAuth2ClientOptions) {
        const oAuth2Client = new google.auth.OAuth2(
            appInfo.clientId,
            appInfo.clientSecret
        );
        oAuth2Client.setCredentials(token);
        this.calendar = google.calendar({ version: 'v3', auth: oAuth2Client });
    }

    async getEventsList(): Promise<calendar_v3.Schema$Events> {
        const result = await this.calendar.events.list({
            calendarId: 'primary',
            timeMin: (new Date()).toISOString(),
            maxResults: 10,
            singleEvents: true,
            orderBy: 'startTime',
        });
        return result.data;
    }

    async watch(calendarId: string): Promise<calendar_v3.Schema$Channel> {
        const result = await this.calendar.events.watch({
            requestBody: {
                id: uuid(),
                type: 'web_hook',
                address: 'https://dmehed.skc.today/webserver/google/calendar/events',
            },
            calendarId: calendarId
        } as calendar_v3.Params$Resource$Events$Watch);
        console.log(result.data);
        taskRefreshChannel(result.data.expiration);
        return result.data;
    }

}
