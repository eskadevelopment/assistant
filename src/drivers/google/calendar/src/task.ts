import { Action, Route, State } from 'assistant';
import { p } from './protocol';

export const taskRefreshChannel = (expireDate) => {
    const delayedState = new State(
        new Route('seq', [new Action('REFRESH_TOKEN', p.executorName)])
    );

    p.start(new State(new Route('seq', [new Action('TASK', 'driver.scheduler')])), {
        date: new Date(expireDate ? expireDate - 10 * 1000 : new Date()),
        task: {
            data: {},
            state: delayedState
        }
    });
};
