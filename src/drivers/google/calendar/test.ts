#! /usr/bin/env ts-node
import { Action, Protocol, Route, State } from 'assistant';

const p = new Protocol(null);

// const s = new State(new Route('seq', [
//     new Action('GET_TOKEN', 'driver.google.auth'),
//     new Action('LIST', 'driver.google.calendar')
// ]), {});

const s = new State(new Route('seq', [
    new Action('GET_TOKEN', 'driver.google.auth'),
    new Action('WATCH', 'driver.google.calendar')
]), { calendarId: 'sk-consulting.com.ua_adrfrma6822g7uvpptet6q5bds@group.calendar.google.com' });

p.start(s, {});
