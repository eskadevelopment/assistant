#! /usr/bin/env ts-node
import { GoogleAuth } from './src/googleAuth';
import { TokenRepository } from './src/repository/token';
import { CredentialsRepository } from './src/repository/credentials';
import { CodeRepository } from './src/repository/code';
import { State } from 'assistant';
import { p } from './src/protocol';
import { taskRefreshToken } from './src/task';
import { Credentials, OAuth2ClientOptions } from 'google-auth-library';

const auth = new GoogleAuth(
    new TokenRepository(),
    new CredentialsRepository(),
    new CodeRepository()
);

class GoogleAuthDriver {

    @p.action('REFRESH_TOKEN')
    async refreshToken(data: {}, state: State) {
        await auth.refreshToken();
        const token = auth.getToken();
        taskRefreshToken(token);
    }

    @p.action('GET_TOKEN')
    getToken(data: any, state: State): { token: Credentials, appInfo: OAuth2ClientOptions } {
        console.log('GET_TOKEN', { token: auth.getToken(), appInfo: auth.getAppInfo() });
        return { token: auth.getToken(), appInfo: auth.getAppInfo() };
    }

    @p.action('SAVE_CODE')
    async saveCode(data: { code: string }, state: State) {
        await auth.setCode(data.code);
        return {};
    }
}

const token = auth.getToken();
taskRefreshToken(token);

// auth.getOAuth2Url();
// auth
//     .setCode('4/gAHFccgkLixljiiPYxKDvvQPlyJk3jd6ySQxWTVPa4i_Uvi-YQCeiPCSAjs7UnPAbSkng6K0-cie35yHQJIKZRc')
//     .then(_ => {
//         auth.getNewToken()
//             .then(__ => console.log('ok'));
//     });

// auth.refreshToken()
//     .then(_ => console.log('ok'));
