import credentials from '../../config/credentials.json';

export type AuthCredentials = {
    clientId: string;
    clientSecret: string;
};

export class CredentialsRepository {
    private readonly credentials: AuthCredentials;

    constructor() {
        const { client_secret, client_id } = credentials.installed;
        this.credentials = {
            clientId: client_id,
            clientSecret: client_secret
        };
    }

    get(): AuthCredentials {
        return this.credentials;
    }
}
