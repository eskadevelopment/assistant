import { Credentials } from 'google-auth-library';
import token from '../../config/token.json';
const path = require('path');

const fs = require('fs').promises;

export class TokenRepository {
    private token: Credentials;

    constructor() {
        this.token = token;
    }

    get(): Credentials | null {
        return this.token;
    }

    async set(token: Credentials) {
        this.token = token;
        await fs.writeFile(path.join(__dirname, '../../config', 'token.json'), JSON.stringify(token));
    }
}
