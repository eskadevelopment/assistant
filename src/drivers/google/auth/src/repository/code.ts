import code from '../../config/code.json';
const path = require('path');

const fs = require('fs').promises;

export class CodeRepository {
    private code: string;

    constructor() {
        this.code = code.code;
    }

    get(): string {
        return this.code;
    }

    async set(code: string) {
        this.code = code;
        await fs.writeFile(path.join(__dirname, '../../config', 'code.json'), JSON.stringify({ code: code }));
    }
}
