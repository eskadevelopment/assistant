import { Credentials, OAuth2ClientOptions } from 'google-auth-library';
import { google } from 'googleapis';
import { TokenRepository } from './repository/token';
import { CredentialsRepository } from './repository/credentials';
import { CodeRepository } from './repository/code';

const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];

export class GoogleAuth {
    // REDIRECT_URI = 'https://dmehed.skc.today/webserver/';
    REDIRECT_URI = 'http://localhost';

    private tokenRepository: TokenRepository;
    private credentialsRepository: CredentialsRepository;
    private codeRepository: CodeRepository;

    constructor(
        tokenRepository: TokenRepository, credentialsRepository: CredentialsRepository, codeRepository: CodeRepository
    ) {
        this.tokenRepository = tokenRepository;
        this.credentialsRepository = credentialsRepository;
        this.codeRepository = codeRepository;
    }

    getToken(): Credentials {
        return this.tokenRepository.get();
    }

    getAppInfo(): OAuth2ClientOptions {
        return this.credentialsRepository.get();
    }

    async getNewToken(): Promise<void> {
        const credentials = this.credentialsRepository.get();
        const oAuth2Client = new google.auth.OAuth2(
            credentials.clientId, credentials.clientSecret, this.REDIRECT_URI
        );
        try {
            const response = await oAuth2Client.getToken(this.codeRepository.get());
            await this.tokenRepository.set(response.tokens);
        } catch (e) {
            console.log('Error', e.message);
        }
    }

    async refreshToken(): Promise<void> {
        const token = this.getToken();
        if (new Date(token.expiry_date) > new Date()) {
            return;
        }

        const credentials = this.credentialsRepository.get();
        const oAuth2Client = new google.auth.OAuth2(
            credentials.clientId, credentials.clientSecret, this.REDIRECT_URI
        );
        oAuth2Client.setCredentials(token);
        try {
            await oAuth2Client.getAccessToken();
            await this.tokenRepository.set(oAuth2Client.credentials);
        } catch (e) {
            console.log('Error', e.message);
        }
    }

    getOAuth2Url() {
        const credentials = this.credentialsRepository.get();
        const oAuth2Client = new google.auth.OAuth2(
            credentials.clientId, credentials.clientSecret, this.REDIRECT_URI
        );
        const authUrl = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
            prompt: 'consent'
        });
        console.log('Authorize this app by visiting this url:', authUrl);
    }

    async setCode(code: string) {
        return this.codeRepository.set(code);
    }
}
