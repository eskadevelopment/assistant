#! /usr/bin/env ts-node
import { Action, Protocol, Route, State } from 'assistant';

const p = new Protocol(null);

const s = new State(new Route('seq', [
    new Action('GET_TOKEN', 'driver.google.auth')
]), {});

p.start(s, {});
