#! /usr/bin/env ts-node
import { State } from 'assistant';
import { p } from './src/protocol';
import { PreparedMessage } from './src/types';
import DB from './src/driver';


class PostgresqlDriver {

    @p.action('GET_ONE')
    async getOne(data: PreparedMessage, state: State) {
        const res = await DB.get(data.name).oneOrNone(data.query, data.args);
        console.log('GET_ONE', res);

        return res;
    }

    @p.action('GET_MANY')
    async getMany(data: PreparedMessage, state: State) {
        const res = await DB.get(data.name).manyOrNone(data.query, data.args);
        console.log('GET_MANY', res);
        return res;
    }

    @p.action('EXECUTE')
    async execute(data: PreparedMessage, state: State) {
        return await DB.get(data.name).none(data.query, data.args);
    }
}
