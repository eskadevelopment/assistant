import { Route, State } from 'assistant';
import { Protocol } from 'assistant/protocol';
import { PreparedMessage } from 'src/types';
import { Action } from 'assistant/models/action';

const p = new Protocol(null);

const s = new State(new Route('seq', [ new Action('GET_ONE', 'driver.postgresql') ]), {});

p.start(s, {
    query: 'select * from "user" limit ${limit};',
    args: { limit: 1 },
    name: 'bpm'
} as PreparedMessage);
