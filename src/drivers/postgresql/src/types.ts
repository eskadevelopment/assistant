export type PreparedMessage = {
    name: string,
    query: string;
    args: {
        [queryArgs: string]: any
    }
};
