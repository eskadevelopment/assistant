import pgp, { IMain, IDatabase } from 'pg-promise';
import { DATABASE_CONFIGS } from '../config';
import { TConnectionParameters } from 'pg-promise/typescript/pg-subset';
const pgpE: IMain = pgp();

export class DataBase {
    private pool = new Map<string, IDatabase<any>>();

    constructor() {
        DATABASE_CONFIGS.forEach((dbConf: TConnectionParameters) => {
            this.pool.set(dbConf.database, pgpE(dbConf));
        });
    }

    get(dbName: string) {
        return this.pool.get(dbName);
    }

}

export default new DataBase();
