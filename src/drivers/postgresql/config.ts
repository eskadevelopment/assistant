import { TConnectionParameters } from 'pg-promise/typescript/pg-subset';

const DEFAULT_DB_CONFIGS: TConnectionParameters = {
    host: '0.0.0.0',
    port: 5432,
    database: 'postgres',
    user: 'postgres',
    password: 'postgres',
    idleTimeoutMillis: 30000,
};

export const DATABASE_CONFIGS: Array<TConnectionParameters> = [
    { ...DEFAULT_DB_CONFIGS, port: 5435, database: 'bpm', password: '1111' },
    // { ...DEFAULT_DB_CONFIGS, port: 5432, database: 'bpm', password: '1111' },
    // { ...DEFAULT_DB_CONFIGS, port: 8888, database: 'bpm', password: '1111' }
];
