import json
from typing import Type


class AssistantMessageData(object):

    def __init__(self, **kwargs):
        pass

    def serialize(self):
        return {}

    def deserialize(self, raw_data):
        return {}


class AssistantMessage(object):
    raw_data: dict = None
    action: str = None
    app_id: int = None
    data: AssistantMessageData = None

    def __init__(
            self,
            data_class: Type[AssistantMessageData] = AssistantMessageData,
            action: action = 'WITHOUT_ACTION',
            app_id: int = 0,
            **kwargs
    ):
        self.action = action
        self.app_id = app_id
        self.data = data_class(**kwargs)

    def serialize(self):
        return json.dumps({
            'raw_data': self.raw_data,
            'action': self.action,
            'app_id': self.app_id,
            'data': self.data.serialize()
        }).encode()

    def deserialize(self, encoded_raw_data):
        body = json.loads(encoded_raw_data)
        self.raw_data = body
        self.action = self.raw_data.get('action', 'WITHOUT_ACTION')
        self.app_id = self.raw_data.get('app_id')
        self.data.deserialize(self.raw_data.get('data'))
