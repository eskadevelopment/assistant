import subprocess

from src.core.globals import CONFIG


def run_bus():
    pass


def run_plugin(module_name, module):
    subprocess.run('screen -dmS {module_name} {command}'.format(module_name=module_name, command=module.command))


def run():
    run_bus()

    for block in ['storage', 'logic', 'views', 'drivers']:
        for module_name, module in CONFIG[block].items():
            run_plugin(module_name, module)


run()
