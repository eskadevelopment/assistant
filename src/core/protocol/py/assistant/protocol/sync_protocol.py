import json
import os
from typing import List, Any, Dict

from kafka import KafkaProducer, KafkaConsumer

from assistant.protocol.models.action import Action
from assistant.protocol.models.protocol_message import ProtocolMessage
from assistant.protocol.models.route import Route
from assistant.protocol.models.state import State


class Protocol:
    executor_name: str
    producer: KafkaProducer
    consumer: KafkaConsumer
    actions: Dict[str, Any]

    def __init__(self, executor_name: str, channels_for_subscriptions: List[str] = None, max_poll_interval_ms=300000):
        self.executor_name = executor_name
        self.actions = {}

        self.producer = KafkaProducer(
            bootstrap_servers='{0}:{1}'.format(os.environ['KAFKA_HOST'], os.environ['KAFKA_PORT'])
        )
        if executor_name:
            self.consumer = KafkaConsumer(
                group_id='protocol',
                auto_offset_reset='latest',
                max_poll_interval_ms=max_poll_interval_ms,
                bootstrap_servers='{0}:{1}'.format(os.environ['KAFKA_HOST'], os.environ['KAFKA_PORT'])
            )
            self.consumer.subscribe(
                [executor_name] + (channels_for_subscriptions if channels_for_subscriptions else [])
            )

    def run_consumer_worker(self):
        print(
            'Run consumer {} worker with actions: \n{}'.format(self.executor_name, '\n'.join(list(self.actions.keys())))
        )
        for record in self.consumer:
            try:
                data = json.loads(record.value.decode())
                message: ProtocolMessage = ProtocolMessage.deserialize(data)

                func = self.actions.get(message.action)
                if func:
                    func(message.data, message.state)
            except Exception as e:
                print('[logic][test][protocol]', e)

    def next(self, data: Any, state: State):
        next_actions = state.get_next_actions(state.current_action.id)
        self._next(next_actions, data, state)

    def _next(self, next_actions: List[Action], data: Any, state: State):
        if not len(next_actions):
            return

        for next_action in next_actions:
            cloned_state: State = state.clone()
            cloned_state.prev_action = state.current_action
            cloned_state.current_action = next_action

            if (
                    (not cloned_state.prev_action and next_action.executor == self.executor_name) or
                    (
                            cloned_state.prev_action and
                            cloned_state.prev_action.executor == cloned_state.current_action.executor
                    )
            ):
                func = self.actions.get(cloned_state.current_action.action)
                if func:
                    func(data, cloned_state)
            else:
                message = ProtocolMessage(data, cloned_state)
                self.producer.send(cloned_state.current_action.executor, json.dumps(message.serialize()).encode())

    def action(self, name):
        def wrap_func(fn):
            def func(data: Any, state: State = None):
                res = fn(data, state)
                self.next(res, state)

            self.actions[name] = func
            return func

        return wrap_func

    def start(self, state: State, init_data: Any = None):
        state.build_barriers(state.route)
        first_route = state.route.route[0]

        if not first_route.type:
            self._next([first_route], init_data, state)
        else:
            next_actions = state.graph.get_first_action_in_parallel(first_route)
            self._next(next_actions, init_data, state)


if __name__ == '__main__':
    p = Protocol('test1')


    @p.action('FUNC_1')
    def func1(data: Any, state: State):
        print(1, data, state.__dict__)
        state.a = 1
        return [2, 3, 4]


    @p.action('FUNC_2')
    def func2(data: Any, state: State):
        print(2, data, state.__dict__)
        state.a = 2


    s = State(Route('seq', [
        Action('FUNC_1', 'TEST'),
        Action('FUNC_2', 'TEST'),
        Action('FUNC_1', 'TEST'),
    ]))
    p.start(s, {})
    p.run_consumer_worker()
