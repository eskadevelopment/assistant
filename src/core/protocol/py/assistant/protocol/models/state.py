from random import random
from typing import List, Any, Dict

from assistant.protocol.models.action import Action
from assistant.protocol.models.graph import Graph
from assistant.protocol.models.route import Route


class State(object):
    id: str
    route: Route
    current_action: Action
    prev_action: Action
    data: Any
    graph: Graph

    def __init__(self, route: Route, init_state: Any = None, id: str = None):
        self.id = id or str(random())
        self.route = route
        self.current_action = None
        self.prev_action = None
        self.data = dict(**(init_state or {}))
        if self.data.get('barriers') is None:
            self.data['barriers'] = {}
        self.graph = Graph(self.route)

    def serialize(self) -> Dict:
        return {
            'id': self.id,
            'route': self.route.serialize(),
            'currentAction': self.current_action.serialize(),
            'prevAction': self.prev_action and self.prev_action.serialize(),
            'data': self.data,
        }

    @staticmethod
    def deserialize(data) -> Any:
        state = State(Route.deserialize(data['route']))
        state.id = data['id']
        state.current_action = Action.deserialize(data['currentAction'])
        state.prev_action = data['prevAction'] and Action.deserialize(data['prevAction'])
        state.data = data['data']
        return state

    def get_next_actions(self, id: str) -> List[Action]:
        return self.graph.get_outputs(id)

    def clone(self) -> Any:  # State
        cloned_state = State(self.route, self.data, self.id)
        cloned_state.current_action = self.current_action
        cloned_state.prev_action = self.prev_action
        return cloned_state

    def build_barriers(self, route: Route):
        if route.type == 'seq':
            for index, subRoute in enumerate(route.route):
                if subRoute.type == 'par':
                    barrier_action = Action('BARRIER', 'logic.process')
                    route.route.splice(index + 1, 0, barrier_action)
                    self.data.barriers[barrier_action.id] = [action.id for action in subRoute.get_last_actions()]
                    self.build_barriers(subRoute)

        if route.type == 'par':
            for index, subRoute in enumerate(route.route):
                if subRoute.type == 'seq':
                    self.build_barriers(subRoute)
