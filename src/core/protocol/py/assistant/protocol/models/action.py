from random import random
from typing import Dict, Any


class Action(object):
    id: str
    action: str
    executor: str
    type: str  # Fast crutch
    extraData: dict

    def __init__(self, action, executor, extraData=None, id=None):
        self.id = id or str(random())
        self.action = action
        self.executor = executor
        self.type = None
        self.extraData = extraData or {}

    def serialize(self) -> Dict:
        return {
            'id': self.id,
            'action': self.action,
            'executor': self.executor,
            'extraArgs': self.extraData
        }

    @staticmethod
    def deserialize(data: Dict) -> Any:
        return Action(**data)
