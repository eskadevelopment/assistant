from typing import Any, Dict

from assistant.protocol.models.state import State


class ProtocolMessage(object):
    action: str
    data: Any
    state: State

    def __init__(self, data: Any, state: State):
        self.data = data
        self.state = state
        self.action = state.current_action.action

    def serialize(self) -> Dict:
        return {
            'action': self.action,
            'data': self.data,
            'state': self.state.serialize()
        }

    @staticmethod
    def deserialize(data: Dict) -> Any:
        return ProtocolMessage(data['data'], State.deserialize(data['state']))
