import random
from typing import List, TypeVar, Any, Dict

from assistant.protocol.models.action import Action

_RouteType = TypeVar('_RouteType', 'par', 'seq')

# {
#     'id': str,
#     'type': _RouteType,
#     'route': List
# }
SerializedRoute = Dict


class Route:
    id: str
    type: _RouteType
    route: Any  # Route | Action

    def __init__(self, type: _RouteType, route: Any, id: str = None):
        self.id = id or str(random.random())
        self.type = type
        self.route = route

    @staticmethod
    def deserialize(data: SerializedRoute) -> Any:
        return Route(
            data['type'],
            [Route.deserialize(r) if r.get('type') else Action.deserialize(r) for r in data['route']],
            data['id']
        )

    def serialize(self) -> SerializedRoute:
        return {
            'id': self.id,
            'type': self.type,
            'route': [r.serialize() if r.type else r.serialize() for r in self.route]
        }

    def get_last_actions(self) -> List[Action]:
        if self.type == 'seq':
            sub_route = self.route[len(self.route) - 1]
            if not sub_route.type:
                return [self.route[len(self.route) - 1]]
            return sub_route.get_last_actions()

        if self.type == 'par':
            actions_array = [
                not [child_route] if child_route.type else child_route.get_last_actions() for child_route in self.route
            ]

            data = []
            for arr in actions_array:
                data.extend(arr)
            return data
