from typing import List, Dict

from assistant.protocol.models.action import Action
from assistant.protocol.models.route import Route


class Node:
    outputs: List[Action]

    def __init__(self, outputs):
        self.outputs = outputs


class Graph:
    nodes: Dict[str, Node]

    def __init__(self, route: Route):
        self.nodes = dict()
        self.build(route, None)

    def build(self, route: Route, parent_next_action: Action):
        if route.type == 'par':
            for sub_route in route.route:
                if sub_route.type:
                    self.build(sub_route, parent_next_action)

                if not sub_route.type:
                    self.nodes[sub_route.id] = Node([parent_next_action] if parent_next_action else [])

        if route.type == 'seq':
            for index, sub_route in enumerate(route.route):
                next_route = None
                if len(route.route) - 1 > index:
                    next_route = route.route[index + 1]

                if sub_route.type:
                    self.build(sub_route, next_route or parent_next_action)

                if not sub_route.type:
                    next_actions = []
                    if len(route.route) - 1 > index:
                        if next_route.type:
                            for elem in self.get_first_action_in_parallel(next_route):
                                next_actions.append(elem)
                        else:
                            next_actions.append(route.route[index + 1])
                    else:
                        if parent_next_action:
                            next_actions.append(parent_next_action)

                    self.nodes[sub_route.id] = Node(next_actions)

    def get_first_action_in_sequence(self, route: Route) -> List[Action]:
        res = []
        sub_route = route.route[0]

        if not sub_route.type:
            res.append(sub_route)

        if sub_route.type:
            for elem in self.get_first_action_in_parallel(sub_route):
                res.append(elem)
        return res

    def get_first_action_in_parallel(self, route: Route) -> List[Action]:
        res = []
        for subRoute in route.route:
            if not subRoute.type:
                res.append(subRoute)

            if subRoute.type:
                for elem in self.get_first_action_in_sequence(subRoute):
                    res.append(elem)
        return res

    def get_outputs(self, id: str) -> List[Action]:
        node = self.nodes.get(id)
        return node.outputs if node else []
