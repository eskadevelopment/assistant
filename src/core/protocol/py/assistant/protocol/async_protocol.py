import json
import os
from typing import List, Any, Dict

from kafka import KafkaProducer, KafkaConsumer

from assistant.protocol.models.action import Action
from assistant.protocol.models.protocol_message import ProtocolMessage
from assistant.protocol.models.state import State
from assistant.protocol.utils import find


class AIOProtocol:
    executor_name: str
    producer: KafkaProducer
    consumer: KafkaConsumer
    actions: Dict[str, Any]

    def __init__(self, executor_name: str, channels_for_subscriptions: List[str] = None):
        self.executor_name = executor_name
        self.actions = {}

        self.producer = KafkaProducer(
            bootstrap_servers='{0}:{1}'.format(os.environ['KAFKA_HOST'], os.environ['KAFKA_PORT'])
        )

        self.consumer = KafkaConsumer(
            group_id='protocol',
            auto_offset_reset='latest',
            bootstrap_servers='{0}:{1}'.format(os.environ['KAFKA_HOST'], os.environ['KAFKA_PORT'])
        )
        self.consumer.subscribe([executor_name] + (channels_for_subscriptions if channels_for_subscriptions else []))

    def _next(self, data: Any, state: State = None):
        previous_action, index = find(lambda act: act.id == state.current_action.id, state.route)

        if len(state.route) - 1 > index:
            state.current_action = state.route[index + 1]
            if previous_action.executor == state.current_action.executor:
                func = self.actions.get(state.current_action.action)
                if func:
                    func(data, state)
            else:
                message = ProtocolMessage(data, state)
                self.producer.send(state.current_action.executor, json.dumps(message.serialize()).encode())

    def action(self, name):
        def wrap_func(fn):
            def func(data: Any, state: State = None):
                res = fn(data, state)
                self._next(res, state)

            self.actions[name] = func
            return func

        return wrap_func

    def action(self, name):
        async def wrap_func(fn):
            def func(data: Any, state: State = None):
                res = fn(data, state)
                self._next(res, state)

            self.actions[name] = func
            return func

        return wrap_func

    def run_consumer_worker(self):
        print('Run consumer worker with actions: \n{}'.format('\n'.join(list(self.actions.keys()))))
        for record in self.consumer:
            try:
                data = json.loads(record.value.decode())
                message: ProtocolMessage = ProtocolMessage.deserialize(data)
                func = self.actions.get(message.action)
                if func:
                    func(message.data, message.state)
            except Exception as e:
                print('[logic][test][protocol]', e)

    def start(self, state: State, init_data: Any = None):
        if state.current_action.executor == self.executor_name:
            func = self.actions.get(state.current_action.action)
            if func:
                func(init_data, state)
        else:
            message = ProtocolMessage(init_data, state)
            self.producer.send(state.current_action.executor, json.dumps(message.serialize()).encode())


if __name__ == '__main__':
    p = AIOProtocol('test1')


    @p.action('FUNC_1')
    def func1(data: Any, state: State):
        print(1, data, state.__dict__)
        state.a = 1
        return [2, 3, 4]


    @p.action('FUNC_2')
    def func2(data: Any, state: State):
        print(2, data, state.__dict__)
        state.a = 2


    s = State([
        Action('FUNC_1', 'TEST'),
        Action('FUNC_2', 'TEST'),
        Action('FUNC_1', 'TEST'),
    ])
    p.start(s, {})
    p.run_consumer_worker()
