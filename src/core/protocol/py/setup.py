from setuptools import setup, find_packages

setup(
    name='assistant_protocol',
    version='0.0.1',
    url='https://gitlab.com/Verbalist/Assistant',
    license='MIT',
    author='Dmytro Mekhed',
    author_email='d.mehed@sk-consulting.com.ua',
    description='Assistant protocol',
    packages=find_packages(),
    long_description='-',
    install_requires=[
        'kafka-python>=1.4.4'
    ],
    zip_safe=False
)
