import { Serializable } from './interfaces';
import { Action, SerializedAction } from './action';
import { Reaction, SerializedReaction } from './reaction';

export type RouteType = 'par' | 'seq';

export type SerializedRoute = {
    id: string;
    type: RouteType;
    route: Array<SerializedRoute | SerializedAction | SerializedReaction>;
};

export class Route implements Serializable<SerializedRoute | SerializedAction | SerializedReaction> {
    id: string;
    type: RouteType;
    route: Array<Route | Action | Reaction>;

    constructor(type: RouteType, route: Array<Route | Action | Reaction>, id: string = null) {
        this.id = id || Math.random().toString();
        this.type = type;
        this.route = route;
    }

    static deserialize(data: SerializedRoute): Route | Action {
        return new Route(
            data.type,
            data.route.map((routeOrAction) =>
                (<SerializedRoute>routeOrAction).type
                    ? Route.deserialize(<SerializedRoute>routeOrAction)
                    : Action.deserialize(<SerializedAction>routeOrAction)
            ),
            data.id
        );
    }

    serialize(): SerializedRoute {
        return {
            id: this.id,
            type: this.type,
            route: this.route.map((routeOrAction) =>
                (<SerializedRoute>routeOrAction).type
                    ? <SerializedRoute>routeOrAction.serialize()
                    : <SerializedAction>routeOrAction.serialize()
            )
        };
    }

    getLastActions(): Array<Action> {
        if (this.type === 'seq') {
            const subRoute = this.route[this.route.length - 1];
            if (!(<Route>subRoute).type) {
                return [<Action>this.route[this.route.length - 1]];
            }
            return (<Route>subRoute).getLastActions();
        }

        if (this.type === 'par') {
            const actionsArray = this.route.map(subRoute => {
                return !(<Route>subRoute).type ? [<Action>subRoute] : (<Route>subRoute).getLastActions();
            });
            return [].concat(...actionsArray);
        }
    }
}
