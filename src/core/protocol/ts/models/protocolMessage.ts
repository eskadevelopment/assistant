import { SerializedState, State } from './state';

export type SerializedProtocolMessage = {
    action: string;
    data: object;
    state: SerializedState;
};

export class ProtocolMessage {
    action: string;
    data: object;
    state: State;

    constructor(data: object, state: State) {
        this.data = data;
        this.state = state;
        this.action = state.currentAction.action;
    }

    serialize(): SerializedProtocolMessage {
        return {
            action: this.action,
            data: this.data,
            state: this.state.serialize()
        };
    }

    static deserialize(data: SerializedProtocolMessage): ProtocolMessage {
        return new ProtocolMessage(data.data, State.deserialize(data.state));
    }

}
