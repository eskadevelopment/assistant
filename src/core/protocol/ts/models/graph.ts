import { Route } from './route';
import { Action } from './action';
import { Reaction } from './reaction';

class Node {

    outputs: Array<Action | Reaction>;

    constructor(outputs: Array<Action | Reaction>) {
        this.outputs = outputs;
    }
}

export class Graph {

    nodes: Map<string, Node>;

    constructor(route: Route) {
        this.nodes = new Map();
        this.build(route, null);
    }

    build(route: Route, parentNextAction: Action): void {
        if ((<Route>route).type === 'par') {
            route.route.forEach(subRoute => {
                if ((<Route>subRoute).type) {
                    this.build(<Route>subRoute, parentNextAction);
                }

                if (!(<Route>subRoute).type) {
                    this.nodes.set((<Action>subRoute).id, new Node(parentNextAction ? [parentNextAction] : []));
                }

            });
        }

        if ((<Route>route).type === 'seq') {
            route.route.forEach((subRoute, index) => {
                let nextRoute: Route | Action | Reaction = null;
                if (route.route.length - 1 > index) {
                    nextRoute = route.route[index + 1];
                }

                if ((<Route>subRoute).type) {
                    this.build(<Route>subRoute, <Action>nextRoute || parentNextAction);
                }

                if (!(<Route>subRoute).type) {
                    const nextActions = [];
                    if (route.route.length - 1 > index) {
                        if ((<Route>nextRoute).type) {
                            this.getFirstActionsInParallel(<Route>nextRoute)
                                .forEach(elem => nextActions.push(elem));
                        } else {
                            nextActions.push(route.route[index + 1]);
                        }
                    } else {
                        if (parentNextAction) {
                            nextActions.push(parentNextAction);
                        }
                    }

                    this.nodes.set((<Action>subRoute).id, new Node(nextActions));
                }

            });
        }
    }

    getFirstActionInSequence(route: Route): Array<Action | Reaction> {
        const res = [];
        const subRoute = route.route[0];

        if (!(<Route>subRoute).type) {
            res.push(<Action>subRoute);
        }

        if ((<Route>subRoute).type) {
            this.getFirstActionsInParallel(<Route>subRoute)
                .forEach(elem => res.push(elem));
        }
        return res;
    }


    getFirstActionsInParallel(route: Route): Array<Action | Reaction> {
        const res = [];
        route.route.forEach(subRoute => {
            if (!(<Route>subRoute).type) {
                res.push(<Action>subRoute);
            }

            if ((<Route>subRoute).type) {
                this.getFirstActionInSequence(<Route>subRoute)
                .forEach(elem => res.push(elem));
            }
        });
        return res;
    }

    getOutputs(id: string): Array<Action | Reaction> {
        const node = this.nodes.get(id);
        return node ? node.outputs : [];
    }

}
