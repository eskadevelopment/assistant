import { Serializable } from './interfaces';

export type SerializedReaction = {
    id: string;
    action: string;
    executor: string;
};

export class Reaction implements Serializable<SerializedReaction> {
    id: string;
    action: string;
    executor: string;

    constructor(action: string, executor: string, id = null) {
        this.id = id || Math.random().toString();
        this.action = action;
        this.executor = executor;
    }


    serialize(): SerializedReaction {
        return {
            id: this.id,
            action: this.action,
            executor: this.executor,
        };
    }


    static deserialize(data: SerializedReaction): Reaction {
        return new Reaction(data.action, data.executor, data.id);
    }

}
