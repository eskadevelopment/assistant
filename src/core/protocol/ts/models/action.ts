import { Serializable } from './interfaces';

export type SerializedAction = {
    id: string;
    action: string;
    executor: string;
    extraData: object;
};

export class Action implements Serializable<SerializedAction> {
    id: string;
    action: string;
    executor: string;
    extraData: any;

    constructor(action: string, executor: string, extraData: any = null, id = null) {
        this.id = id || Math.random().toString();
        this.action = action;
        this.executor = executor;
        this.extraData = extraData;
    }


    serialize(): SerializedAction {
        return {
            id: this.id,
            action: this.action,
            executor: this.executor,
            extraData: this.extraData
        };
    }


    static deserialize(data: SerializedAction): Action {
        return new Action(data.action, data.executor, data.extraData, data.id);
    }

}
