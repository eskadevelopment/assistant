export interface Serializable<T> {
    serialize(): T;
    // implement with static deserialize(data: T): Serialize
}
