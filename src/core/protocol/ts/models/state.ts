import { Action, SerializedAction } from './action';
import { Route, SerializedRoute } from './route';
import { Graph } from './graph';
import { Reaction, SerializedReaction } from './reaction';
import { Serializable } from './interfaces';

export interface SerializedState {
    id: string;
    route: SerializedRoute;
    currentAction: SerializedAction | SerializedReaction;
    prevAction: SerializedAction | SerializedReaction;
    data: any;
}

export class State {
    id: string;
    route: Route;
    currentAction: Action | Reaction;
    prevAction: Action | Reaction;
    data: { barriers: { [actionId: string]: Array<string> } } & any;
    graph: Graph;

    constructor(route: Route, initState: any = {}, id: string = null) {
        this.id = id || Math.random().toString();
        this.route = route;
        this.currentAction = null;
        this.prevAction = null;
        this.data = { barriers: {}, ...initState };
        this.graph = new Graph(this.route);
    }

    serialize(): SerializedState {
        return {
            id: this.id,
            route: this.route.serialize(),
            currentAction: (<Serializable<SerializedReaction | SerializedAction>>this.currentAction).serialize(),
            prevAction: this.prevAction &&
                (<Serializable<SerializedReaction | SerializedAction>>this.prevAction).serialize(),
            data: this.data,
        };
    }

    static deserialize(data: SerializedState): State {
        const state = new State(<Route>Route.deserialize(data.route));
        state.id = data.id;
        // TODO Reaction <Action>data.currentAction/prevAction
        state.currentAction = Action.deserialize(<Action>data.currentAction);
        state.prevAction = data.prevAction && Action.deserialize(<Action>data.prevAction);
        state.data = data.data;
        return state;
    }

    getNextActions(id: string): Array<Action | Reaction> {
        return this.graph.getOutputs(id);
    }

    clone(): State {
        const clonedState = new State(this.route, this.data, this.id);
        clonedState.currentAction = this.currentAction;
        clonedState.prevAction = this.prevAction;
        return clonedState;
    }

    buildBarriers(route: Route) {
        if (route.type === 'seq') {
            route.route.forEach((subRoute, index) => {
                if ((<Route>subRoute).type === 'par') {
                    const barrierAction = new Action('BARRIER', 'logic.process');
                    route.route.splice(index + 1, 0, barrierAction);
                    this.data.barriers[barrierAction.id] = (<Route>subRoute).getLastActions().map(action => action.id);
                    this.buildBarriers(<Route>subRoute);
                }
            });
        }

        if (route.type === 'par') {
            route.route.forEach((subRoute, index) => {
                if ((<Route>subRoute).type === 'seq') {
                    this.buildBarriers(<Route>subRoute);
                }
            });
        }
    }

    attach(state: State): void {
        throw Error('Not implemented, use subStart instead');
    }

}
