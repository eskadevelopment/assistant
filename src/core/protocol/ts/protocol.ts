import { ConsumerGroup, KafkaClient, Message, Producer } from 'kafka-node';
import { ProtocolMessage } from './models/protocolMessage';
import { State } from './models/state';
import { Route } from './models/route';
import { Action } from './models/action';
import { Reaction } from './models/reaction';
import { DEBUG } from './config';
import { EventEmitter } from 'events';

export class Protocol {
    executorName: string;
    client: KafkaClient;
    producer: Producer;
    consumer: ConsumerGroup;
    actions: Map<string, (data: any, state: State) => any>;
    responseEventBus: EventEmitter;

    constructor(executorName: string, channelsForSubscriptions: Array<string> = [], maxPollIntervalMs = 300000) {
        this.executorName = executorName;
        this.actions = new Map();

        const kafkaHost = `${process.env.KAFKA_HOST || '0.0.0.0'}:${process.env.KAFKA_PORT || '9092'}`;
        this.client = new KafkaClient({ kafkaHost });

        this.producer = new Producer(this.client);
        if (this.executorName) {
            this.consumer = new ConsumerGroup({
                    kafkaHost,
                    groupId: 'protocol',
                    autoCommit: true,
                    autoCommitIntervalMs: 100,
                    sessionTimeout: 15000,
                    fromOffset: 'latest',
                    outOfRangeOffset: 'earliest',
                },
                this.executorName
            );

            this.client.on('ready', () => {
                this.run_consumer_worker();
            });
        }

        this.responseEventBus = new EventEmitter();

        this.response = this.response.bind(this);
        this.actions.set('RESPONSE', this.response);

    }

    run_consumer_worker() {
        console.log(`Run consumer ${this.executorName} worker with actions:\n${[...this.actions.keys()].join('\n')}`);
        this.consumer.on('message', async (message: Message) => {
            try {
                const msg = ProtocolMessage.deserialize(JSON.parse(message.value.toString('utf-8')));
                const func = this.actions.get(msg.action);
                // TODO reaction and check par process
                if (func) {
                    const data = (<Action>msg.state.currentAction).extraData
                        ? { ...(<Action>msg.state.currentAction).extraData, data: msg.data }
                        : msg.data;
                    func(data, msg.state);
                } else {
                    console.error(`In Executor ${this.executorName} not exist action ${msg.action}`);
                }
            } catch (e) {
                console.error('[Protocol][on]', e.message);
                console.error(e.stack);
            }
        });
    }

    next(data: any, state: State) {
        const nextActions = state.getNextActions(state.currentAction.id);
        this._next(nextActions, data, state);
    }

    private _next(nextActions: Array<Action | Reaction>, data: any, state: State): void {
        if (!nextActions.length) {
            return;
        }

        // TODO implement reaction logic
        nextActions.forEach((nextAction, index) => {
            const clonedState: State = state.clone();
            clonedState.prevAction = state.currentAction;
            clonedState.currentAction = <Action>nextAction;
            if (
                (!clonedState.prevAction && nextAction.executor === this.executorName) ||
                (clonedState.prevAction && clonedState.prevAction.executor === clonedState.currentAction.executor)
            ) {
                const func = this.actions.get(clonedState.currentAction.action);
                if (func) {
                    // TODO reaction
                    const actionData = (<Action>clonedState.currentAction).extraData
                        ? { ...(<Action>clonedState.currentAction).extraData, data }
                        : data;
                    if (nextActions.length > 1 && Array.isArray(actionData) && actionData.length === nextActions.length) {
                        func(actionData[index], clonedState);
                    } else {
                        func(actionData, clonedState);
                    }
                } else {
                    console.error(`In Executor ${this.executorName} not exist action ${clonedState.currentAction.action}`);
                }
            } else {
                const actionData = (nextActions.length > 1 && Array.isArray(data) && data.length === nextActions.length)
                    ? data[index]
                    : data;
                const message = new ProtocolMessage(actionData, clonedState);
                try {
                    this.producer.send([{
                        topic: clonedState.currentAction.executor,
                        messages: JSON.stringify(message.serialize())
                    }], (error, d) => {
                        console.log('SEND', d);
                        // pass
                    });
                } catch (e) {
                    console.error('SOME SHIT WITH KAFKA', e, e.stack);
                }

            }
        });
    }

    action(name: string) {
        return (
            target: Object,
            propertyKey: string,
            descriptor: TypedPropertyDescriptor<(data: any, state: State) => any>
        ) => {
            const method = descriptor.value;
            descriptor.value = async (data: any, state: State): Promise<any> => {
                if (DEBUG) {
                    console.log(`Executor: ${this.executorName}, action: ${name}, data: ${JSON.stringify(data)}`);
                }

                const res = await method(data, state);
                this.next(res, state);
            };
            this.actions.set(name, descriptor.value);
            return descriptor;
        };
    }


    start<T>(state: State, initData: T = {} as any): void {
        state.buildBarriers(state.route);
        const firstRoute = state.route.route[0];
        if (!(<Route>firstRoute).type) {
            this._next([<Action>firstRoute], initData, state);
        } else {
            const nextActions = state.graph.getFirstActionsInParallel(<Route>firstRoute);
            this._next(nextActions, initData, state);
        }
    }

    async subStart<T>(state: State, initData: any): Promise<T> {
        if (state.route.type == 'seq') {
            state.route.route.push(
                new Action('RESPONSE', this.executorName)
            );
        } else {
            state.route = new Route('seq', [
                state.route,
                new Action('RESPONSE', this.executorName)
            ]);
        }
        return new Promise((resolve) => {

            this.responseEventBus.once(state.route.route[0].id, event => {
                resolve(event);
            });

            this.start(state, initData);
        });
    }

    async response<T>(data: any, state: State): Promise<void> {
        this.responseEventBus.emit(state.route.route[0].id, data);
    }

    async request<T>(action: string, executor: string, data: any): Promise<T> {
        return new Promise((resolve) => {
            const s = new State(new Route('seq', [
                new Action(action, executor),
                new Action('RESPONSE', this.executorName),
            ]));

            this.responseEventBus.once(s.route.route[0].id, event => {
                resolve(event);
            });

            this.start(s, data);

            // TODO add logic for return timeout error (maybe?)
        });
    }
}
