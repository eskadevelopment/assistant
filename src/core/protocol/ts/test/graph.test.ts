import { Graph } from '../models/graph';
import { testRoute } from './testRoute';

const graph = new Graph(testRoute);
const expectedResult = JSON.stringify(
    [['1', {
        'outputs': [{
            'id': '2',
            'action': 'test2',
            'executor': 'protocol.test',
            'extraData': null
        }]
    }], ['2', {
        'outputs': [{
            'id': '3.1.1',
            'action': 'test2',
            'executor': 'protocol.test',
            'extraData': null
        }, { 'id': '3.2', 'action': 'test2', 'executor': 'protocol.test', 'extraData': null }]
    }], ['3.1.1', {
        'outputs': [{
            'id': '3.1.2',
            'action': 'test1',
            'executor': 'protocol.test',
            'extraData': null
        }]
    }], ['3.1.2', {
        'outputs': [{
            'id': '4',
            'action': 'test1',
            'executor': 'protocol.test',
            'extraData': null
        }]
    }], ['3.2', {
        'outputs': [{
            'id': '4',
            'action': 'test1',
            'executor': 'protocol.test',
            'extraData': null
        }]
    }], ['4', { 'outputs': [] }]]);

console.log([...graph.nodes.entries()]);
console.log(`test ${JSON.stringify([...graph.nodes.entries()]) === expectedResult ? 'ok' : 'fail'}`);
