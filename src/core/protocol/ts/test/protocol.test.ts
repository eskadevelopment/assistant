import { State } from '../models/state';
import { Protocol } from '../protocol';
import { testRoute } from './testRoute';

const p = new Protocol('protocol.test');


class Test {

    @p.action('test1')
    test1(data: any, state: State) {
        console.log('test1', {
            prev: state.prevAction && state.prevAction.id,
            curr: state.currentAction.id
        }, data);
        return 'test1';
    }

    @p.action('test2')
    test2(data: any, state: State) {
        console.log('test2', {
            prev: state.prevAction && state.prevAction.id,
            curr: state.currentAction.id
        }, data);
        return 'test2';
    }

    @p.action('test3')
    test3(data: any, state: State) {
        return ['test1', 'test2'];
    }

}


p.producer.on('ready', () => {
    p.consumer.on('connect', () => {
        console.log('+==================================+');
        const s = new State(testRoute);
        p.start(s, 'init');
    });
});


/* Must look's like
+==================================+
test1 { prev: null, curr: '1' } init
test2 { prev: '0.12279301221947181', curr: '2' } [ 'test1' ]
test2 { prev: '2', curr: '3.1.1' } test2
test1 { prev: '3.1.1', curr: '3.1.2' } test2
test2 { prev: '2', curr: '3.2' } test2
test1 { prev: '0.3204733032515883', curr: '4' } [ 'test1', 'test2' ]
*/
