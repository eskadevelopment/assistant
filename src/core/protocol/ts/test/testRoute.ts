import { Route } from '../models/route';
import { Action } from '../models/action';

/* template.ts {
    'type': 'seq',
    'route': [
        {
            'type': 'par',
            'route': [
                {
                    'id': '1',
                    'action': 'test1',
                    'executor': 'protocol.test'
                },
            ]
        },
        {
            'id': '2',
            'action': 'test2',
            'executor': 'protocol.test'
        },
        {
            'type': 'par',
            'route': [
                {
                    'type': 'seq',
                    'route': [
                        {
                            'id': '3.1.1',
                            'action': 'test2',
                            'executor': 'protocol.test'
                        },
                        {
                            'id': '3.1.2',
                            'action': 'test1',
                            'executor': 'protocol.test'
                        }
                    ]
                },
                {
                    'id': '3.2',
                    'action': 'test2',
                    'executor': 'protocol.test'
                }
            ]
        },
        {
            'id': '4',
            'action': 'test1',
            'executor': 'protocol.test'
        }
    ]
} */
export const testRoute = new Route('seq', [
        new Route('par', [
            new Action('test1', 'protocol.test', null, '1')
        ]),
        new Action('test2', 'protocol.test', null, '2'),
        new Route('par', [
            new Route('seq', [
                new Action('test3', 'protocol.test', null, '3.0.1'),
                new Route('par', [
                    new Action('test2', 'protocol.test', null, '3.1.1.1'),
                    new Action('test2', 'protocol.test', null, '3.1.1.2'),
                ]),
                new Action('test2', 'protocol.test', null, '3.1.2'),
                new Action('test1', 'protocol.test', null, '3.1.3')
            ]),
            new Action('test2', 'protocol.test', null, '3.2')
        ]),
        new Action('test1', 'protocol.test', null, '4')
    ]
);
