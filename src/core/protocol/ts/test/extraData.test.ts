import { State } from '../models/state';
import { Protocol } from '../protocol';
import { Action, Route } from '..';

const p = new Protocol('protocol.test.extra');


class Test {

    @p.action('EXTRA_TEST_1')
    test1(data: any, state: State) {
        console.log('EXTRA_TEST_1', {
            prev: state.prevAction && state.prevAction.id,
            curr: state.currentAction.id
        }, data);
        return 'EXTRA_TEST_1';
    }

    @p.action('EXTRA_TEST_2')
    test2(data: any, state: State) {
        console.log('EXTRA_TEST_2', {
            prev: state.prevAction && state.prevAction.id,
            curr: state.currentAction.id
        }, data);
        return 'EXTRA_TEST_2';
    }

}


p.producer.on('ready', () => {
    p.consumer.on('connect', () => {
        console.log('+==================================+');
        const s = new State(new Route('seq', [
            new Route('par', [
                new Action('EXTRA_TEST_1', 'protocol.test.extra', 'not init', '1')
            ]),
            new Action('EXTRA_TEST_2', 'protocol.test.extra', null, '2'),
            new Route('par', [
                new Route('seq', [
                    new Action('EXTRA_TEST_2', 'protocol.test.extra', null, '3.1.1'),
                    new Action('EXTRA_TEST_1', 'protocol.test.extra', 'not EXTRA_TEST_2', '3.1.2')
                ]),
                new Action('EXTRA_TEST_2', 'protocol.test.extra', null, '3.2')
            ]),
            new Action('EXTRA_TEST_1', 'protocol.test.extra', 'not barrier', '4')
        ]));
        p.start(s, 'init');
    });
});


/* Must look's like
+==================================+
EXTRA_TEST_1 { prev: null, curr: '1' } not init
EXTRA_TEST_2 { prev: '0.12540951844084902', curr: '2' } [ 'EXTRA_TEST_1' ]
EXTRA_TEST_2 { prev: '2', curr: '3.1.1' } EXTRA_TEST_2
EXTRA_TEST_2 { prev: '2', curr: '3.2' } EXTRA_TEST_2
EXTRA_TEST_1 { prev: '3.1.1', curr: '3.1.2' } not EXTRA_TEST_2
EXTRA_TEST_1 { prev: '0.8907684775044096', curr: '4' } not barrier
*/
