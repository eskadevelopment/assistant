import os

from src.core.globals import CONFIG

blocks = ['drivers', 'logic', 'storage', 'views']

for block in blocks:
    modules = os.listdir('src/{}'.format(block))
    for module in modules:
        control_file_path = 'src/{}/{}/control.yaml'.format(block, module)
        try:
            control_file = open(control_file_path)
            # TODO gather all file into root control file and check dependencies
            CONFIG[block][module] = {}
        except FileNotFoundError:
            print('Control file not fount in {} read more in PLUGIN_PROTOCOL.md'.format(control_file_path))

