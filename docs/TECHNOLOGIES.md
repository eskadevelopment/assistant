# Technologies which we use

### Frontend
* [react](https://reactjs.org/)
* [redux](https://redux.js.org/)
* [typescript](https://www.typescriptlang.org/)

### Storage
* [postgresql](https://www.postgresql.org/)
* Any NoSQL storage
* Any SQL storage
* Any Cloud storage like GDrive or dropbox 

### Service Mech
* [kafka](https://kafka.apache.org/)

### Drivers
* Any language which can connect to Kafka

### Logic
* Functions ideology (1 logic module = 1 function)
* Any language which can connect to Kafka
