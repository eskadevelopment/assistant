Folder structure
```
/src
    /drivers
        /base
        /protocol
        /{any_drivers}
    /event_bus
    /view  
        /desktop  # submodule
        /mobile  # submodule
        /{any_view_module}
    /storage
        /SQL
        /NoSQL
        /Cloud
    /logic
        /{module_name}
```
![Node](images/Node.png)
