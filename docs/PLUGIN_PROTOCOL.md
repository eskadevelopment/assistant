#Protocol for plugins

All plugins communicate between each other through kafka.
For send whatever to postgres need write into kafka topic `storage:postgres`

### Plugin protocol
`control.yaml` must be placed in root folder for plugin
```yaml
drivers:
  slack-reader: # unique pluginName for injection
    image: slack-reader # for future registry
    build:
      context: .
      builder: docker-compose | vagga | shell
      command: docker-compose up screen1 -d
    depend:
      storage:
       - postgres
      logic:
       - union-messanger
```

### Root control plugin File

```yaml
drivers:
  slack-reader:
  screen1:
    context: .
    builder: docker-compose | vagga | shell
    command: docker-compose up screen1 -d
  slack-writer:
    context: ./slack
    builder: vagga
    command: vagga slack

event_bus:
  topics:
    - test
    - slack
    - messanger

logic:
  union-messanger:
  screen1:
    context: .
    builder: vagga
    command: vagga screen1

storage:
  postgres:
  mongoDb:
    context: .
    builder: vagga
    command: vagga mongoDb

view:
  dashboard:
  mailler:
    type: mailler
    context: .
    builder: vagga
    command: vagga mailler

```